# ADAPRO

ADAPRO stands for ALICE Data Point Processing Framework. ADAPRO is written in
C++14 (for CentOS 7, but it should be fairly portable to other POSIX-compliant
operating systems as well). The main focus of the framework is on producing
multi-threaded application with optional support for configuration files and
remote control and monitoring. Remote control and monitoring is provided through
the Distributed Information Management (DIM) protocol.

## 1. Warnings

It seems that NetBeans likes to use absolute paths in the makefiles by default.
This behaviour can be corrected by setting *Tools -> Options -> C/C++ -> Project
Options -> File Path Mode* to `Always Relative`. *Using absolute paths in
makefiles breaks the build on other machines.*

## 2. Dependencies

This project depends on
* *Little-endian machine arcihecture*
* `gcc` with C++14 support (version 6.2 or higher is recommended)
* `git` (and CERN GitLab, of course)
* All the usual tools for building C++ projects (standard headers, `make`, etc.)
* `pkg-config`
* systemd headers. The required dpkg package on Debian is `libsystemd-dev`, and
  the required rpm package on CentOS is `systemd-devel`.

Without these pieces of software, it's not possible to obtain and compile ADAPRO
from the source code on Linux. Precompiled shared objects are available at the
*Pipelines* section of this repository. They may or may not work on any
particular CentOS/Debian setup. Compiling the source code is the recommended way
for obtaining the shared object.

Additionally, the project has the following *optional* dependencies:
* `NetBeans` IDE (version 8.1 or higher recommended, with C++ support, duh)
* `dia` (for document generation)
* `pdflatex` (for document generation)
  * many (La)TeX packages are needed, see Dockerfile
* `doxygen` (for document generation; *version 1.8.13 doesn't work*)
  * depends on `graphviz`
* `cppcheck` (for static analysis)
* `cppunit` (for tests).
* `gdb` for debugging
* `valgrind` for memory debugging

Optional dependencies are used for developing ADAPRO, and they're not needed for
building/installation/deployment.

The following versions, for example, are known to work:
* `gcc 6.3.0`
* `git 2.11.0`
* `make 4.1`
* `pkg-config 0.2.9`
* `libsystemd-dev 232-25+deb9u1`
* `NetBeans 8.1`
* `dia 0.97+git`
* `doxygen 1.7`
* `LaTeX 2016.20170`
* `cppcheck 1.76.1`
* `libcppunit-dev 1.13.2`
* `gdb 7.12`
* `valgrind 3.12.0.SVN`

## 3. Compiling and Installing

This section assumes that the (non-optional) dependencies are installed.

Then the project can be built by simply running `make` at project root
directory.
  * Optionally, it can be built with the `Debug` (or `No_DIM`) configuration by
  running `make CONF=Debug`. In debug mode, ADAPRO prints debug messages into
  `stdout` in addition to the error messages that are always printed into
  `stderr`.

The shared object containing ADAPRO runtime will be created at
  * `dist/Release/GNU-Linux/libadapro.so` for `Release` configuration;
  * `dist/Debug/GNU-Linux/libadapro.so` for `Debug` configuration; or
  * `dist/No_DIM/GNU-Linux/libadapro-no-dim.so` for `No_DIM` configuration.

Additionally, a system-wide installation of ADAPRO can be done by invoking
  `make install` with the *super user* (root) rights.
  * This script will copy the headers to `/usr/include/adapro`.
  * The shared object will be copied to `/usr/lib/adapro/libadapro.so`
    * `Release` configuration has priority over `Debug`. `Release` and `Debug`
    shared objects can't both be installed at the same time.
    * Optionally, a shared object compiled with the `No_DIM` configuration can
    be also installed at `/usr/lib/adapro/libadapro-no-dim.so`.
  * A symbolic link pointing to `/usr/lib/adapro/libadapro.so` will be created
    with the name `/usr/lib/libadapro.so`.
    * Also, a symbolic link pointing to `/usr/lib/adapro/libadapro-no-dim.so`
    with the name `/usr/lib/libadapro-no-dim.so` will be created if the `No_DIM`
    version of the shared object existed.

As mentioned in the previous section, precompiled shared objects are available
at the *Pipelines* section of this repository.

## 4. Building Documentation

This section assumes that the optional dependencies are intalled.

To build the API Documentation, run `make apidoc` at the project root directory.
The HTML version of the API Documentation will appear at `dist/apidoc/html`.
The PDF version generated with LaTeX will appear at
`dist/apidoc/pdf/refman.pdf`.

To build the Technical Documentation, run `make techdoc` at the project root
directory. The PDF file, will appear at `dist/doc/TechnicalDocumentation.pdf`.

## 5. Running Unit Tests and Analysis

This section assumes that the optional dependencies are intalled.

To run the unit tests, run `make test CONF=No_DIM` at the project root
directory. The output of the tests should go to `stdout` and/or `stderr`.
Running the tests will take some time.

Optionally, a code coverage report can be generated after the tests by running
`make lcov`. The results will appear in `dist/reports/lcov`.

Static analysis can be performed on the code by running `make cppcheck`. The
results will appear in `dist/reports/cppcheck`.

## 6. Running Integration Tests / Example Applications

This section assumes that ADAPRO has been successfully installed (see section
3).

The two example applications are located at `examples`. These applications can
be compiled by running `make` in their respective subdirectories. The compiled
executable should appear in the same directory and they are immediately ready to
be executed.
