/*
 * File:   Parameters.hpp
 * Author: john Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 February 2017, 11:32
 */

#ifndef ADAPRO_PARAMETERS_HPP
#define ADAPRO_PARAMETERS_HPP

#include <string>
#include <algorithm>
#include <map>
#include <utility>
#include <regex>
#include <functional>
#include "../data/Typedefs.hpp"

namespace ADAPRO
{
namespace Data
{
    /**
     * A regular expression matching signed integers.
     */
    static const std::regex REGEX_INTEGER("^-?[0-9]+$");

    /**
     * A regular expression matching unsigned integers.
     */
    static const std::regex REGEX_UNSIGNED("^[0-9]+$");

    /**
     * A regular expression matching IPv4 addresses.
     */
    static const std::regex REGEX_IPV4_ADDRESS
    (
            "^(25[0-4]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])"
            "(\\.(25[0-4]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])){3}$"
    );

    /**
     * A regular expression matching hostnames.
     */
    static const std::regex REGEX_HOSTNAME("^(\\w|-|\\.)+$");

    /**
     * A regular expression matching DIM service names supported by ADAPRO.
     */
    static const std::regex REGEX_DIM_SERVICE_NAME("^(\\w|-|/|:|&)+$");

    /**
     * A regular expression matching well-formed DIM service description
     * strings. Used by <tt>ADAPRO::DIM::Wrapper</tt> for input validation.
     */
    static const std::regex REGEX_DIM_SERVICE_DESCRIPTION
    (
            "^([CSILXFD]|"
            "(([CSILXFD]:[1-9]\\d*(;[CSILXFD]:[1-9]\\d*)*)+(;[CSILXFD])?))$"
    );

    /**
     * A regular expression matching strings of word characters
     * (<tt>^[a-zA-Z0-9_]$</tt>).
     */
    static const std::regex REGEX_WORD("^(\\w)+$");

    /**
     * A regular expression matching all strings containing whitespace
     * (including non-breaking spaces).
     */
    static const std::regex REGEX_WHITESPACE("[[:space:]]| ");

    /**
     * A regular expression matching sensible Linux filenames.
     */
    static const std::regex REGEX_FILENAME("^(/?(\\w|-|\\.)+)+$");

    /**
     * A regular expression matching sensible Linux directory names that end
     * with a <tt>'/'</tt>.
     */
    static const std::regex REGEX_DIRECTORY
    (
            "^/?"
            "(\\.?(([^ \\s!$`&*()+\\.\\\\/]+|"
            "(\\\\(\\ |\\!|\\$|\\`|\\&|\\*|\\(|\\)|\\+)))"
            "(\\.([^ \\s!$`&*()+\\.\\\\/]+|"
            "(\\\\(\\ |\\!|\\$|\\`|\\&|\\*|\\(|\\)|\\+))))?|"
            "\\.)/)+$"
    );

    /**
     * A regular expression matching a sorted string expressing a logging level
     * mask.
     */
    static const std::regex REGEX_LOGGING_MASK
    (
            "^A?D?E?F?I?S?W?$"
    );

    /**
     * Returns <tt>true</tt> if and only if the argument equals to
     * <tt>"TRUE"</tt> or <tt>"FALSE"</tt>.
     */
    static const std::function<bool(const std::string&)> BOOLEAN =
            [](const std::string& s){return s == "TRUE" || s == "FALSE";};

    /**
     * This parameter is used for setting the nice value of the Linux process
     * of the ADAPRO application. The value range is the string representations
     * of integers between -20 and 19 (inclusive). The default value is "-20".
     * For more information, see <tt>man nice</tt>. Setting a negative nice
     * value requires <tt>CAP_SYS_NICE</tt> capability on Linux.
     */
    static const parameter_t NICE
    {
        "ADAPRO_NICE",
        "0",
        [](const std::string& s)
        {
            return std::regex_match(s, REGEX_INTEGER) && std::stoi(s) >= -20 &&
                    std::stoi(s) < 20;
        }
    };

    /**
     * This parameter is used for enabling or disabling memory locking. Locking
     * the process' virtual memory prevents the (Linux) operating system from
     * swapping it to disk. Locking the process memory requires
     * <tt>CAP_IPC_LOCK</tt> capability on Linux.
     */
    static const parameter_t LOCK_MEMORY
    {
        "ADAPRO_LOCK_MEMORY",
        "FALSE",
        BOOLEAN
    };

    /**
     * This Linux-only parameter is used for enabling or disabling daemon mode.
     * In daemon mode, an ADAPRO process will run as a Linux daemon, i.e. as an
     * background process detached from a virtual terminal. In daemon mode, the
     * output goes to a log file.
     */
    static const parameter_t DAEMON_ENABLED
    {
        "ADAPRO_DAEMON_ENABLED",
        "FALSE",
        BOOLEAN
    };

    /**
     * This Linux-only parameter is used for setting the logfile for the daemon
     * mode. In daemon mode, the ADAPRO Logger prints its output into the
     * logfile instead of <tt>stdout</tt> or <tt>stderr</tt>.
     */
    static const parameter_t DAEMON_LOGFILE
    {
        "ADAPRO_DAEMON_LOGFILE",
        "/var/log/adapro.log",
        [](const std::string& s)
        {
            return s.length() < 255 && std::regex_match(s, REGEX_FILENAME);
        }
    };

    /**
     * <p>
     * This parameter can be used for filtering events in log output. Events
     * with a logging level not included in the logging mask, constructed from
     * the value of this parameter, are discarded. The default logging mask
     * includes all logging levels, except <tt>ADAPRO_DEBUG</tt>.
     * </p>
     * <p>
     * The value of this parameter is expected to be a non-empty string of the
     * following alphabet:
     * </p>
     * <table>
     *     <tr><th>Symbol</th><th>LoggingLevel</th></tr>
     *     <tr><td><tt>A</tt></td><td><tt>ADAPRO_DEBUG</tt></td></tr>
     *     <tr><td><tt>D</tt></td><td><tt>USER_DEBUG</tt></td></tr>
     *     <tr><td><tt>I</tt></td><td><tt>INFO</tt></td></tr>
     *     <tr><td><tt>S</tt></td><td><tt>SPECIAL</tt></td></tr>
     *     <tr><td><tt>W</tt></td><td><tt>WARNING</tt></td></tr>
     *     <tr><td><tt>E</tt></td><td><tt>ERROR</tt></td></tr>
     *     <tr><td><tt>F</tt></td><td><tt>FATAL</tt></td></tr>
     * </table>
     * <p>The symbols can occur in any order in the string, but duplicates are
     * not allowed. For example,</p>
     * <ul>
     *     <li><tt>"DISWEF"</tt> (the default logging mask)</li>
     *     <li><tt>"FWISE"</tt></li>
     *     <li><tt>"F"</tt></li>
     * </ul>
     * <p>are well-formed, whereas</p>
     * <ul>
     *     <li><tt>""</tt></li>
     *     <li><tt>"AA"</tt></li>
     *     <li><tt>"XYZ"</tt></li>
     * </ul>
     * <p>are not.</p>
     *
     * @see ADAPRO::Control::Logger
     * @see ADAPRO::Data::LoggingLevel
     */
    static const parameter_t LOGGING_MASK
    {
        "ADAPRO_LOGGING_MASK",
        "DISWEF",
        [](const std::string& s)
        {
            if (!s.empty())
            {
                std::string copy(s);
                std::sort(copy.begin(), copy.end());
                return std::regex_match(copy, REGEX_LOGGING_MASK);
            }
            else
            {
                return false;
            }
        }
    };

    /**
     * This parameter is used for setting the <tt>DIM_DNS_NODE</tt> when using
     * the DIM server mode. The value range is DNS hostnames and the default
     * value is <tt>"localhost"</tt>.
     */
    static const parameter_t DIM_DNS_NODE
    {
        "ADAPRO_DIM_DNS_NODE",
        "localhost",
        [](const std::string& s)
        {
            return std::regex_match(s, REGEX_HOSTNAME) ||
                    std::regex_match(s, REGEX_IPV4_ADDRESS);
        }
    };

    /**
     * This parameter is used for setting the <tt>DIM_DNS_PORT</tt> when using
     * the DIM server mode. The value range is natural numbers greater than 0
     * and less than 65536 (represented as strings). The default value is
     * <tt>"2505"</tt>.
     */
    static const parameter_t DIM_DNS_PORT
    {
        "ADAPRO_DIM_DNS_PORT",
        "2505",
        [](const std::string& s)
        {
            return std::regex_match(s, REGEX_UNSIGNED) && std::stoi(s) < 65536;
        }
    };

    /**
     * This parameter is used for enabling or disabling the DIM server mode. The
     * range of this parameter contains <tt>"TRUE"</tt> and <tt>"FALSE"</tt>
     * (case sensitive). DIM server mode will be enabled if and only if the
     * value of this parameter is <tt>"TRUE"</tt>.
     */
    static const parameter_t DIM_SERVER_ENABLED
    {
        "ADAPRO_DIM_SERVER_ENABLED",
        "TRUE",
        BOOLEAN
    };

    /**
     * This parameter is used for setting the <tt>DIM_DNS_NODE</tt> when using
     * the DIM server mode. The value range is DNS hostnames and the default
     * value is <tt>"localhost"</tt>.
     */
    static const parameter_t DIM_SERVER_NAME
    {
        "ADAPRO_DIM_SERVER_NAME",
        "ADAPRO",
        [](const std::string& s){return std::regex_match(s, REGEX_WORD);}
    };

    /**
     * This parameter sets the affinity for the main thread (i.e. the one
     * executing <tt>ADAPRO::Control::Session::run</tt>). If the value is a
     * negative integer (represented as a string), then no affinity will be set.
     * The range of this parameter is integers, though the affinity can't be set
     * if the value of this parameter is greater than, or equal to the number of
     * CPU cores available (since their indexing starts from zero). The default
     * value of this parameter is <tt>"-1"</tt>.
     */
    static const parameter_t MAIN_CORE
    {
        "ADAPRO_MAIN_CORE",
        "-1",
        [](const std::string& s){return std::regex_match(s, REGEX_INTEGER);}
    };

    /**
     * This parameter sets the affinity for the ADAPRO Supervisor Thread. If the
     * value is a negative integer (represented as a string), then no affinity
     * will be set. The range of this parameter is integers, though the affinity
     * can't be set if the value of this parameter is greater than, or equal to
     * the number of CPU cores available (since their indexing starts from
     * zero). The default value of this parameter is <tt>"-1"</tt>.
     */
    static const parameter_t SUPERVISOR_CORE
    {
        "ADAPRO_SUPERVISOR_CORE",
        "-1",
        [](const std::string& s){return std::regex_match(s, REGEX_INTEGER);}
    };

    /**
     * This parameter sets the affinity for the DIM callback thread. If the
     * value is a negative integer (represented as a string), then no affinity
     * will be set.  The range of this parameter is integers, though the
     * affinity can't be set if the value of this parameter is greater than, or
     * equal to the number of CPU cores available (since their indexing starts
     * from zero). The default value of this parameter is <tt>"-1"</tt>.
     */
    const parameter_t DIM_CALLBACK_CORE
    {
        "ADAPRO_DIM_CALLBACK_CORE",
        "-1",
        [](const std::string& s){return std::regex_match(s, REGEX_INTEGER);}
    };

    /**
     * This parameter sets the affinity for the ADAPRO Controller Thread. If the
     * value is a negative integer (represented as a string), then no affinity
     * will be set. The range of this parameter is integers, though the affinity
     * can't be set if the value of this parameter is greater than, or equal to
     * the number of CPU cores available (since their indexing starts from
     * zero). The default value of this parameter is <tt>"-1"</tt>.
     */
    static const parameter_t DIM_CONTROLLER_CORE
    {
        "ADAPRO_DIM_CONTROLLER_CORE",
        "-1",
        [](const std::string& s){return std::regex_match(s, REGEX_INTEGER);}
    };

    /**
     * This parameter is used for enabling or disabling serial command
     * propagation mode. In serial mode, Supervisor propagates commands to
     * workers serially, i.e. waiting for each worker to complete its state
     * transition before sending a command to the next worker. The strategy for
     * propagating the commands follows stack logic: The worker that was started
     * first, will be stopped last. This also applies to pausing and resuming:
     * The thread that was started first, will be paused last, but resumed
     * first.
     */
    static const parameter_t SERIALIZE_COMMANDS
    {
        "ADAPRO_SERIALIZE_COMMANDS",
        "TRUE",
        BOOLEAN
    };

    /**
     * A map containing the default configuration of ADAPRO.
     */
    static const ADAPRO::Data::config_t DEFAULT_CONFIGURATION
    {
        {std::get<0>(NICE),                 std::get<1>(NICE)},
        {std::get<0>(LOCK_MEMORY),          std::get<1>(LOCK_MEMORY)},
        {std::get<0>(DAEMON_ENABLED),       std::get<1>(DAEMON_ENABLED)},
        {std::get<0>(DAEMON_LOGFILE),       std::get<1>(DAEMON_LOGFILE)},
        {std::get<0>(LOGGING_MASK),         std::get<1>(LOGGING_MASK)},
        {std::get<0>(DIM_DNS_NODE),         std::get<1>(DIM_DNS_NODE)},
        {std::get<0>(DIM_DNS_PORT),         std::get<1>(DIM_DNS_PORT)},
        {std::get<0>(DIM_SERVER_NAME),      std::get<1>(DIM_SERVER_NAME)},
        {std::get<0>(DIM_SERVER_ENABLED),   std::get<1>(DIM_SERVER_ENABLED)},
        {std::get<0>(MAIN_CORE),            std::get<1>(MAIN_CORE)},
        {std::get<0>(SUPERVISOR_CORE),      std::get<1>(SUPERVISOR_CORE)},
        {std::get<0>(DIM_CALLBACK_CORE),    std::get<1>(DIM_CALLBACK_CORE)},
        {std::get<0>(DIM_CONTROLLER_CORE),  std::get<1>(DIM_CONTROLLER_CORE)},
        {std::get<0>(SERIALIZE_COMMANDS),   std::get<1>(SERIALIZE_COMMANDS)},
    };

    /**
     * This macro is used for generating the ADAPRO default relation.
     *
     * @param x An object of type <tt>param_t</tt>.
     */
#define ADAPRO_RELATION_BRANCH(x) \
    if (key == std::get<0>(x))\
    {\
        return std::get<2>(x)(value);\
    }

    /**
     * The default relation used by ADAPRO to check that its parameters are well
     * typed. It is used as a default parameter of the constuctor of Context.
     *
     * @see ADAPRO::Data::Context
     */
    static const std::function<bool(const std::string&, const std::string&)> DEFAULT_RELATION =
            [](const std::string& key, const std::string& value)
            {
                ADAPRO_RELATION_BRANCH(NICE)
                else ADAPRO_RELATION_BRANCH(LOCK_MEMORY)
                else ADAPRO_RELATION_BRANCH(DAEMON_ENABLED)
                else ADAPRO_RELATION_BRANCH(DAEMON_LOGFILE)
                else ADAPRO_RELATION_BRANCH(LOGGING_MASK)
                else ADAPRO_RELATION_BRANCH(DIM_DNS_NODE)
                else ADAPRO_RELATION_BRANCH(DIM_DNS_PORT)
                else ADAPRO_RELATION_BRANCH(DIM_SERVER_NAME)
                else ADAPRO_RELATION_BRANCH(DIM_SERVER_ENABLED)
                else ADAPRO_RELATION_BRANCH(MAIN_CORE)
                else ADAPRO_RELATION_BRANCH(SUPERVISOR_CORE)
                else ADAPRO_RELATION_BRANCH(DIM_CALLBACK_CORE)
                else ADAPRO_RELATION_BRANCH(DIM_CONTROLLER_CORE)
                else ADAPRO_RELATION_BRANCH(SERIALIZE_COMMANDS)
                else {return true;}
            };

    /**
     * The default (empty) map of signal callbacks.
     */
    static const std::map<uint8_t, std::function<void(void)>> DEFAULT_SIGNAL_CALLBACKS =
            std::map<uint8_t, std::function<void(void)>>{};

    /**
     * The default exit handler, that calls <tt>std::exit</tt>.
     */
    static const std::function<void(const uint8_t)> DEFAULT_EXIT_HANDLER =
            [](const uint8_t code){exit(code);};
}
}

#endif /* ADAPRO_PARAMETERS_HPP */

