/*
 * File:   LoggingLevel.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 27 April 2017, 16:32
 */

#ifndef ADAPRO_LOGGING_LEVEL_HPP
#define ADAPRO_LOGGING_LEVEL_HPP

#include <cstdint>
#include <string>
#include <utility>
#include <regex>
#include <stdexcept>
#include "../library/GenericFunctions.hpp"
#include "../data/Parameters.hpp"

namespace ADAPRO
{
namespace Data
{
    /**
     * Logging level is used for characterising the severity of an event that
     * generates a message using <tt>ADAPRO::Library::Logger</tt>. Events with
     * level <tt>WARNING</tt> and below shouldn't require any action from the
     * user.
     */
    enum LoggingLevel
    {
        /**
         * <tt>ADAPRO_DEBUG</tt> is the lowest logging level. It should be
         * reserved for the use of ADAPRO framework only. Messages of this
         * logging level will be displayed only in the most verbose mode of
         * logging. In BASH environment, the colour of these messages is gray.
         */
        ADAPRO_DEBUG    = 0x01,

        /**
         * Messages with <tt>USER_DEBUG</tt> level are printed to
         * <tt>stdout</tt>. This is the default logging level. In BASH
         * environment, the colour of these messages is gray.
         */
        USER_DEBUG      = 0x02,

        /**
         * Messages with <tt>INFO</tt> will be printed to <tt>stdout</tt>. These
         * messages don't have special highlighting in BASH.
         */
        INFO            = 0x04,

        /**
         * Messages with <tt>WARNING</tt> level are printed to <tt>stdout</tt>.
         * In BASH environment, the colour of these messages is blue. The
         * purpose of this logging level is to specially highlight certain
         * events. This logging level should be reserved for the ADAPRO
         * framework only.
         */
        SPECIAL         = 0x08,

        /**
         * Messages with <tt>WARNING</tt> level are printed to <tt>stdout</tt>.
         * In BASH environment, the colour of these messages is yellow.
         */
        WARNING         = 0x10,

        /**
         * Messages with <tt>ERROR</tt> level are printed to <tt>stderr</tt>.
         * This logging level is used for signaling the occurence of recoverable
         * runtime errors. In BASH environment, the colour of these messages is
         * red.
         */
        ERROR           = 0x20,

        /**
         * Messages with <tt>FATAL</tt> level are printed to <tt>stderr</tt>.
         * This logging level is used for signaling the occurence of fatal
         * runtime errors, that result in abnormal shutdown of the application.
         * In BASH environment, the colour of these messages is red.
         */
        FATAL           = 0x40
    };
}

namespace Library
{
    using namespace ADAPRO::Data;

    /**
     * A simple conversion from LoggingLevels to chars. Returns the first letter
     * in the name of the LoggingLevel in uppercase.
     *
     * @param severity  The logging level.
     * @return          A character symbolizing that level.
     * @throws std::domain error If applied to an invalid value.
     */
    inline char symbol(const LoggingLevel severity)
    {
        switch (severity)
        {
            case ADAPRO_DEBUG:  return 'A';
            case USER_DEBUG:    return 'D';
            case INFO:          return 'I';
            case SPECIAL:       return 'S';
            case WARNING:       return 'W';
            case ERROR:         return 'E';
            case FATAL:         return 'F';
            default:            throw std::domain_error("Invalid LoggingLevel.");
        }
    }

    /**
     * Converts the given string into a logging mask.
     *
     * @param input String representation of the logging mask.
     * @return      Integral representation of the logging mask.
     * @throws std::domain_error If applied to a malformed logging mask string.
     * @see ADAPRO::Data::LOGGING_MASK
     */
    inline uint8_t logging_mask(const std::string& input)
    {
        if (std::get<2>(LOGGING_MASK)(input))
        {
            uint8_t mask(0);
            for (const char x : input)
            {
                switch (x)
                {
                    case 'A': mask |= ADAPRO_DEBUG; break;
                    case 'D': mask |= USER_DEBUG;   break;
                    case 'I': mask |= INFO;         break;
                    case 'S': mask |= SPECIAL;      break;
                    case 'W': mask |= WARNING;      break;
                    case 'E': mask |= ERROR;        break;
                    case 'F': mask |= FATAL;        break;
                }
            }
            return mask;
        }
        else
        {
            throw std::domain_error("Invalid logging mask.");
        }
    }

    /**
     * A simple conversion from <tt>std::string</tt> to
     * <tt>ADAPRO::Data::LoggingLevel</tt>. Used when reading the configuration
     * file. <em>Note that the string equivalent of <tt>ADAPRO_DEBUG</tt> is
     * <tt>"ADAPRO"</tt> and the string equivalent of <tt>USER_DEBUG</tt> is
     * <tt>"DEBUG"</tt>.</em>
     *
     * @param str   String
     * @return      Logging level
     * @throws std::domain_error If the string didn't represent a valid logging
     * level.
     */
    template<> inline LoggingLevel read(const std::string& input)
    {
        if (input == "ADAPRO")        {return ADAPRO_DEBUG;}
        else if (input == "DEBUG")    {return USER_DEBUG;}
        else if (input == "INFO")     {return INFO;}
        else if (input == "SPECIAL")  {return SPECIAL;}
        else if (input == "WARNING")  {return WARNING;}
        else if (input == "ERROR")    {return ERROR;}
        else if (input == "FATAL")    {return FATAL;}
        else
        {
            throw std::domain_error("\"" + input + "\" is not a valid logging"
                " level. (Is it all uppercase?)");
        }
    }
}
}

#endif /* ADAPRO_LOGGING_LEVEL_HPP */

