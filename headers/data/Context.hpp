/*
 * File:   Context.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 17 January 2017, 17:24
 */

#ifndef ADAPRO_CONTEXT_HPP
#define ADAPRO_CONTEXT_HPP

#include <cstdint>
#include <string>
#include <list>
#include <map>
#include <utility>
#include <regex>
#include <memory>
#include <functional>
#include "../control/Logger.hpp"
#include "../library/ConfigurationUtils.hpp"
#ifndef EXCLUDE_DIM
#include "/usr/include/dim/dim_common.h"
#include "../DIM/Typedefs.hpp"
namespace ADAPRO
{
namespace DIM
{
namespace Wrapper
{
    void default_dim_error_handler(int severity, int code, char* message)
    noexcept;
}
}
}
#endif
#include "../data/Parameters.hpp"
#include "../data/LoggingLevel.hpp"
#include "../data/Typedefs.hpp"

namespace ADAPRO
{
namespace Data
{
    /**
     * Context is an immutable struct containing the parameters for
     * <tt>ADAPRO::Control::Session</tt>.
     *
     * @see ADAPRO::Control::Session
     */
    struct Context final
    {
        /**
         * Name of the ADAPRO application that is shown in the environment
         * report.
         */
        const std::string application_name;

        /**
         * Count of command-line arguments provided to the main procedure of the
         * ADAPRO application.
         */
        const int argument_count;

        /**
         * The values of the command-line arguments. If <tt>argument_count</tt>
         * is greater than zero, then the first argument is interpreted as a
         * configuration path with the highest priority.
         */
        const char** const argument_values;

        /**
         * Worker factories are functions returning
         * <tt>unique_ptr&ltADAPRO::Control::Configurable;&gt;</tt> objects.
         * They are used by <tt>ADAPRO::Control::Supervisor</tt> to initialize
         * the user-implemented <tt>Configurable</tt>s.
         *
         * @see ADAPRO::Control::Configurable
         * @see ADAPRO::Control::Supervisor
         */
        const std::list<worker_factory_t> worker_factories;

        /**
         * List of the runtime search paths for the configuration used for
         * creating the <tt>Configurable</tt> Threads implemented by the ADAPRO
         * application.
         *
         * @see ADAPRO::Library::import_configuration
         */
        const std::list<std::string> configuration_paths;

        /**
         * The initial configuration for the ADAPRO Session. The final
         * configuration of the Session will be a copy of this configuration,
         * where all the key-value pairs that also exist in the configuration
         * file, are replaced with their counterparts in the said file.
         *
         * @see ADAPRO::Library::import_configuration
         * @see ADAPRO::Data::Context::configuration_paths
         * @see ADAPRO::Control::Session
         */
        const ADAPRO::Data::config_t initial_configuration;

        /**
         * <p>If <tt>true</tt>, during startup sequence, Session will attempt
         * updating its configuration using the first accessible configuration
         * file found using <tt>configuration_paths</tt>.</p>
         * <p>If the application doesn't use a configuration file, then it must
         * provide at least all the parameters included in
         * <tt>ADAPRO::Data::DEFAULT_CONFIGURATION</tt> as the
         * <tt>initial_configuration</tt> and set this field to <tt>false</tt>
         * (by using the Context constructor).
         */
        const bool update_configuration;

        /**
         * If <tt>false</tt>, then a key found in the configuration file, but
         * not in the default configuration, generates a runtime exception,
         * which then causes <tt>ADAPRO::Control::main</tt> to abort with a
         * status code indicating the scenario.
         *
         * @see ADAPRO::Library::import_configuration
         */
        const bool allow_new_keys;

        /**
         * The relation that the configuration file needs to satisfy. By
         * default, this is simply the binary cartesian product of strings,
         * implying that every well-formed key-value pair is accepted.
         *
         * @see ADAPRO::Library::import_configuration
         */
        const std::function<bool(const std::string&, const std::string&)> relation;

        /**
         * <p>A map of POSIX signals and their corresponding callback functions.
         * A callback function identified by the signal will be invoked when
         * ADAPRO signal hander for the particular signal is activated. After
         * control returns from the callback, ADAPRO Supervisor starts
         * propagating stop commands to workers.</p>
         * <p><em>The purpose of a signal callback is not trying to recover from
         * the exceptional situation, but rather give the user a chance to warn
         * external systems of the fact that the ADAPRO application is about to
         * terminate. A signal callback function must not terminate the
         * application, nor free any memory that might be still used during a
         * graceful shutdown sequence. The signal callback will be invoked
         * synchronously, i.e. it blocks the shutdown sequence.
         * </em></p>
         * <p>Currently, the following signals are supported: <tt>SIGSEGV</tt>,
         * <tt>SIGABRT</tt>, <tt>SIGINT</tt>, and <tt>SIGHUP</tt>. For more
         * information on signal handling, see <tt>man signal</tt>.</p>
         */
        const std::map<uint8_t, std::function<void(void)>> signal_callbacks;

        /**
         * The exit handler will be invoked at the end of forceful shutdown
         * sequence. (After a graceful shutdown, Session returns a status code.)
         *
         * @see ADAPRO::Control::Session::run
         */
        const std::function<void(const uint8_t)> exit_handler;

#ifndef EXCLUDE_DIM
        /**
         * <p>A list of callback functions returning tuples containing
         * information of user-defined DIM command services that ADAPRO will
         * publish. The components of the tuples are:</p>
         * <ol>
         *     <li>Name of the service</li>
         *     <li>DIM service description (e.g. <tt>"I:1"</tt>)</li>
         *     <li>The callback</li>
         * </ol>
         */
        const std::list<ADAPRO::DIM::command_factory_t> dim_command_factories;

        /**
         * The DIM client error callback.
         */
        const ADAPRO::DIM::error_callback_t dim_client_error_handler;

        /**
         * The DIM server error callback.
         */
        const ADAPRO::DIM::error_callback_t dim_server_error_handler;
#endif

        /**
         * The deleted default constructor for Context.
         */
        Context() = delete;

        /**
         * The constructor for Context.
         *
         * @param application_name      Name of the ADAPRO application that is
         * shown in the environment report.
         * @param argument_count        Number of command-line arguments given.
         * @param argument_values       Values of the command-line arguments.
         * @param worker_factories      Worker factories. Supervisor uses them
         * for creating the workers.
         * @param configuration_paths   The paths where to search for the
         * configuration file.
         * @param initial_configuration Initial configuration. The final
         * configuration of the Session is determined based on the initial
         * configuration and, if <tt>update_configuration</tt> is true, the
         * contents of the configuration file, with the configuration file
         * having priority.
         * @param update_configuration  If <tt>true</tt>, Session will update
         * its configuration using a configuration file during startup sequence.
         * (The possibility for bypassing the configuration file was introduced
         * in ADAPRO 4.0.0.)
         * @param allow_new_keys        If <tt>false</tt>, then keys contained
         * in the configuration file, but not in the default configuration will
         * generate runtime exceptions.
         * @param relation              Relation used for performing sanity
         * checks for the configuration keys.
         * @param signal_callbacks      Callbacks that will be executed at the
         * beginning of ADAPRO signal handlers. <em>These callback functions
         * must not invalidate the objects owned by ADAPRO (such as worker
         * Threads).</em>
         * @param dim_command_factories A list of factories for callback
         * functions to be registered as DIM command services. (Effective when
         * using DIM server mode.) The components of the tuples are: Name of the
         * service, DIM service description (e.g. <tt>"I:1"</tt>) and the
         * callback.
         * @param dim_client_error_handler_factory_t A factory for registering a
         * DIM client error handler. Takes the ADAPRO Session configuration as a
         * parameter.
         * @param dim_server_error_handler_factory_t A factory for registering a
         * DIM server error handler. Takes the ADAPRO Session configuration as a
         * parameter.
         *
         * @see ADAPRO::Control::Supervisor
         * @see ADAPRO::Library::import_dpids
         */
        Context
        (
                const std::string& application_name,
                const int argument_count,
                const char** const argument_values,
                const std::list<ADAPRO::Data::worker_factory_t>& worker_factories,
                const std::list<std::string>& configuration_paths,
                const ADAPRO::Data::config_t& initial_configuration =
                        DEFAULT_CONFIGURATION,
                const bool update_configuration = true,
                const bool allow_new_keys = true,
                const std::function<bool(const std::string&, const std::string&)>& relation =
                        DEFAULT_RELATION,
                const std::map<uint8_t, std::function<void(void)>>& signal_callbacks =
                        DEFAULT_SIGNAL_CALLBACKS,
                const std::function<void(const uint8_t)>& exit_handler =
                        DEFAULT_EXIT_HANDLER
#ifndef EXCLUDE_DIM
                ,
                const std::list<ADAPRO::DIM::command_factory_t>& dim_command_factories =
                    std::list<ADAPRO::DIM::command_factory_t>{},
                const ADAPRO::DIM::error_callback_t& dim_client_error_handler
                    = ADAPRO::DIM::Wrapper::default_dim_error_handler,
                const ADAPRO::DIM::error_callback_t& dim_server_error_handler
                    = ADAPRO::DIM::Wrapper::default_dim_error_handler
#endif
        )
        noexcept:
                application_name(application_name),
                argument_count(argument_count),
                argument_values(argument_values), // Maybe I should memcpy this.
                worker_factories(worker_factories),
                configuration_paths(configuration_paths),
                initial_configuration(initial_configuration),
                update_configuration(update_configuration),
                allow_new_keys(allow_new_keys),
                relation(relation),
                signal_callbacks(signal_callbacks),
                exit_handler(exit_handler)
#ifndef EXCLUDE_DIM
                , dim_command_factories(dim_command_factories),
                dim_client_error_handler(dim_client_error_handler),
                dim_server_error_handler(dim_server_error_handler)
#endif
        {}

        /**
         * The copy constructor for Context.
         *
         * @param other The Context to be moved.
         */
        Context(Context& other) = default;

        /**
         * The move constructor for Context.
         *
         * @param other The Context to be moved.
         */
        Context(Context&& other) = default;

        ~Context() noexcept {}

    };
}
}

#endif /* ADAPRO_CONTEXT_HPP */

