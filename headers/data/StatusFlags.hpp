
/*
 * File:   StatusFlags.hpp
 * Author: john Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 February 2017, 11:36
 */

#ifndef ADAPRO_STATUS_FLAGS_HPP
#define ADAPRO_STATUS_FLAGS_HPP

#include <cstdint>

namespace ADAPRO
{
namespace Data
{
    /**
     * Status flag for malformed configuration file.
     */
    static constexpr uint8_t FLAG_BAD_CONFIG = 0x01;

    /**
     * Status flag for unhandled exceptions.
     */
    static constexpr uint8_t FLAG_UNHANDLED_EXCEPTION = 0x02;

    /**
     * Status flag for user interrupt.
     */
    static constexpr uint8_t FLAG_SIGINT = 0x04;

    /**
     * Status flag for the hang up signal.
     */
    static constexpr uint8_t FLAG_SIGHUP = 0x08;

    /**
     * Status flag for segmentation violation.
     */
    static constexpr uint8_t FLAG_SIGSEGV = 0x10;

    /**
     * Status flag for abort signal.
     */
    static constexpr uint8_t FLAG_SIGABRT = 0x20;

    /**
     * Status flag for timed out shutdown. This flag is masked into the
     * process exit code when Supervisor fails to halt in one minute after
     * receiving an interrupt from the user.
     *
     * @see ADAPRO::Control::Supervisor::get_status_code
     * @see ADAPRO::Control::main
     * @see ADAPRO::Control::default_SIGINT_handler
     */
    static constexpr uint8_t FLAG_TIMEOUT = 0x40;

    /**
     * Status flag for the death of one or more Worker Threads.
     */
    static constexpr uint8_t FLAG_WORKER = 0x80;
}
}

#endif /* ADAPRO_STATUS_FLAGS_HPP */

