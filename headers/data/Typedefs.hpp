/*
 * File:   Typedefs.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * This file contains the common typedefs used in this project.
 *
 * Created on 20 July 2015, 9:15
 */

#ifndef ADAPRO_DATA_TYPEDEFS_HPP
#define ADAPRO_DATA_TYPEDEFS_HPP

#include <cstdint>
#include <functional>
#include <memory>
#include <map>
#include <utility>
#include <string>
//#include "../control/Logger.hpp"
#include "../control/AlignedAllocator.hpp"
#include "../data/State.hpp"
#ifndef EXCLUDE_DIM
#include "/usr/include/dim/dim_common.h"
#include "../DIM/SubscriptionType.hpp"
#endif

namespace ADAPRO
{
namespace Control
{
    class Logger;
    class Worker;
    class Supervisor;
}
}

/**
 * This is the main namespace of the Alice Data Point Processing (ADAPRO)
 * application framework.
 */
namespace ADAPRO
{
/**
 * This namespace contains the data types of ADAPRO.
 */
namespace Data
{
    /**
     * Type of the transition callback used by <tt>ADAPRO::Control::Thread</tt>.
     * @see ADAPRO::Control::Thread
     */
    using trans_cb_t = std::function<void(const ADAPRO::Data::State)>;

    /**
     * Type of the configuration that Configurable and some library procedures
     * accept. Configuration is essentially a key-value map of pairs of strings
     * (<tt>std::map&lt;std::string, std::string&gt;</tt>).
     */
    using config_t = std::map<std::string, std::string>;

    /**
     * <p>The type of an ADAPRO parameter, which is a triple with the following
     * components:</p>
     * <ol>
     *     <li>Name of the parameter that can be included in a configuration
     *     file. Exact, case sensitive comparison will be performed.</li>
     *     <li>Default value of the parameter. Also case sensitive.</li>
     *     <li>A predicate that can be used for checking whether or not a given
     *     value is in the range of this parameter. This function will return
     *     <tt>true</tt> if and only if this is the case.</li>
     * </ol>
     * <p>For example:</p>
     * <pre>
     *  const parameter_t SUPERVISOR_CORE("ADAPRO_SUPERVISOR_CORE", "1",
     *       [](const std::string s){return std::regex_match(s, std::regex("-?[0-9]+"));});
     * </pre>
     */
    using parameter_t = struct std::tuple<std::string, std::string,
            std::function<bool(const std::string&)>>;

    /**
     * Type of a worker factory, i.e. a function taking a const reference to a
     * <tt>config_t</tt>  (that is
     * <tt>std::map&lt;std::string, std::string&gt;</tt>) and a reference to a
     * <tt>ADAPRO::Control::Supervisor</tt>, and returning a
     * <tt>std::unique_ptr&lt;Configurable&gt;</tt>
     */
    using worker_factory_t = std::function<std::unique_ptr<ADAPRO::Control::Worker>(
            ADAPRO::Control::Logger&, ADAPRO::Control::Supervisor&, const config_t&)>;
}
}

#endif /* ADAPRO_DATA_TYPEDEFS_HPP */

