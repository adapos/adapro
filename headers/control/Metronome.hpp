
/*
 * File:   Metronome.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 November 2016, 13:43
 */

#ifndef ADAPRO_METRONOME_HPP
#define ADAPRO_METRONOME_HPP

#include <cstdint>
#include <string>
#include <chrono>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "Thread.hpp"
#include "Logger.hpp"

class MetronomeTest;
class MetronomeFactoryTest;

namespace ADAPRO
{
namespace Control
{
    /**
     * Metronome is a simple (Thread) object that wakes up periodically and
     * wakes up every thread that have called its member function
     * <tt>synchronize</tt>. An instance of this class can be used for
     * low-frequency temporal synchronization of tasks with low importance.
     *
     * @see Metronome::synchronize
     * @see ADAPRO::Library::get_metronome
     */
    class Metronome final: private Thread
    {
        friend MetronomeTest;
        friend MetronomeFactoryTest;

        /**
         * A serial number used for generating unique names for Metronome
         * instances.
         */
        static uint32_t S_NO;

        /**
         * The internal mutex object of Metronome.
         */
        std::mutex semaphore;

        /**
         * The internal condition variable of Metronome.
         */
        std::condition_variable barrier;

    protected:
        virtual void prepare() override;
        virtual void execute() override;
        virtual void finish() override;
        virtual void handle_exception(const char* message) noexcept override;

    public:

        /**
         * Duration of a single tick of the Metronome.
         */
        const chrono::milliseconds tick;

        /**
         * The constructor for Metronome. If a fresh new instance is not needed,
         * consider using Metronome factory (ADAPRO::Library::get_metronome).
         *
         * @param logger            The Logger instance of an ADAPRO Session.
         * @param tick_length       Minimum duration of a single tick in
         * milliseconds. The default value is <tt>1000</tt>.
         * @param preferred_core    Preferred CPU core number. A negative value
         * indicates no preference. For more information, see
         * <tt>ADAPRO::Control::Thread</tt>.
         * @see ADAPRO::Control::Thread
         * @see ADAPRO::Library::get_metronome
         */
        Metronome
        (
                Logger& logger,
                const uint32_t tick_length = 1000,
                const int preferred_core = -1
        )
        noexcept;

        /**
         * Returns control after the calling thread has been waken up the given
         * number of times. This method is protected against spurious wakeups
         * when Metronome is <tt>RUNNING</tt>. When Metronome stops  (e.g. is
         * being destroyed), it wakes up all threads that called this method,
         * regardless of the time passed. This method cannot guarantee any
         * specific maximum sleep duration.
         *
         * @param ticks How many times the tick duration to wait before
         * returning control to the caller. The default value is 1.
         * @see ADAPRO::Control::Thread::start
         * @see ADAPRO::Data::State
         */
        void synchronize(const uint32_t ticks = 1);

        /**
         * Virtual destructor for Metronome. Takes care of calling
         * <tt>ADAPRO::Control::Thread::stop</tt> in order to segmentation
         * violations (see issue #5).
         */
        virtual ~Metronome() noexcept;
    };
}
}

#endif /* ADAPRO_METRONOME_HPP */

