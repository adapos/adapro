/*
 * File:   Thread.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 July 2015, 17:25
 */

#ifndef ADAPRO_THREAD_HPP
#define	ADAPRO_THREAD_HPP

#include <thread>
#include <memory>
#include <functional>
#include <mutex>
#include <chrono>
#include <condition_variable>
#include <atomic>
#include <string>
#include <list>
#include <stdexcept>
#include "../library/GenericFunctions.hpp"
#include "../data/State.hpp"
#include "../data/Command.hpp"
#include "../data/LoggingLevel.hpp"
#include "../data/Typedefs.hpp"
#include "Logger.hpp"

using namespace std;

class SupervisorTest;

namespace ADAPRO
{
namespace Control
{
    class Supervisor;

    /**
     * <p>
     * Thread is a finite state machine executing independently and
     * asynchronously a predefined task of indeterminate duration. For a simple
     * one-time execution of arbitrary code, consider using standard library
     * threads instead. The behaviour of the Thread depends on its task (i.e.
     * implementing class), state, and command. A Thread must not throw
     * exceptions, but abort execution instead and set the state to
     * <tt>ABORTED</tt> should an unrecoverable runtime error occur.
     * </p>
     * <p>
     * The life cycle of a Thread in terms of states typically goes as follows:
     * <tt>READY</tt>, <tt>STARTING</tt>, <tt>RUNNING</tt>, <tt>STOPPING</tt>
     * (<tt>ABORTING</tt>), <tt>STOPPED</tt> (<tt>ABORTED</tt>). Immediately
     * after construction, the Thread is in the state <tt>READY</tt>, which
     * means it's waiting for the command <tt>START</tt>. Invoking the method
     * <tt>start</tt> sends the command <tt>START</tt> and sets the state to
     * <tt>STARTING</tt>. In this state, the Thread is executing some
     * preliminary actions (like memory allocation) that need to be done before
     * starting the actual computational task. These preliminary actions are
     * defined in the method <tt>prepare</tt>. When the Thread is executing its
     * computational task (the actual algorithm producing the desired side
     * effects on memory), it's in the state <tt>RUNNING</tt> with command set
     * to <tt>CONTINUE</tt>. This computational task is defined in the method
     * <tt>execute</tt>. The execution (i.e. invoking <tt>execute</tt>)
     * continues in a loop for as long as the command remains unchanged. When
     * the command is set to <tt>STOP</tt> by invoking the method stop, the
     * Thread begins a shutdown sequence which in state <tt>STOPPING</tt>. When
     * all of the computation has ended, the Thread is in state
     * <tt>STOPPED</tt> (or <tt>ABORTED</tt>). At this point, the Thread is
     * ready to be deleted.
     * </p>
     * <p>
     * Any state, other than <tt>READY</tt>, <tt>STOPPED</tt>, and
     * <tt>ABORTED</tt>, can also be aborted by a runtime exception or call to
     * the <tt>abort</tt> by another thread, or <tt>trap</tt> from inside one of
     * the virtual hook methods (<tt>prepare</tt>, <tt>execute</tt>, and
     * <tt>finish</tt> of the Thread itself. When the abortion sequence begins,
     * Thread sets it state to <tt>ABORTING</tt> and prints an error message,
     * after which it halts in the state <tt>ABORTED</tt>.
     * </p>
     * <p>
     * <em>Known issue: The destructors for <tt>Thread</tt> or any of its
     * inheriting classes are only safe to call when the Thread is in one of the
     * states <tt>READY</tt>, <tt>STOPPED</tt>, or <tt>ABORTED</tt>.</em> In
     * other cases, a <tt>SIGSEGV</tt> or <tt>SIGABRT</tt> might occur. This
     * issue will not be fixed. The easiest way to circumvent this problem is to
     * add <tt>stop(false);</tt> in the virtual destructor of the final
     * inheriting class.
     * </p>
     * <p>
     * <em>The framework user should not create Threads before the ADAPRO
     * session has been started.</em> It is recommended to use
     * <tt>ADAPRO::Control::Worker</tt> as the basis of user-defined Threads,
     * and register Worker instances to the framework via Worker factories given
     * to <tt>ADAPRO::Data::Context</tt>. A Worker registered to the framework
     * can safely create more Threads. In this case, it's recommended to use
     * <tt>STATE_PROPAGATION_CB</tt> to create a transition callback to
     * propagate state transitions to the Threads owned by the Worker.
     * </p>
     * <p>
     * For more information on Thread, please consult the Technical
     * Documentation.
     * </p>
     * @see ADAPRO::Control::Thread::stop
     * @see ADAPRO::Control::Thread::STATE_PROPAGATION_CB
     * @see ADAPRO::Control::Worker
     * @see ADAPRO::Control::Session
     * @see ADAPRO::Data::Context
     */
    class Thread
    {
        friend Supervisor;
        friend SupervisorTest;

        /**
         * Number of available CPU cores in the system.
         */
        static const size_t CORE_COUNT;

        /**
         * Reference to the <tt>ADAPRO::Control::Logger</tt> instance used in
         * the current ADAPRO session. It is valid during the whole lifetime of
         * the Thread.
         */
        Logger& logger;

        /**
         * The mutex object used for ensuring mutual exclusion when accessing
         * the class member <tt>state</tt>.
         */
        mutex state_mutex;

        /**
         * The internal condition variable of the Thread used for barrier
         * synchronization.
         */
        condition_variable semaphore;

        /**
         * The state of the Thread.
         */
        atomic<ADAPRO::Data::State> state;

        /**
         * The command of the Thread.
         */
        atomic<ADAPRO::Data::Command> command;

        /**
         * This callback is executed every time the Thread enters into a new
         * state.
         */
        const ADAPRO::Data::trans_cb_t transition_callback;

        /**
         * This field is used for a watchdog mechanism for the Worker class. For
         * technical reasons, it's included in Thread class, even if it's
         * redundant for non-Worker Threads. The value is <tt>true</tt> if and
         * only if the Worker is alive. This value will be set to <tt>false</tt>
         * periodically by the Supervisor.
         *
         * @see ADAPRO::Control::Suprevisor
         */
        atomic_bool responsive;

        /**
         * <tt>true</tt> if and only if <tt>pthread_join</tt> has been invoked.
         */
        atomic_bool joined;

        /**
         * Unique pointer to the <tt>std::thread</tt> backend object.
         */
        unique_ptr<thread> worker_ptr;

        /**
         * This function is passed as a callback to the std::thread constructor.
         * It carries out the internal Thread control logic.
         */
        void run() noexcept;

        /**
         * Thread's internal setter for state.
         *
         * @param new_state     The new state value.
         */
        inline void set_state(const ADAPRO::Data::State new_state) noexcept
        {
            state.store(new_state, memory_order_release);
            semaphore.notify_all();
        }

        /**
         * Thread's internal setter for command.
         *
         * @param new_command   The new command value.
         */
        inline void set_command(const ADAPRO::Data::Command new_command) noexcept
        {
            command.store(new_command, memory_order_release);
        }

        /**
         * Makes sure that the backend <tt>std::thread</tt> object has joined.
         * This method is used when stopping a thread synchronously.
         */
        void handle_join() noexcept;

    protected:

        /**
         * This transition callback does nothing, hence the acronym for "No
         * OPeration CallBack".
         */
        static const ADAPRO::Data::trans_cb_t NOP_CB;

        /**
         * Returns a transition callback that propagates the state of the Thread
         * to the subordinate Threads synchronously. <em>The callback function
         * assumes that the subordinates and the calling Thread have all
         * synchronized states and does no state checking. The behaviour of the
         * callback in case of any error is undefined.</em>
         *
         * @param subordinates  The subordinate Threads.
         * @return              A transition callback.
         */
        static ADAPRO::Data::trans_cb_t STATE_PROPAGATION_CB(
                std::list<Thread*>&& subordinates) noexcept;

        /**
         * The current error message, that will be printed to <tt>stderr</tt>
         * when the exception handler starts.
         */
        char* error_msg;

        /**
         * Constructor for Thread.
         *
         * @param logger                The logger used for the ADAPRO Session
         * that this Thread will be part of.
         * @param name              The name (identifier) of the Thread.
         * @param preferred_core    <p>Number of the preferred CPU core.</p>
         * <p>If set to a non-negative value that is less than the number of
         * available CPU cores, then Thread attempts to set its affinity to this
         * core number when starting. Also, in this case Thread attempts to set
         * its scheduling policy to FIFO with maximum priority to ensure maximal
         * CPU time.</p>
         * <p>
         * If the value of this parameter is outside the range specified above,
         * then the Thread object gets the standard treatment from the Operating
         * System.
         * </p>
         */
        Thread
        (
                Logger& logger,
                const string&& name,
                const int preferred_core = -1
        )
        noexcept;

        /**
         * Constructor for Thread.
         *
         * @param logger                The logger used for the ADAPRO Session
         * that this Thread will be part of.
         * @param name                  The name (identifier) of the Thread.
         * @param transition_callback   A callback to be executed every time the
         * state of the Thread changes. The argument of the callback will be the
         * new state of the Thread.
         * @param preferred_core        <p>Number of the preferred CPU core.</p>
         * <p>If set to a non-negative value that is less than the number of
         * available CPU cores, then Thread attempts to set its affinity to this
         * core number when starting. Also, in this case Thread attempts to set
         * its scheduling policy to FIFO with maximum priority to ensure maximal
         * CPU time.</p>
         * <p>
         * If the value of this parameter is outside the range specified above,
         * then the Thread object gets the standard treatment from the Operating
         * System.
         * @since 0.7.0
         */
        Thread
        (
                Logger& logger,
                const string&& name,
                const ADAPRO::Data::trans_cb_t transition_callback,
                const int preferred_core = -1
        )
        noexcept;

        /**
         * <p>Throws a <tt>std::exception</tt> and updates
         * <tt>error_message</tt> to point to the given C-style error message.
         * If the exception is not handled locally, then the Thread aborts and
         * calls <tt>handle_exception</tt>.
         *
         * @param message           The error message.
         * @throws  std::exception
         * @see ADAPRO::Control::Thread::abort
         * @see ADAPRO::Control::Thread::handle_exception
         */
        void trap(const char* message);

        /**
         * Ibid.
         *
         * @param message   The error message as a <tt>std::string</tt>
         * instance.
         */
        void trap(const std::string& message);

        /**
         * <p>Prints the given message with the given logging level, if it is
         * greater than or equal to the configured logging level of the ADAPRO
         * session. The Logger instance of the ADAPRO session is used for
         * output.</p>
         * <p><em>This function is meant to be used only from inside the Thread,
         * that is, from a method, whose call stack includes <tt>prepare</tt>,
         * <tt>execute</tt>, <tt>finish</tt>, or <tt>handle_exception</tt>.</em>
         * The purpose of this arrangment is to have the Thread shown as the
         * origin of the message, to correspond with the actual runtime thread
         * calling this method. This semantic condition cannot be enforced by
         * the C++ type system, so it's on user's responsibility to honour it.
         * </p>
         *
         * @param message   The message.
         * @param severity  The logging level.
         * @see ADAPRO::Control::Logger
         */
        inline void print
        (
                const string message,
                ADAPRO::Data::LoggingLevel severity =
                        ADAPRO::Data::LoggingLevel::USER_DEBUG
        )
        noexcept
        {
            logger.print(*this, message, severity);
        }

        /**
         * Performs the required preparations before starting to execute the
         * main task of the Thread.
         */
        virtual void prepare() = 0;

        /**
         * Performs one computational step. This method will be called
         * repeatedly as long as the command of the Thread remains CONTINUE. If
         * an error or exception occures during runtime, the method abort must
         * be called.
         *
         * @see Thread::abort
         */
        virtual void execute() = 0;

        /**
         * Executes the required actions after the execution of the main task
         * has ceased.
         */
        virtual void finish() = 0;

        /**
         * This method is executed in case of an error or exception and is
         * supposed to perform the proper operations required for continuing the
         * execution of other Threads. This method needs to have a
         * <tt>noexcept</tt> guarantee.
         *
         * @param message An error message as a null-terminated <tt>char</tt>
         * array.
         */
        virtual void handle_exception(const char* message) noexcept;

    public:

        /**
         * Name of the Thread. Used for logging.
         */
        const string name;

        /**
         * Number of the preferred CPU core. If this number is between zero
         * (inclusive) and the number of available CPU cores (exclusive), Thread
         * attempts to set its affinity to this core when starting. Also, the
         * scheduling policy for the Thread will be set to FIFO for ensuring
         * maximal CPU time.
         */
        const int preferred_core;

        /**
         * The deleted default constructor.
         */
        Thread() noexcept = delete;

        /**
         * <p>Aborts the execution of the Thread with the given message.
         * <em>Calling This procedure doesn't stop the execution
         * immediately</em>. It triggers the abortion sequence once control
         * returns to the Thread. If the execution needs to be stopped
         * immediately, calling <tt>trap</tt> from one of the four protected
         * virtual methods is a better option.</p>
         * <p>For example, if this procedure is called inside <tt>execute</tt>,
         * the abortion sequence begins as soon as a <tt>return</tt> statement
         * gets executed (or the end of the block is reached).</p>
         *
         * @param message   Reason for aborting as a null-terminated char array.
         * @see ADAPRO::Control::Thread::trap
         */
        void abort(const char* message) noexcept;

        /**
         * Ibid.
         *
         * @param message   The error message as a <tt>std::string</tt>
         * instance.
         */
        void abort(const std::string& message) noexcept;

        /**
         * This blocking method returns control to the caller once the Thread
         * has entered to the given state or a later state. Note that there's no
         * guarantee that this will ever happen.
         */
        void wait_for_state(const ADAPRO::Data::State target_state) noexcept;

        /**
         * This blocking method returns control to the caller once the Thread
         * has entered to the given state or a later state. If the Thread fails
         * to do so before the timeout is reached, an exception is thrown.
         *
         * @param timeout Maximum time for blocking.
         * @throws std::runtime_error If the Thread failed to perform the
         * desired state transition before timeout.
         */
        void wait_for_state(const ADAPRO::Data::State target_state,
                const chrono::milliseconds timeout);

        /**
         * Like <tt>wait_for_state</tt> for a single state, but accepts a state
         * mask. This method returns control to the caller only after the Thread
         * has entered a state <tt>s</tt> for which <tt>state_in(s)</tt> returns
         * <tt>true</tt> (which might never happen).
         *
         * @param state_mask    A bit mask representing a selection of
         * <tt>ADAPRO::Data::State</tt> values. When (or if) the Thread enters
         * one of these states, then this method returns control to the caller.
         *
         * @see ADAPRO::Data::State
         * @see ADAPRO::Control::Thread::state_in
         */
        void wait_for_state_mask(const uint8_t state_mask) noexcept;

        /**
         * Like <tt>wait_for_state</tt> for a single state, but accepts a state
         * mask. This method returns control to the caller only after the Thread
         * has entered a state <tt>s</tt> for which <tt>state_in(s)</tt> returns
         * <tt>true</tt> (which might never happen). This method also has a
         * timeout parameter for guarding against infinite waiting.
         *
         * @param state_mask    A bit mask representin a selection of
         * <tt>ADAPRO::Data::State</tt> values. When (or if) the Thread enters
         * one of these states, then this method returns control to the caller.
         * @param timeout       A timeout period, after which an exception will
         * be thrown if the Thread hasn't moved to any of the masked states.
         * @throws std::runtime_error If the Thread failed to perform the
         * desired state transition before timeout.
         */
        void wait_for_state_mask(const uint8_t state_mask,
                const chrono::milliseconds timeout);

        /**
         * Returns the current state of the Thread.
         *
         * @return A state.
         * @see State.hpp
         */
        inline ADAPRO::Data::State get_state() const noexcept
        {
            return state.load(memory_order_consume);
        }

        /**
         * Returns the current command of the Thread.
         *
         * @return A command.
         * @see Command.hpp
         */
        inline ADAPRO::Data::Command get_command() const noexcept
        {
            return command.load(memory_order_consume);
        }

        /**
         * Compares the current state of the Thread against the given bit mask
         * using bitwise AND. For example, for Thread <tt>t</tt> in state
         * <tt>RUNNING</tt>, it holds that
         * <tt>t.state_in(READY | STOPPED) == false</tt>, but
         * <tt>t.state_in(RUNNING) == true</tt>.
         *
         * @param state_mask    A bit mask of states.
         * @return              <tt>true</tt> if and only if this Thread is in
         * one of the masked states.
         */
        inline bool state_in(const uint8_t state_mask) const noexcept
        {
            return (get_state() & state_mask) > 0;
        }

        /**
         * Compares the current command of the Thread against the given bit mask
         * using bitwise AND. For example, for Thread <tt>t</tt> with command
         * <tt>START</tt>, it holds that
         * <tt>t.command_in(CONTINUE | PAUSE) == false</tt>, but
         * <tt>t.command_in(START) == true</tt>.
         *
         * @param state_mask    A bit mask of commands.
         * @return              <tt>true</tt> if and only if the current command
         * of this Thread is one of the masked commands.
         */
        inline bool command_in(const uint8_t command_mask) const noexcept
        {
            return (get_command() & command_mask) > 0;
        }

        /**
         * Starts the execution of the Thread. The behaviour of the Thread
         * depends on its task and parameter given in the constructor. If this
         * function is called more than once, the Thread aborts. Also note that
         * this method is asynchronous (i.e. non-blocking).
         *
         * @param wait Whether or not to wait until the Thread has fully started
         * or moved to the state <tt>ABORTING</tt>.
         */
        void start(const bool wait = false) noexcept;

        /**
         * Causes the Thread to temporarily stop executing its task and move to
         * the state <tt>PAUSED</tt>. In this state, the thread can either be
         *
         * @param wait Whether or not to wait until the Thread has fully paused
         * or moved to the state <tt>ABORTING</tt>.
         *
         * @see ADAPRO::Control::Thread::resume
         */
        void pause(const bool wait = false) noexcept;

        /**
         * Continues the execution of the Thread. This command only has effect
         * when called on a Thread that is <tt>PAUSED</tt>, in which case the
         * Thread moves back to <tt>RUNNING</tt> and resumes its compuational
         * task.
         *
         * @param wait Whether or not to wait until the Thread has moved back to
         * state <tt>RUNNING</tt>, or moved to the state <tt>ABORTING</tt>.
         *
         * @see ADAPRO::Control::Thread::pause
         */
        void resume(const bool wait = false) noexcept;

        /**
         * Signals the Thread to stop executing its task if it hadn't received
         * this signal already. <em>The behaviour of this method is
         * undefined if the Thread tries to call this itself.</em> In this
         * scenario, there will probably be a deadlock or a fatal runtime error.
         *
         * @param join This procedure will be blocking if and only if this
         * variable is set <tt>true</tt>.
         */
        void stop(const bool join = true) noexcept;

        /**
         * Returns a report that can be used for debugging purposes. The default
         * report contains the name, state, and command of the Thread.
         *
         * @return A string.
         */
        virtual string report() noexcept
        {
            // Perhaps I could use new lines and indentation...
            return name + ": state = " + ADAPRO::Library::show(get_state()) +
                    "; command = " + ADAPRO::Library::show(get_command());
        }

        /**
         * Virtual destructor for the Thread. Note that calling a destructor for
         * a Thread that is not in any of the states <tt>READY</tt>,
         * <tt>STOPPED</tt>, or <tt>ABORTED</tt>, leads to a <tt>SIGSEGV</tt>,
         * <tt>SIGABRT</tt> or other fatal runtime situation. This is because
         * the memory allocations belonging to Thread are not available anymore
         * to the backend <tt>std::thread</tt> object. <em>It is user's
         * responsibility to make sure that the destructor for <tt>Thread</tt>
         * (or any of its subclasses) never gets called during these states.
         * </em>
         */
        virtual ~Thread() noexcept;

        /**
         * Causes the caller to spend at least the given number of nanoseconds
         * in a spinlock. The actual performance of this function will depend on
         * the runtime environment, naturally. <em>Waiting in a spinlock
         * consumes CPU cycles that could be allocated to other threads.</em>
         * This function should only be used in situations where the granularity
         * of <tt>std::this_thread::sleep_for</tt> is insufficient.
         *
         * @param interval  The minimum amount of time to be spent in a
         * spinlock.
         */
        inline static void SPINLOCK(const chrono::nanoseconds interval) noexcept
        {
            chrono::nanoseconds waited(0);
            chrono::high_resolution_clock::time_point beginning =
                    chrono::high_resolution_clock::now();
            while (waited < interval)
            {
                waited = chrono::duration_cast<std::chrono::nanoseconds>(
                        chrono::high_resolution_clock::now() - beginning
                );
            }
        }

        /**
         * On supported platforms, this static method sets the CPU core affinity
         * for the thread invoking it. On other platforms, this procedure does
         * nothing. Currently, only systems with POSIX threads are supported
         * (e.g. Linux OS family). <em>This procedure works for all threads, not
         * only for ADAPRO Threads.</em>
         *
         * @param thread_name   Name of the thread. Used for printing
         * debug/warning/error messages.
         * @param affinity
         * @throws std::runtime_error If setting the affinity or scheduling
         * parameters failed.
         */
        static void SET_AFFINITY (const int affinity);
    };
}
}

#endif	/* ADAPRO_THREAD_HPP */

