/*
 * File:   Supervisor.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 January 2017, 9:30
 */

#ifndef ADAPRO_SUPERVISOR_HPP
#define ADAPRO_SUPERVISOR_HPP

#include <cstdint>
#include <string>
#include <map>
#include <utility>
#include <list>
#include <memory>
#include <functional>
#include <atomic>
#include <mutex>
#include "../data/State.hpp"
#include "../data/Command.hpp"
#include "../data/Parameters.hpp"
#include "../data/Typedefs.hpp"
#include "Worker.hpp"

class SupervisorTest;
class FBIAgentTest;
class ProducerConsumerTest;
class ControllerTest;

namespace ADAPRO
{
namespace Control
{

    /**
     * Supervisor is a Configurable whose responsibility is to manage other
     * Configurables. The state transitions of Supervisor are propagated to
     * all Configurables under its management.
     *
     * @see ADAPRO::Control::Configurable
     * @see ADAPRO::Control::Thread
     */
    class Supervisor final: public Worker
    {
        friend SupervisorTest;
        friend FBIAgentTest;
        friend ProducerConsumerTest;
        friend ControllerTest;

        /**
         * <tt>true</tt> if and only if the Supervisor has been started (by
         * calling <tt>ADAPRO::Thread::start</tt> on it).
         *
         * @see ADAPRO::Control::Thread::start
         */
        bool started;

        /**
         * <tt>true</tt> if and only if every worker has stopped or aborted (or
         * when this is being checked).
         *
         * @see ADAPRO::Data::State
         */
        bool all_workers_halted;

        /**
         * <tt>true</tt> if and only if one or more workers have moved to state
         * <tt>ABORTED</tt>.
         *
         * @see ADAPRO::Data::State
         */
        bool some_workers_dead;

        /**
         * The function responsible for creating the workers.
         */
        const std::list<ADAPRO::Data::worker_factory_t> worker_factories;

        /**
         * List of the managed Configurable Threads.
         */
        std::list<std::unique_ptr<ADAPRO::Control::Worker>> workers;

        /**
         * This mutex object is used for ensuring mutual exclusion to
         * propagating commands to workers.
         */
        std::mutex command_mutex;

        /**
         * Sends a command to the given worker.
         *
         * @param worker_ptr    The recipient of the command.
         * @param command       The command to be sent.
         * @param wait          If <tt>true</tt> this method will block until
         * the worker has performed an appropriate state transition.
         */
        void send_command
        (
                unique_ptr<Worker>& worker_ptr,
                const ADAPRO::Data::Command command,
                const bool wait
        )
        noexcept;

        /**
         * Gives the worker 10 seconds of time to stop gracefully. If the worker
         * fails to shut down gracefully within this period, then it will be
         * stopped forcefully.
         *
         * @param worker_ptr    Pointer to the worker.
         */
        void stop_worker(std::unique_ptr<ADAPRO::Control::Worker>& worker_ptr)
        noexcept;

        inline void propagate_command
        (
                const ADAPRO::Data::Command command,
                const ADAPRO::Data::State expected_state
        )
        noexcept
        {
            if (configuration.at(std::get<0>(ADAPRO::Data::SERIALIZE_COMMANDS)) == "TRUE")
            {
                propagate_command_in_lifo(command, state);
            }
            else
            {
                propagate_command_in_parallel(command, state);
            }
        }

        /**
         * Propagates the given command to all workers and blocks until they
         * have moved to a state greater than or equal to the given expected
         * state. This procedure only blocks after sending the commands.
         *
         * @param command           The command to be given to every worker.
         * @param expected_state    The expected new state of the workers.
         */
        void propagate_command_in_parallel
        (
                const ADAPRO::Data::Command command,
                const ADAPRO::Data::State expected_state
        )
        noexcept;

        /**
         * Propagates the given command serially to the workers in
         * <em>Last In, First Out</em> (LIFO) mode. This means that the worker,
         * whose factory is first in the list given in the Supervisor's
         * constructor, will be started/resumed first and stopped/paused last.
         * This procedure blocks between after sending a command to each worker,
         * until the worker has moved to a state greater than or equal to the
         * given expected state.
         *
         * @param command           The command to be given to every worker.
         * @param expected_state    The expected new state of the workers.
         */
        void propagate_command_in_lifo
        (
                const ADAPRO::Data::Command command,
                const ADAPRO::Data::State expected_state
        )
        noexcept;

        /**
         * This constuctor is included only for unit testing.
         *
         * @param logger    A logger instance.
         */
        explicit Supervisor(Logger& logger) noexcept:
                Worker(logger, *this, std::move("Supervisor")),
                started(false),
                all_workers_halted(false),
                worker_factories(),
                workers(),
                command_mutex()
        {}

    protected:

        /**
         * In this procedure, Supervisor constructs the workers by calling the
         * factory functions given as constructor parameters. After the workers
         * are created, Supervisor starts them synchronously and moves to
         * <tt>RUNNING</tt> only after each of the workers have moved to a state
         * greater than or equal to <tt>RUNNING</tt>.
         */
        virtual void prepare() override;

        /**
         * In this procedure, Supervisor does nothing that would affect the
         * workers.
         */
        virtual void execute() override;

        /**
         * In this procedure, Supervisor stops the workers asynchronously and
         * then blocks until every worker has halted.
         */
        virtual void finish() override;

        virtual void handle_exception(const char* message) noexcept override;

    public:

        /**
         * Constructs a Supervisor.
         *
         * @param logger                Logger of the ADAPRO Session.
         * @param state_ptr             Pointer to the memory address used for
         * storing the state of this Supervisor.
         * @param worker_factories      List of the factory functions providing
         * unique pointers to workers.
         * @param configuration         The configuration to be passed on to the
         * workers.
         * @param preferred_core        Preferred core.
         *
         * @throws std::out_of_range    If the configuration key
         * <tt>ADAPRO::Data::SUPERVISOR_CORE</tt> was undefined.
         * @see ADAPRO::Data::SUPERVISOR_CORE
         */
        Supervisor
        (
                Logger& logger,
                uint32_t* state_ptr,
                const std::list<ADAPRO::Data::worker_factory_t>& worker_factories,
                const ADAPRO::Data::config_t& configuration
        );

        /**
         * A debugging command used for printing the states of the Configurables
         * managed by the Supervisor.
         */
        void print_report() noexcept;

        virtual ~Supervisor() noexcept {}
    };
}
}

#endif /* ADAPRO_SUPERVISOR_HPP */

