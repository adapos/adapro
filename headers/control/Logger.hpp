
/*
 * File:   Logger.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 30 August 2016, 15:19
 */

#ifndef ADAPRO_LOGGER_HPP
#define ADAPRO_LOGGER_HPP

#include <cstdint>
#include <iostream>
#include <fstream>
#include <string>
#include <mutex>
#include <memory>
#include "../library/Clock.hpp"
#include "../data/State.hpp"
#include "../data/Command.hpp"
#include "../data/LoggingLevel.hpp"

class LoggerTest;

namespace ADAPRO
{
namespace Control
{
    class Thread;

    /**
     * Logger is the ADAPRO class responsible for printing formatted output in a
     * <tt>std::ostrem</tt>. The output stream can be <tt>std::cout</tt> or a
     * <tt>std::ofstream</tt>. <em>Logger doesn't perform any error checking,
     * other than ensuring that the output stream is open. </em> If a
     * <tt>std::ofstream</tt> cannot be opened (e.g. due to lacking file system
     * access rights), then Logger reverts to <tt>std::cout</tt>. If the output
     * is directed to a file stream, it will be closed when <tt>~Logger</tt> is
     * executed.
     */
    class Logger final
    {
        friend LoggerTest;
        friend Thread;

        /**
         * An empty string that can be used as the output path in the
         * constructor of Logger.
         */
        static const std::string EMPTY_STRING;

        /**
         * The stream used for constructing Logger in daemon mode. In normal
         * operation, this field is unused.
         */
        std::ofstream file_stream;

        /**
         * The output stream where Logger prints all messages.
         */
        std::ostream& output_stream;

        /**
         * A mutex object used for guarding output stream access.
         */
        std::mutex output_mutex;

        /**
         * This bitmask is used for filtering messages for output. Only messages
         * with their logging level contained in this bitmask, will be printed.
         */
        const uint8_t logging_mask;

        /**
         * <p>Prints the given message with the given logging level (if equal to
         * or higher than Logger's logging level), and the given Thread as the
         * origin, to the output stream. The output format is:</p>
         * <tt>
         * &lt;log-entry&gt;   ::= &lt;timestamp&gt; " " &lt;level&gt; " "
         *                         &lt;thread-info&gt; " " &lt;message&gt; "\\n"\n
         * &lt;thread-info&gt; ::= "&lt;" &lt;name&gt; "," &lt;state&gt; ","
         *                         &lt;command&gt; &lt;core&gt; "&gt;"\n
         * &lt;core&gt;        ::= "," &lt;sched_getcpu&gt;
         * </tt>
         * <p>where <tt>&lt;timestamp&gt;</tt> is the return value of
         * <tt>ADAPRO::Library::Clock::timestamp</tt>, level is the return value
         * of <tt>ADAPRO::Data::LoggingLevel::symbol</tt> applied with the
         * given LoggingLevel, and <tt>&lt;message&gt;</tt> is the given
         * message, <tt>&lt;name</tt> is the name of the given Thread,
         * <tt>&lt;state&gt;</tt> is the Thread's current state,
         * <tt>&lt;command&gt;</tt> is the Thread's current command, and
         * if <tt>&lt;sched_getcpu&gt;</tt> is a string containing the output of
         * the function <tt>sched_getcpu</tt> on GNU-compliant systems (where C
         * preprocessor has defined <tt>GNU_SOURCE</tt>. For example, this
         * procedure might print a line like
         * <tt>"[2017-05-18 14:23:45] (I) &lt;HelloWorldThread,STARTING,CONTINUE,3&gt; Hello,_World!"</tt>
         * when applied with a HelloWorldThread instance (see the ADAPRO example
         * application), <tt>"Hello, World!"</tt>, and
         * <tt>ADAPRO::Data::LoggingLevel::INFO</tt>.</p>
         *
         * @param origin    A Thread whose name, command, state, and CPU core
         * number will be printed as the origin of the message. If the pointer
         * is <tt>nullptr</tt>, then the origin part of the message is omitted.
         * @param message   The message to be printed to the output stream.
         * @param severity  Logging level of the message.
         * @see ADAPRO::Data::LoggingLevel
         * @see ADAPRO::Library::Clock::timestamp
         * @see ADAPRO::Data::LoggingLevel::symbol
         * @see ADAPRO::Control::Thread
         * @see ADAPRO::Data::State
         * @see ADAPRO::Data::Command
         */
        void print
        (
                const ADAPRO::Control::Thread& origin,
                const std::string& message,
                const ADAPRO::Data::LoggingLevel severity =
                        ADAPRO::Data::LoggingLevel::USER_DEBUG
        )
        noexcept;

    public:

        /**
         * Constructor for Logger. Uses the given logging level mask and output
         * file.
         *
         * @param logging_mask  A bitmask of accepted LoggingLevels. Only
         * messages having LoggingLevel values contained in this mask will be
         * printed.
         * @param output_file   The output file. If this string is empty or
         * refers to an inaccessible path, then<tt>std::cout</tt> will be used
         * for output instead of a file stream.
         *
         * @see ADAPRO::Data::LoggingLevel
         */
        Logger
        (
                const uint8_t logging_mask =
                        ADAPRO::Data::LoggingLevel::USER_DEBUG  |
                        ADAPRO::Data::LoggingLevel::INFO        |
                        ADAPRO::Data::LoggingLevel::SPECIAL     |
                        ADAPRO::Data::LoggingLevel::WARNING     |
                        ADAPRO::Data::LoggingLevel::ERROR       |
                        ADAPRO::Data::LoggingLevel::FATAL,
                const std::string& output_file = EMPTY_STRING
        )
        noexcept:
                file_stream(output_file),
                output_stream(file_stream.is_open() ? file_stream : std::cout),
                output_mutex(),
                logging_mask(logging_mask) {}

        /**
         * <p>Prints the given message with the given logging level (if equal to
         * or higher than Logger's logging level) to the output stream. The
         * output format is:</p>
         * <tt>
         * &lt;log-entry&gt; ::= &lt;timestamp&gt; " " &lt;level&gt; " "
         *                       &lt;message&gt; "\\n"
         * </tt>
         * <p>where <tt>&lt;timestamp&gt;</tt> is the return value of
         * <tt>ADAPRO::Library::Clock::timestamp</tt>, level is the return value
         * of <tt>ADAPRO::Data::LoggingLevel::symbol</tt> applied with the
         * given LoggingLevel, and <tt>&lt;message&gt;</tt> is the given
         * message. For example, this procedure might print a line like
         * <tt>"[2017-05-18 14:18:24] (I) Hello, World!"</tt> when applied with
         * <tt>"Hello, World!"</tt> and
         * <tt>ADAPRO::Data::LoggingLevel::INFO</tt>.</p>
         *
         * @param message   The message to be printed to the output stream.
         * @param severity  Logging level of the message.
         * @see ADAPRO::Data::LoggingLevel
         * @see ADAPRO::Library::Clock::timestamp
         * @see ADAPRO::Data::LoggingLevel::symbol
         */
        void print
        (
                const std::string& message,
                const ADAPRO::Data::LoggingLevel severity =
                        ADAPRO::Data::LoggingLevel::USER_DEBUG
        )
        noexcept;

        /**
         * The trivial virtual destructor of Logger, which implicitly closes the
         * output stream.
         */
        ~Logger() noexcept {};
    };
}
}

#endif /* ADAPRO_LOGGER_HPP */