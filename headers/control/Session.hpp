/*
 * File:   Session.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 January 2017, 9:34
 */

#ifndef ADAPRO_SESSION_HPP
#define ADAPRO_SESSION_HPP

#include <cstdint>
#include <list>
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <mutex>
#include <functional>
#include <atomic>
#include <stdexcept>
#ifdef __linux__
#include <systemd/sd-daemon.h>
#endif
#include "../control/Logger.hpp"
#include "../data/Context.hpp"
#include "../data/State.hpp"

class SessionTest;

namespace ADAPRO
{
namespace Control
{
    class Supervisor;

    /**
     * <p>Session class is responsible for running an ADAPRO session. The user's
     * application should construct a Session object with a Context object and
     * call its method <tt>run</tt> in its <tt>main</tt> procedure.</p>
     * <p>In <em>ADAPRO 3.0.0</em> Session was promoted from a static procedure
     * into a class in order to make it follow the <em>RAII</em> principle.</p>
     * <p><em>Note that Session is designed to be a singleton class, that is a
     * class with exactly one instance.</em> Behaviour of multiple Sessions in
     * one process is undefined.</p>
     *
     * @see ADAPRO::Data::Context
     */
    class Session final
    {
        friend SessionTest;
        friend Supervisor;

        /**
         * Terminates the process with the given status code. If a Session is
         * running, then the Supervisor Thread is given one minute to shut down
         * gracefully.
         *
         * @param code The status code that will be returned from the program.
         */
        static void FORCE_SHUTDOWN(uint8_t code) noexcept;

        /**
         * The top-level exception handler of ADAPRO. It tries to print an error
         * message, and then exits.
         */
        static void ADAPRO_TERMINATE_HANDLER() noexcept;

#ifdef _POSIX_C_SOURCE

        /**
         * The primary signal handler of ADAPRO, that will be used in
         * POSIX-compliant environments. In other environments, ANSI C signal
         * API is used as a fallback mechanism.
         *
         * @param signal Number of the signal that was caught.
         */
        static void ADAPRO_SIGNAL_HANDLER(int signal) noexcept;
#else
        /**
         * The SIGSEGV handler. Prints an error message, calls the user defined
         * callback, and initiates the forceful shutdown sequence. This static
         * method is used as an ANSI C fallback in environments that are not
         * POSIX-compliant.
         *
         * @see ADAPRO::Control::Session::USER_SIGSEGV_HANDLER
         */
        static void ADAPRO_SIGSEGV_HANDLER(int dummy) noexcept;

        /**
         * The SIGABRT handler. Prints an error message, calls the user defined
         * callback, and initiates the forceful shutdown sequence. This static
         * method is used as an ANSI C fallback in environments that are not
         * POSIX-compliant.
         *
         * @see ADAPRO::Control::Session::USER_SIGABRT_HANDLER
         */
        static void ADAPRO_SIGABRT_HANDLER(int dummy) noexcept;

        /**
         * The SIGINT handler. Prints an error message, calls the user defined
         * callback, and initiates the forceful shutdown sequence. This static
         * method is used as an ANSI C fallback in environments that are not
         * POSIX-compliant.
         *
         * @see ADAPRO::Control::Session::USER_SIGINT_HANDLER
         */
        static void ADAPRO_SIGINT_HANDLER(int dummy) noexcept;

        /**
         * The SIGHUP handler. Prints an error message, calls the user defined
         * callback, and initiates the forceful shutdown sequence. This static
         * method is used as an ANSI C fallback in environments that are not
         * POSIX-compliant.
         *
         * @see ADAPRO::Control::Session::USER_SIGHUP_HANDLER
         */
        static void ADAPRO_SIGHUP_HANDLER(int dummy) noexcept;
#endif

        /**
         * Informs the operating system about a state transition of the ADAPRO
         * process. Currently, this operation is only supported on Linux systems
         * using <tt>systemd</tt>. On unsupported platforms, this method does
         * nothing. Also, this method only has effect when running in
         * <emph>daemon mode</emph>. The purpose of this method is to make
         * debugging and diagnostics easier.
         *
         * @param s The new state of the ADAPRO process.
         */
        static inline void SET_PROCESS_STATE(const ADAPRO::Data::State s) noexcept
        {
#ifdef __linux__
            if (SESSION_PTR != nullptr && SESSION_PTR->daemon_mode_started)
            {
                SESSION_PTR->notify_sd
                (
                        ("STATUS=" + ADAPRO::Library::show<ADAPRO::Data::State>(s))
                                .c_str()
                );
            }
#endif
        }

        /**
         * On a Linux platform with <tt>systemd</tt> support, when running the
         * ADAPRO application in daemon mode, this procedure enables the
         * <tt>systemd</tt> watchdog mechanism.
         */
        static inline void ENABLE_WATCHDOG() noexcept
        {
#ifdef __linux__
            if (SESSION_PTR != nullptr && SESSION_PTR->daemon_mode_started)
            {
                SESSION_PTR->notify_sd("WATCHDOG_USEC=2000000");
            }
#endif
        }

        /**
         * On a Linux platform with <tt>systemd</tt> support, when running the
         * ADAPRO application in daemon mode, this procedure disables the
         * <tt>systemd</tt> watchdog mechanism.
         */
        static inline void DISABLE_WATCHDOG() noexcept
        {
#ifdef __linux__
            if (SESSION_PTR != nullptr && SESSION_PTR->daemon_mode_started)
            {
                SESSION_PTR->notify_sd("WATCHDOG_USEC=0");
            }
#endif
        }

        /**
         * On a Linux platform with <tt>systemd</tt> support, when running the
         * ADAPRO application in daemon mode, this procedure notifies the
         * <tt>systemd</tt> watchdog.
         */
        static inline void NOTIFY_WATCHDOG() noexcept
        {
#ifdef __linux__
            if (SESSION_PTR != nullptr && SESSION_PTR->daemon_mode_started)
            {
                SESSION_PTR->notify_sd("WATCHDOG=1");
            }
#endif
        }

        /**
         * This static counter is used for keeping track of the number of times
         * when <tt>FORCE_SHUTDOWN</tt> has been called. It shouldn't be called
         * multiple times.
         */
        static uint32_t SHUTDOWN_COUNTER;

        /**
         * A static pointer that points to the current singleton instance of
         * Session.
         */
        static Session* SESSION_PTR;

#ifdef __linux__
        /**
         * A mutex object used for ensuring mutual exclusion to Systemd API.
         */
        std::mutex sd_mutex;
#endif

        /**
         * The Context object that contains all the configuration needed for
         * running an ADAPRO Session.
         */
        const ADAPRO::Data::Context context;

        /**
         * The actual configuration of the Session that will be constructed
         * using the initial configuration of the Context and the possible
         * overriding values in the configuration file.
         */
        ADAPRO::Data::config_t configuration;

        /**
         * <tt>true</tt> if and only if the Logger of Session has been
         * initialized according to the configuration.
         */
        bool logger_ready;

        /**
         * <tt>true</tt> if and only if ADAPRO is running in daemon mode and
         * <tt>handle_daemon_mode</tt> has been executed.
         *
         * @see ADAPRO::Control::Session::handle_daemon_mode
         */
        bool daemon_mode_started;

#ifdef _POSIX_C_SOURCE
        /**
         * <tt>true</tt> if and only if the process' virtual memory is locked.
         * Only applicable in Linux systems.
         */
        bool memory_locked;
#endif

        /**
         * <tt>true</tt> if and only if <tt>cleanup</tt> has been invoked.
         *
         * @see ADAPRO::Control::Session::cleanup
         */
        bool cleaned_up;

#ifndef EXCLUDE_DIM
        /**
         * <tt>true</tt> if and only if running in DIM server mode, and DIM
         * server has been started.
         */
        bool dim_server_started;

        /**
         * Contains the identifiers of the DIM services published by ADAPRO.
         * If C preprocessor has defined <tt>EXCLUDE_DIM</tt>, all DIM
         * functionality (and dependencies) are disabled.
         */
        std::list<unsigned> dim_service_ids;
#endif

        /**
         * A shared pointer to the Logger instance of the Session.
         */
        std::shared_ptr<Logger> logger_ptr;

        /**
         * Shared pointer to the Supervisor Thread of the Session. Supervisor is
         * responsible for propagating commands to the user-defined worker
         * Threads. When Session executes its method <tt>run</tt>, it invokes
         * <tt>start</tt> on Supervisor.
         *
         * @see ADAPRO::Control::Supervisor
         */
        std::shared_ptr<Supervisor> supervisor_ptr;

        /**
         * Contains the state of the Supervisor Thread as a non-negative
         * integer. If running in DIM service mode, this number will be
         * published as a DIM service <tt>&lt;DIM_SERVER_NAME&gt;/STATE</tt>
         * (<tt>I:1</tt>). This service represents the overall state of the
         * ADAPRO application. <tt>&lt;DIM_SERVER_NAME&gt;</tt> refers to the
         * value of this configuration parameter.
         *
         * @see ADAPRO::Data::DIM_SERVER_NAME
         */
        uint32_t supervisor_state;

        /**
         * Current value of the status code.
         */
        std::atomic<uint8_t> status_code;

#ifdef __linux__

        /**
         * A wrapper method for notifying systemd in a mutually excluded
         * fashion. Ignores all errors.
         *
         * @param message   The message to be passed on to <tt>sd_notify</tt>.
         */
        inline void notify_sd(const std::string& message) noexcept
        {
            std::lock_guard<std::mutex> lg(sd_mutex);
            sd_notify(0, message.c_str());
        }
#endif

        /**
         * Registers the signal handlers and user-provided callbacks (if any).
         */
        void register_handlers() noexcept;

        /**
         * Updates the context with the configuration read from the
         * configuration file.
         *
         * @throws std::ios_base::failure   If none of the key-value files
         * provided in the contextcould be accessed.
         */
        void update_configuration();

        /**
         * Decides whether or not to start in the daemon mode and performs the
         * appropriate actions.
         *
         * @throws std::out_of_range If one of the following ADAPRO
         * configuration keys were needed, but undefined (both in the initial
         * configuration and the configuration possibly read from the
         * configuration file):
         * <tt>LOGGING_LEVEL</tt>, <tt>DAEMON_ENABLED</tt>,
         * <tt>DAEMON_LOGFILE</tt>.
         * @see ADAPRO::Data::LOGGING_LEVEL
         * @see ADAPRO::Data::DAEMON_ENABLED
         * @see ADAPRO::Data::DAEMON_LOGFILE
         */
        void handle_daemon_mode();

#ifdef _POSIX_C_SOURCE
        /**
         * Handles the POSIX-specific parameters (process nice value and virtual
         * memory locking).
         *
         * @throws std::out_of_range If one of the following ADAPRO
         * configuration keys were needed, but undefined (both in the initial
         * configuration and the configuration possibly read from the
         * configuration file):
         * <tt>NICE</tt>, <tt>LOCK_MEMORY</tt>, <tt>MAIN_CORE</tt>.
         * @see ADAPRO::Data::NICE
         * @see ADAPRO::Data::LOCK_MEMORY
         * @see ADAPRO::Data::MAIN_CORE
         */
        void handle_POSIX_parameters();

        /**
         * On POSIX-compliant systems, this procedure prints some information
         * related to the runtime environment.
         */
        void print_environment_report() noexcept;

#endif

#ifndef EXCLUDE_DIM
        /**
         * Decides whether or not to activate the DIM Server Mode, and takes the
         * appropriate actions.
         *
         * @throws std::out_of_range If one of the following ADAPRO
         * configuration keys were needed, but undefined (both in the initial
         * configuration and the configuration possibly read from the
         * configuration file):
         * <tt>DIM_SERVER_ENABLED</tt>, <tt>DIM_DNS_NODE</tt>,
         * <tt>DIM_DNS_PORT</tt>, <tt>DIM_SERVER_NAME</tt>.
         *
         * @throws std::length_error        If the value of
         * <tt>DIM_DNS_NODE</tt> exceeded 255 characters.
         *
         * @throws std::length_error        If one or more of the user-provided
         * DIM command factories returned a tuple containing a DIM command
         * service name longer than 132 characters.
         *
         * @throws std::invalid_argument    If one or more of the user-provided
         * DIM command factories returned a tuple containing an invalid DIM
         * description string.
         *
         * @see ADAPRO::Data::DIM_SERVER_ENABLED
         * @see ADAPRO::Data::DIM_DNS_NODE
         * @see ADAPRO::Data::DIM_DNS_PORT
         * @see ADAPRO::Data::DIM_SERVER_NAME
         * @see ADAPRO::Data::Context::dim_command_factories
         */
        void handle_DIM_server_mode();
#endif

        /**
         * <p>Gets the current status code, which is a bit mask of the following
         * flags: </p>
         * <table>
         *     <thead>
         *         <tr><th>Flag</th><th>Decimal</th><th>Description</th></tr>
         *     </thead>
         *     <tbody>
         *         <tr><td><tt>FLAG_BAD_CONFIG</tt></td><td>1</td><td>Malformed configuration file</td></tr>
         *         <tr><td><tt>FLAG_UNHANDLED_EXCEPTION</tt></td><td>2</td><td>Generic runtime error</td></tr>
         *         <tr><td><tt>FLAG_SIGINT</tt></td><td>4</td><td>User-originated interrupt</td></tr>
         *         <tr><td><tt>FLAG_SIGHUP</tt></td><td>8</td><td>Hang up (virtual terminal closed)</td></tr>
         *         <tr><td><tt>FLAG_SIGABRT</tt></td><td>16</td><td>Abort signal</td></tr>
         *         <tr><td><tt>FLAG_SIGSEGV</tt></td><td>32</td><td>Segmentation violation</td></tr>
         *         <tr><td><tt>FLAG_TIMEOUT</tt></td><td>64</td><td>Shutdown timed out</td></tr>
         *         <tr><td><tt>FLAG_WORKER</tt></td><td>128</td><td>One or more workers have died</td></tr>
         *     </tbody>
         * </table>
         * <p>This function is called by <tt>ADAPRO::Control::main</tt> when
         * the application is about to halt, and it is used for calculating the
         * process exit code.</p>
         *
         * @return The status code.
         * @see ADAPRO::Data::FLAG_BAD_CONFIG
         * @see ADAPRO::Data::FLAG_UNHANDLED_EXCEPTION
         * @see ADAPRO::Data::FLAG_SIGINT
         * @see ADAPRO::Data::FLAG_SIGHUP
         * @see ADAPRO::Data::FLAG_SIGABRT
         * @see ADAPRO::Data::FLAG_SIGSEGV
         * @see ADAPRO::Data::FLAG_TIMEOUT
         * @see ADAPRO::Data::FLAG_WORKER
         * @see ADAPRO::Data::FLAG_USER
         */
        inline uint8_t get_status_code() noexcept
        {
            return status_code.load(std::memory_order_consume);
        }

        /**
         * Masks the given flags into the status code.
         *
         * @param flags The flags to be (<tt>OR</tt>) masked.
         */
        inline void mask_status_code(const uint8_t flags) noexcept
        {
            status_code.store(status_code.load(std::memory_order_consume) | flags);
        }

        /**
         * Performs cleanup operations that need to be carried out before the
         * program can terminate.
         */
        void cleanup() noexcept;

        /**
         * Even though Session is not a Thread, this procedure is somewhat
         * similar in purpose to <tt>ADAPRO::Control::Thread::prepare</tt>. This
         * procedure carries out the necessary preparations before the
         * Supervisor and the workers can be constructed and started.
         *
         * @throws std::out_of_range If one of the following ADAPRO
         * configuration keys were needed, but undefined (both in the initial
         * configuration and the configuration possibly read from the
         * configuration file):
         * <tt>LOGGING_LEVEL</tt>, <tt>DAEMON_ENABLED</tt>,
         * <tt>DAEMON_LOGFILE</tt>, <tt>NICE</tt>, <tt>LOCK_MEMORY</tt>,
         * <tt>MAIN_CORE</tt>, <tt>DIM_SERVER_ENABLED</tt>,
         * <tt>DIM_DNS_NODE</tt>, <tt>DIM_DNS_PORT</tt>,
         * <tt>DIM_SERVER_NAME</tt>, <tt>ADAPRO::Data::SUPERVISOR_CORE</tt>.
         *
         * @throws std::ios_base::failure   If none of the key-value files
         * provided in the contextcould be accessed.
         *
         * @throws std::length_error In DIM server mode (i.e. when
         * <tt>DIM_SERVER_ENABLED</tt> was set to <tt>"TRUE"</tt>), if the value
         * of <tt>DIM_DNS_NODE</tt> exceeded 255 characters.
         *
         * @throws std::length_error In DIM server mode (i.e. when
         * <tt>DIM_SERVER_ENABLED</tt> was set to <tt>"TRUE"</tt>), if one or
         * more of the user-provided DIM command factories returned a tuple
         * containing a DIM command service name longer than 132 characters.
         *
         * @throws std::invalid_argument    In DIM server mode (i.e. when
         * <tt>DIM_SERVER_ENABLED</tt> was set to <tt>"TRUE"</tt>), if one or
         * more of the user-provided DIM command factories returned a tuple
         * containing an invalid DIM description string.
         *
         * @see ADAPRO::Data::LOGGING_LEVEL
         * @see ADAPRO::Data::DAEMON_ENABLED
         * @see ADAPRO::Data::DAEMON_LOGFILE
         * @see ADAPRO::Data::NICE
         * @see ADAPRO::Data::LOCK_MEMORY
         * @see ADAPRO::Data::MAIN_CORE
         * @see ADAPRO::Data::DIM_SERVER_ENABLED
         * @see ADAPRO::Data::DIM_DNS_NODE
         * @see ADAPRO::Data::DIM_DNS_PORT
         * @see ADAPRO::Data::DIM_SERVER_NAME
         * @see ADAPRO::Data::SUPERVISOR_CORE
         */
        void prepare();

        /**
         * Even though Session is not a Thread, this procedure is somewhat
         * similar in purpose to <tt>ADAPRO::Control::Thread::execute</tt>. An
         * important difference, is that
         * <tt>ADAPRO::Control::Session::execute</tt> will be only invoked once.
         * This procedure constructs and runs Supervisor among other things.
         * This procedure returns control only after Supervisor has fully
         * stopped.
         */
        void execute() noexcept;

        /**
         * Even though Session is not a Thread, this procedure is somewhat
         * similar in purpose to <tt>ADAPRO::Control::Thread::finish</tt>. This
         * procedure prepares the necessary cleanup operations when the Session
         * is shutting down.
         */
        void finish() noexcept;

    public:

        /**
         * Prints the given message using the Logger of the Session, if
         * available. If not, the message will be printed to <tt>stderr</tt>
         * without fomatting.
         *
         * @param message   The message to be printed.
         * @param severity  Logging level. Has only effect if the Logger
         * singleton is available.
         */
        static inline void PRINT
        (
                const std::string& message,
                const ADAPRO::Data::LoggingLevel severity
        )
        noexcept
        {
            if (SESSION_PTR != nullptr && SESSION_PTR->logger_ptr != nullptr)
            {
                SESSION_PTR->logger_ptr->print(message, severity);
            }
            else
            {
                std::cerr << "[Logger absent] " << message << std::endl;
            }
        }

        /**
         * Returns a reference to the Logger of the Session, or throws a runtime
         * error if the Session or Logger instance has not been initialized yet
         * using the Session configuration. <em>Due to the risk of runtime
         * error, the use of this static accessor is discouraged.</em> Worker
         * Threads should use their own Logger references instead of this
         * accessor (and inject it to their subcomponents if needed).
         *
         * @return A reference to a Logger instance.
         * @throws std::runtime_error if called prematurely.
         * @see ADAPRO::Control::Worker::logger
         */
        static inline Logger& GET_LOGGER()
        {
            if (SESSION_PTR != nullptr && SESSION_PTR->logger_ready)
            {
                return *(SESSION_PTR->logger_ptr);
            }
            else
            {
                throw std::runtime_error("Session/Logger is not ready yet.");
            }
        }

        /**
         * The deleted default constructor for Session.
         */
        Session() = delete;

        /**
         * The constructor for Session.
         *
         * @param context The context.
         *
         * @see ADAPRO::Data::Context
         */
        Session(ADAPRO::Data::Context context) noexcept;

        /**
         * Runs the session. Blocks until the Supervisor instance halts (or the
         * process exits in one of the signal handlers).
         *
         * @return The status code as an <tt>OR</tt> mask of the eight status
         * flags.
         *
         * @see headers/data/StatusFlags.hpp
         */
        uint8_t run() noexcept;

        /**
         * The virtual destructor of Session. Takes care of releasing resources
         * that were acquired during runtime.
         */
        virtual ~Session() noexcept;

    };
}
}

#endif /* ADAPRO_SESSION_HPP */

