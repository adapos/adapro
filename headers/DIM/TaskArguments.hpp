/*
 * File:   TaskArguments.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 31. May 2017, 17:11
 */

#ifndef ADAPRO_DIM_TASK_ARGUMENTS_HPP
#define ADAPRO_DIM_TASK_ARGUMENTS_HPP

#include "../DIM/Typedefs.hpp"

namespace ADAPRO
{
namespace DIM
{
    /**
     * The base struct for the parameters of <tt>ADAPRO::DIM::Controller</tt>.
     * The derived structs are used for containing parameters to be passed on to
     * DIM C API.
     */
    struct TaskArgument
    {
        virtual ~TaskArgument() noexcept {}
    };

    /**
     * A simple boxed DIM service identifier.
     *
     * @see ADAPRO::DIM::Controller::update
     * @see ADAPRO::DIM::Wrapper::server_update_service
     */
    struct ServiceID final: public TaskArgument
    {
        const service_id_t service_id;

        ServiceID(const service_id_t service_id) noexcept:
                TaskArgument(), service_id(service_id) {}

        virtual ~ServiceID() noexcept {}
    };

    /**
     * A simple struct used as an argument to <tt>DIM::Controller::publish</tt>.
     *
     * @see ADAPRO::DIM::Controller::publish
     * @see ADAPRO::DIM::Wrapper::server_publish_service
     */
    struct ServicePublication final: public TaskArgument
    {
        const std::string service_name;
        const std::string service_description;
        void* buffer;
        const size_t buffer_size;
        const server_callback_t user_routine;
        const uint32_t tag;

        ServicePublication
        (
                const std::string& service_name,
                const std::string& service_description,
                void* buffer,
                const size_t buffer_size,
                const server_callback_t user_routine = nullptr,
                const uint32_t tag = 0
        )
        noexcept:
                TaskArgument(),
                service_name(service_name),
                service_description(service_description),
                buffer(buffer),
                buffer_size(buffer_size),
                user_routine(user_routine),
                tag(tag)
        {}

        virtual ~ServicePublication() noexcept {}
    };

    /**
     * A simple struct used as an argument to <tt>DIM::Controller::publish</tt>.
     *
     * @see ADAPRO::DIM::Controller::publish
     * @see ADAPRO::DIM::Wrapper::server_publish_command
     */
    struct CommandPublication final: public TaskArgument
    {
        const std::string service_name;
        const std::string service_description;
        const command_callback_t user_routine;
        const uint32_t tag;

        CommandPublication
        (
                const std::string& service_name,
                const std::string& service_description,
                const command_callback_t user_routine,
                const uint32_t tag = 0
        )
        noexcept:
                TaskArgument(),
                service_name(service_name),
                service_description(service_description),
                user_routine(user_routine),
                tag(tag)
        {}

        virtual ~CommandPublication() noexcept {}
    };


    /**
     * A simple struct used as an argument to
     * <tt>DIM::Controller::subscribe</tt>.
     *
     * @see ADAPRO::DIM::Controller::subscribe
     * @see ADAPRO::DIM::Wrapper::client_subscribe
     * @see ADAPRO::DIM::Wrapper::client_subscribe_stamped
     */
    struct ServiceSubscription final: public TaskArgument
    {
        const std::string service_name;
        const SubscriptionType subscription_type;
        const uint32_t interval;
        void* buffer;
        const size_t buffer_size;
        const command_callback_t user_routine;
        const uint32_t tag;
        const void* const error_object;
        const size_t error_object_size;
        const bool with_timestamps;

        ServiceSubscription
        (
                const std::string& service_name,
                const SubscriptionType subscription_type,
                const uint32_t interval,
                void* buffer,
                const size_t buffer_size,
                const command_callback_t user_routine = nullptr,
                const uint32_t tag = 0,
                const void* const error_object = nullptr,
                const size_t error_object_size = 0,
                const bool with_timestamps = false
        )
        noexcept:
                TaskArgument(),
                service_name(service_name),
                subscription_type(subscription_type),
                interval(interval),
                buffer(buffer),
                buffer_size(buffer_size),
                user_routine(user_routine),
                tag(tag),
                error_object(error_object),
                error_object_size(error_object_size),
                with_timestamps(with_timestamps)
        {}

        virtual ~ServiceSubscription() noexcept {}

    };

    /**
     * A simple struct used as an argument to <tt>DIM::Controller::call</tt>.
     *
     * @see ADAPRO::DIM::Controller::call
     * @see ADAPRO::DIM::Wrapper::client_call_command
     */
    struct CommandInvocation final: public TaskArgument
    {
        const std::string service_name;
        void* buffer;
        const size_t buffer_size;

        CommandInvocation
        (
                const std::string& service_name,
                void* buffer,
                const size_t buffer_size
        )
        noexcept:
                TaskArgument(),
                service_name(service_name),
                buffer(buffer),
                buffer_size(buffer_size)
        {}

        virtual ~CommandInvocation() noexcept {}

    };

    /**
     * A simple struct used as an argument to
     * <tt>DIM::Controller::overwrite</tt>.
     *
     * @see ADAPRO::DIM::Controller::overwrite
     * @see ADAPRO::DIM::Wrapper::client_write_timestamp
     */
    struct TimestampRequest final: public TaskArgument
    {
        const service_id_t service_id;
        int* milliseconds_ptr;
        int* seconds_ptr;

        TimestampRequest
        (
                const service_id_t service_id,
                int* milliseconds_ptr,
                int* seconds_ptr
        )
        noexcept:
                TaskArgument(),
                service_id(service_id),
                milliseconds_ptr(milliseconds_ptr),
                seconds_ptr(seconds_ptr)
        {}

        virtual ~TimestampRequest() noexcept {}

    };
}
}

#endif /* ADAPRO_DIM_TASK_ARGUMENTS_HPP */

