/*
 * File:   Consumer.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 14 February 2017, 17:48
 */

#ifndef ADAPRO_CONSUMER_HPP
#define ADAPRO_CONSUMER_HPP

#include <cstdint>
#include <vector>
#include <atomic>
#include <deque>
#include <map>
#include <string>
#include "../control/AlignedAllocator.hpp"
#include "../control/Supervisor.hpp"
#include "../data/Typedefs.hpp"
#include "PipelineProcessor.hpp"
#include "DataPointCompositeObject.hpp"
#include "DataPointIdentifier.hpp"
#include "DataPointValue.hpp"
#include "DataPointEventRecord.hpp"

namespace ADAPRO
{
namespace ADAPOS
{
    /**
     * Consumer is the counterpart of the Producer in the Producer-Consumer
     * model of ADAPRO/ADAPOS. Consumer is responsible for receiving updates to
     * ADAPOS services, stored in DPVAL objects, from the Producer. Consumer
     * takes care of bookkeeping, so the user only needs to implement the
     * procedure <tt>consume</tt> for taking care of the updates when they
     * arrive. For performance reasons, the updates (DPVALs) are separated from
     * the service identifiers (DPIDs).
     *
     * @see ADAPRO::ADAPOS::DataPointValue
     */
    class Consumer: public PipelineProcessor
    {
        Consumer() = delete;

    protected:

        /**
         * When <tt>RUNNING</tt>, Consumer keeps receiving data from Producer.
         * When a new DPVAL arrives, Consumer takes care of locking the updates
         * in the shared ring buffer. It then executes <tt>consume</tt> with the
         * index and DPVAL object received. When control returns to Consumer, it
         * releases the lock.
         */
        virtual void execute() final override;

        /**
         * Invokes <tt>Thread::handle_exception</tt> and then
         * <tt>Consumer::finish</tt>. The latter <em>must be executed in all
         * exception handling scenarios</em>. Not doing so might cause
         * <tt>Producer</tt> to get stuck in an infinite loop.
         *
         * @param message   The error message.
         */
        virtual void handle_exception(const char* message) noexcept override;

        /**
         * <p>Processes the given DPVAL object that contains an update to an
         * ADAPOS service.</p><p>When the DPVAL object belongs to a service that
         * <tt>Producer</tt> encountered for the first time, the
         * <tt>NEW_FLAG</tt> is set. By default, when <tt>Producer</tt>
         * terminates, it sets the <tt>TERMINATOR_FLAG</tt> of an DPVAL object,
         * which then is passed to this procedure. It is on the framework user's
         * responsibility to check and react to these flags, if needed.
         *
         * @param index This index can be used for retrieving the corresponding
         * DPID element from the DPID deque.
         * @param dpval The DPVAL object containing the update.
         * @return <tt>true</tt> if and only if the <tt>Consumer</tt> should
         * proceed to consume the next elements in the ring buffers.
         *
         * @see ADAPRO::ADAPOS::PipelineProcessor
         * @see ADAPRO::ADAPOS::DataPointValue
         */
        virtual void consume(const size_t index, const DPVAL& dpval) = 0;

        /**
         * Constructor for Consumer.
         *
         * @param logger                The Logger instance of an ADAPRO
         * Session.
         * @param buffer                The fixed-size shared ring buffer.
         * @param buffer_size           Size of the ring buffer.
         * @param registry              The deque containing the currently known
         * DPIDs. When an event belonging to a new service is produced, its
         * identifier is appended to the registry. Consumer may safely read from
         * any index of the registry at any time.
         * @param supervisor            The ADAPRO Supervisor instance.
         * @param name                  Name of the Consumer instance.
         * @param configuration         The configuration map of the ADAPRO
         * process.
         * @param preferred_core        Preferred CPU core number.
         * @param transition_callback   The transition callback.
         */
        Consumer
        (
                ADAPRO::Control::Logger& logger,
                buffer_t buffer,
                const size_t buffer_size,
                registry_t& registry,
                ADAPRO::Control::Supervisor& supervisor,
                const std::string&& name,
                const ADAPRO::Data::config_t& configuration =
                        ADAPRO::Data::DEFAULT_CONFIGURATION,
                const int preferred_core = -1,
                const ADAPRO::Data::trans_cb_t transition_callback =
                        ADAPRO::Control::Thread::NOP_CB
        )
        noexcept;

        virtual ~Consumer() noexcept {}

    };
}
}

#endif /* ADAPRO_CONSUMER_HPP */

