/*
 * File:   FBIAgent.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 February 2017, 17:30
 */

#ifndef ADAPRO_FBI_AGENT_HPP
#define ADAPRO_FBI_AGENT_HPP

#include <cstdint>
#include <string>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <map>
#include <deque>
#include <vector>
#include <functional>
#include "../control/Thread.hpp"
#include "../control/Worker.hpp"
#include "../control/Supervisor.hpp"
#include "../control/Metronome.hpp"
#include "../control/Logger.hpp"
#include "../control/AlignedAllocator.hpp"
#include "../data/Parameters.hpp"
#include "../data/Typedefs.hpp"
#include "DataPointCompositeObject.hpp"
#include "DataPointValue.hpp"
#include "Typedefs.hpp"

namespace ADAPRO
{
namespace ADAPOS
{
    /**
     * <p>Like the Producer-Consumer model, FBIAgent is primarily designed to be
     * used in ADAPOS applications. It is a Configurable with an additional
     * data structure for storing DataPointCompositeObjects, called
     * <em>Full Buffer Image</em> (FBI). <em>FBI access is safe between FBIAgent
     * and one other Thread (that uses <tt>append_to_fbi</tt> or
     * <tt>update_fbi</tt>).</em></p>
     * <p>FBIAgent can be used <em>passively</em>, that is consuming FBI on
     * request (by invoking <tt>notify</tt>), or actively by periodically
     * consuming the FBI.
     * </p><p><em>Note that FBIAgent is designed for a use case where the
     * DataPointIdentifiers of the DataPointCompositeObjects in FBI are
     * immutable and doesn't provide API for changing them after initialization.
     * </em></p>
     *
     * @see ADAPRO::Control::Worker
     * @see ADAPRO::ADAPOS::FBIAgent::append_to_fbi
     * @see ADAPRO::ADAPOS::FBIAgent::update_fbi
     * @see ADAPRO::ADAPOS::FBIAgent::notify
     * @see ADAPRO::ADAPOS::DataPointCompositeObject
     */
    class FBIAgent : public ADAPRO::Control::Worker
    {
        /**
         * The deleted default constructor for FBIAgent.
         */
        FBIAgent() = delete;

        /**
         * <tt>true</tt> if and only if passive agent needs to consume FBI.
         */
        std::atomic_bool should_consume;

        /**
         * Used for maintaining a stable rate of FBI copies in active mode.
         */
        ADAPRO::Control::Metronome& metronome;

        /**
         * Stands for Full Buffer Image.
         */
        image_t fbi;

        /**
         * Copy of FBI, that gets periodical updates in active mode.
         */
        image_copy_t fbi_copy;

        /**
         * Like <tt>copy_fbi</tt>, but given a vector of <em>n</em> DPCOMs,
         * overwrites their DPVAL parts with the DPVALs of <em>n</em> first
         * DPCOMs present in the FBI. Additionally, if there were more DPCOMs in
         * FBI, than in the vector provided, by the time this method was called,
         * these extra DPCOMs will be appended to the end of the vector. If the
         * given vector had more DPCOMs than were in FBI, these extra DPCOMs in
         * the given vector are not modified.
         *
         * @param storage The vector where the data from FBI will be written
         * into.
         * @see ADAPRO::ADAPOS::FBIAgent::copy_fbi
         */
        void read_fbi(image_copy_t& storage) noexcept;

    protected:

        /**
         * Updates a copy of FBI, passes it to <tt>consume_fbi</tt>, and
         * synchronizes with the metronome.
         *
         * @see ADAPRO::ADAPOS::FBIAgent::consume_fbi
         */
        virtual void execute() final override;

        /**
         * Consumes a copy of FBI.
         *
         * @param fbi_copy  Copy of FBI.
         */
        virtual void consume_fbi(const image_copy_t& fbi_copy) = 0;

        /**
         * Copy of FBI which is to be sent - not blocked by mutexes.
         */
        image_copy_t fbi_to_send;
        
        /**
         * Sends a copy of FBI.
         */
        virtual void send_fbi() = 0;
        
    public:

        /**
         * <tt>true</tt> if and only if FBIAgent is active, which means that it
         * will consume FBI on (more or less) constant intervals.
         */
        const bool active;

        /**
         * <p>Copies a part of FBI into the given vector.</p>
         * <p>If there were no concurrent insertions to FBI during the execution
         * of this procedure, then the whole FBI is returned; otherwise the
         * first <em>n</em> elements, where <em>n</em> &gt;= 0.</p>
         * <p><em>In case of concurrent DPCOM access between this procedure and
         * <tt>update_fbi</tt>, it is undefined, whether the old, or the new
         * state of the DPCOM object in question gets copied to the FBI copy
         * being constructed.</em> However, DPCOM access to single DPCOMs is
         * guaranteed to be transactional in the sense of the ACID principle.
         * Therefore, if a DPCOM object with state <tt>S1</tt> gets updated to
         * state <tt>S2</tt> during the execution of this procedure, and if this
         * procedure copies the object in state <tt>S1</tt>, it is guaranteed
         * that the same object will be copied in state <tt>S2</tt>, or newer,
         * the next time this procedure is executed.
         * </p><p>If a deep copy is not needed, but merely an update of the
         * mutable parts of data, consider updating an existing copy by using
         * <tt>read_fbi</tt></p>
         *
         * @param storage A vector for storing the copies of the DPCOM objects
         * that were present in FBI when this procedure was executed, in the
         * state that was observed by the thread that executed this procedure.
         * The DPCOMs will be pushed back to the vector sequentially.
         *
         * @see ADAPRO::ADAPOS::FBIAgent::insert_fbi
         * @see ADAPRO::ADAPOS::FBIAgent::update_fbi
         * @see ADAPRO::ADAPOS::FBIAgent::read_fbi
         */
        void copy_fbi(image_copy_t& storage) noexcept;

        /**
         * The constructor for FBIAgent. Fore more information on the
         * <em>active</em> and <em>passive</em> mode, see the API documentation
         * entry for this class.
         *
         * @param logger                The Logger instance of the ADAPRO
         * Session.
         * @param tick_length           If zero, then the FBIAgent will be
         * passive, which means that it will only consume FBI when
         * <tt>notify</tt> has been invoked. If positive, then FBIAgent will use
         * a Metronome with this tick length for synchronizing the interval
         * between <tt>consume_fbi</tt> calls.
         * @param suggested_copy_size   Size that will be tried to reserve for
         * a reusable FBI copy vector. Values below <tt>1</tt> enable automatic
         * copy size reservation deduction.
         * @param supervisor            The ADAPRO Supervisor.
         * @param name                  Name of the FBI Agent.
         * @param configuration         The ADAPRO application configuration.
         * @param preferred_core        Preferred CPU core number.
         * @param transition_callback   The transition callback.
         *
         * @see ADAPRO::ADAPOS::FBIAgent
         * @see ADAPRO::ADAPOS::FBIAgent::notify
         */
        FBIAgent
        (
                ADAPRO::Control::Logger& logger,
                const uint32_t tick_length,
                const image_copy_t::size_type suggested_copy_size,
                ADAPRO::Control::Supervisor& supervisor,
                const std::string&& name,
                const ADAPRO::Data::config_t& configuration =
                        ADAPRO::Data::DEFAULT_CONFIGURATION,
                const int preferred_core = -1,
                const ADAPRO::Data::trans_cb_t transition_callback =
                        ADAPRO::Control::Thread::NOP_CB
        )
        noexcept;

        virtual ~FBIAgent() noexcept {}

        /**
         * Appends the given DPCOM into the FBI. The control flags of the DPCOM
         * object will be cleared. Otherwise the object will remain unchanged.
         *
         * @param dpcom The DPCOM object to be added.
         *
         * @see update_fbi
         */
        inline void append_to_fbi(DPCOM& dpcom) noexcept
        {
            std::lock_guard<std::mutex> lock(fbiAccessMutex);
            dpcom.data.flags &= DPVAL::CONTROL_MASK;
            fbi.push_back(dpcom);
        }

        /**
         * Appends the given DPCOM into the FBI. <em>If the <tt>READ_FLAG</tt>
         * or <tt>WRITE_FLAG</tt> on the <tt>DataPointValue</tt> part of the
         * given DPCOM object are set, then subsequent read or write access
         * (via <tt>read_fbi</tt> or <tt>update_fbi</tt>) to the DPCOM object in
         * FBI may cause a deadlock.</em> Clearing the control flags before
         * appending to FBI, or calling the non-const argument overload of this
         * function is highly recommended.
         *
         * @param dpcom The DPCOM object to be added.
         *
         * @see update_fbi
         */
        inline void append_to_fbi(const DPCOM& dpcom) noexcept
        {
            std::lock_guard<std::mutex> lock(fbiAccessMutex);
            fbi.push_back(dpcom);
        }

        /**
         * Updates the FBI by writing the given DPVAL object into the DPCOM in
         * FBI identified with the given index.
         *
         * @param index Index of the DPCOM object to be updated in FBI.
         * @param dpval
         * @return A const reference to the DPCOM object that was updated.
         */
        inline const DPCOM& update_fbi
        (
                const image_t::size_type index,
                const DPVAL& dpval
        )
        noexcept
        {
            DPCOM& dpcom(fbi[index]);
            {
                std::lock_guard<std::mutex> lock(fbiAccessMutex);
                dpcom.update(dpval);
            }

            return dpcom;
        }

        /**
         * Returns a const reference to the corresponding index in FBI. no
         * bounds checking is performed.
         *
         * @param index Index in FBI.
         * @return      Reference to the DPCOM object at the given index.
         */
        inline const DPCOM& peek_fbi(const image_t::size_type index) noexcept
        {
            std::lock_guard<std::mutex> lock(fbiAccessMutex);
            return fbi[index];
        }

        /**
         * Notifies a passive FBIAgent, that FBI needs to be copied and
         * consumed. Waits in a spinlock until FBIAgent becomes ready to consume
         * next FBI.
         */
        inline void notify() noexcept
        {
            should_consume.store(true, memory_order_release);
        }

        /**
         * Returns the current size of FBI.
         *
         * @return Size of FBI.
         */
        inline image_t::size_type get_fbi_size() noexcept
        {
            std::lock_guard<std::mutex> lock(fbiAccessMutex);
            return fbi.size();
        }

        inline void clear_fbi() noexcept
        {
            std::lock_guard<std::mutex> lock(fbiAccessMutex);
            fbi.clear();
        }

        /**
         * Protect the access for read/write operations on FBI.
         * This mutex replaces the acquire read/write lock mechanism
         * implemented within DataPointValue.hpp file.
         */
        std::mutex fbiAccessMutex;
        
        /**
         * Protect the access for clear/read/write operations on FBI.
         */
        std::mutex fbiOperationMutex;
    };
}
}

#endif /* ADAPRO_FBI_AGENT_HPP */

