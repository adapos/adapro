/*
 * File:   DataPointEventRecord.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 21 February 2017, 12:25
 */

#ifndef ADAPRO_DATAPOINT_EVENT_RECORD
#define ADAPRO_DATAPOINT_EVENT_RECORD

#include <cstdint>
#include <mutex>
#include <ios>
#include <ostream>
#include "DataPointValue.hpp"

namespace ADAPRO
{
namespace ADAPOS
{
    /**
     * DataPointEventRecord is the basic data typed used in the ADAPOS/ADAPRO
     * Producer-Consumer model for processing updates to ADAPOS services. It
     * contains the index of the DataPointIdentifier object that corresponds
     * with the DataPointValue object known as the event. These objects can be
     * combined into a DataPointCompositeObject that represents the state of an
     * ADAPOS service at a specific moment. DataPointEventRecord also contains a
     * mutex object and another index of a DataPointIdentifier object that will
     * be used in case of buffer overflow to indicate which service lost an
     * event.
     */
    struct alignas(128) DataPointEventRecord final
    {

        /**
         * The mutex object used for ensuring mutual exclusion between Producer
         * and Consumer.
         */
        std::mutex mutex_object;

        /**
         * Index of the shared DataPointIdentifier deque containing the
         * identifier of the ADAPOS service to which the event stored in this
         * record belongs to.
         */
        uint64_t current_dpid_index;

        /**
         * In case of buffer overflow, this variable contains the value of
         * <tt>current_dpid_index</tt> before it was overwritten. In other
         * words, if <tt>DPVAL::OVERFLOW_FLAG</tt> is set
         */
        uint64_t previous_dpid_index;

        /**
         * The DataPointValue object containing the value of an ADAPOS service.
         */
        DataPointValue data;

        DataPointEventRecord() noexcept:
                mutex_object(), current_dpid_index(0), previous_dpid_index(0) {}

        DataPointEventRecord
        (
                const uint64_t current_dpid_index,
                DataPointValue&& data
        )
        noexcept:
                current_dpid_index(current_dpid_index),
                previous_dpid_index(0),
                data(std::move(data))
        {}

        DataPointEventRecord(const DataPointEventRecord& other) noexcept
        {
            current_dpid_index  = other.current_dpid_index;
            previous_dpid_index = other.previous_dpid_index;
            data                = other.data;
        }

        DataPointEventRecord& operator =(const DataPointEventRecord& other)
        {
            current_dpid_index  = other.current_dpid_index;
            previous_dpid_index = other.previous_dpid_index;
            data                = other.data;
            return *this;
        }

        ~DataPointEventRecord() noexcept {}

        friend inline std::ostream& operator<<
        (
                std::ostream& os,
                const DataPointEventRecord& dper
        )
        noexcept
        {
            return os << std::uppercase << std::hex << (uintptr_t) &dper <<
                    std::dec << std::nouppercase << ";" <<
                    dper.current_dpid_index << ";" <<
                    dper.previous_dpid_index << ";" << dper.data;
        }
    };
}
}

#endif /* ADAPRO_DATAPOINT_EVENT_RECORD */

