/*
 * File:   Typedefs.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 12 June 2017, 11:22
 */

#ifndef ADAPRO_ADAPOS_TYPEDEFS_HPP
#define ADAPRO_ADAPOS_TYPEDEFS_HPP

#include <vector>
#include <deque>
#include <unordered_map>
#include "../control/AlignedAllocator.hpp"
#include "DataPointCompositeObject.hpp"
#include "DataPointIdentifier.hpp"
#include "DataPointValue.hpp"
#include "DataPointEventRecord.hpp"

namespace ADAPRO
{
/**
 * ADAPOS is the namespace containing the parts of ADAPRO that are used by ALICE
 * Data Point Service applications.
 */
namespace ADAPOS
{
    /**
     * An acronym for DataPointCompositeObject.
     */
    using DPCOM = DataPointCompositeObject;

    /**
     * An acronym for DataPointIdentifier.
     */
    using DPID  = DataPointIdentifier;

    /**
     * An acronym for DataPointValue.
     */
    using DPVAL = DataPointValue;

    /**
     * An acronym for DataPointEventRecord.
     */
    using DPER  = DataPointEventRecord;

    /**
     * Type of an contiguous indexed data structure of ADAPOS service states
     * that is thread-safe in terms of appending to the end and random read
     * access.
     */
    using image_t = std::deque<DPCOM, ADAPRO::Control::AlignedAllocator<DPCOM>>;

    /**
     * Type of an contiguous indexed data structure of ADAPOS service states.
     */
    using image_copy_t = std::vector<DPCOM,
            ADAPRO::Control::AlignedAllocator<DPCOM>>;

    /**
     * Type of a thread-safe (in terms of inserts and reads) indexed data
     * structure of ADAPOS service identifiers.
     */
    using registry_t = std::deque<DPID, ADAPRO::Control::AlignedAllocator<DPID>>;

    /**
     * Type of a shared ring buffer containing ADAPOS service update events. The
     * buffer needs to have a fixed size, which cannot be conveniently enforced
     * by the type system, unfortunately.
     */
    using buffer_t = DPER*; // Maybe a STL container would be more appropriate.

    /**
     * Type of a hash map used as a lookup table for associating DPIDs with
     * positive integers.
     */
    using lookup_t = std::unordered_map<DPID, size_t, DPIDHash>;
}
}

#endif /* ADAPRO_ADAPOS_TYPEDEFS_HPP */

