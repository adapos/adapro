/*
 * File:   DPCOMUtils.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 24 March 2017, 12:33
 */

#ifndef ADAPRO_DPCOM_UTILS_HPP
#define ADAPRO_DPCOM_UTILS_HPP

#include <cstdint>
#include <cstring>
#include <string>
#include <stdexcept>
#include "DataPointCompositeObject.hpp"
#include "DataPointIdentifier.hpp"
#include "DataPointValue.hpp"
#include "DeliveryType.hpp"
#include "Typedefs.hpp"

using namespace ADAPRO::Data;

namespace ADAPRO
{
namespace ADAPOS
{
    /**
     * Increments the value of the given data point by one. The exact
     * interpretation of this operation depends ton the type of the data point,
     * defined as the <tt>DeliveryType</tt> of the <tt>DataPointIdentifier</tt>
     * component of the argument. This operation can also update the timestamp.
     *
     * @param dpcom The <tt>DataPointCompositeObject</tt> to be incremented,
     * representing the state of a service.
     * @param update_timestamp  If <tt>true</tt>, the timestamp of the
     * <tt>DataPointValue</tt> component of the given DPCOM object will be
     * updated to match the current system time.
     * @throws std::domain_error If the given DPCOM object had the
     * DeliveryType <tt>VOID</tt>.
     * @see ADAPRO::ADAPOS::DeliveryType
     */
    static inline void increment
    (
            DPCOM& dpcom,
            const bool update_timestamp = true
    )
    {
        DPVAL& dpval(dpcom.data);
        switch (dpcom.id.get_type())
        {
            case RAW_INT:       case DPVAL_INT:
                dpval.payload_pt1 = (int32_t) (dpval.payload_pt1 + 1);
                break;
            case RAW_UINT:      case DPVAL_UINT:
                dpval.payload_pt1 = (uint32_t) (dpval.payload_pt1 + 1);
                break;
            case RAW_FLOAT:     case DPVAL_FLOAT:
                union {uint32_t n; float x;} floatConverter;
                floatConverter.n = dpval.payload_pt1;
                floatConverter.x += 1.0F;
                dpval.payload_pt1 = floatConverter.n;
                break;
            case RAW_DOUBLE:    case DPVAL_DOUBLE:
                union {uint64_t n; double x;} doubleConverter;
                doubleConverter.n = dpval.payload_pt1;
                doubleConverter.x += 1.0;
                dpval.payload_pt1 = doubleConverter.n;
                break;
            case RAW_BOOL:      case DPVAL_BOOL:
                dpval.payload_pt1 = !dpval.payload_pt1;
                break;
            case RAW_CHAR:      case DPVAL_CHAR:
                dpval.payload_pt1 = (dpval.payload_pt1 + 1) % 10 + 48;
                break;
            case RAW_STRING:    case DPVAL_STRING:
                if(*((char*) &dpval.payload_pt1))
                {
                    try
                    {
                        int stringValue = std::stoi((char*) &dpval.payload_pt1);
                        strncpy((char*) &dpval.payload_pt1, std::to_string(stringValue + 1).c_str(), 55);
                    }
                    catch(std::invalid_argument& e)
                    {
                        throw std::domain_error("Increment operation is allowed only on numerical string");
                    }
                }
                else
                {
                    dpval.payload_pt1 = 49;
                }
                break;
            case RAW_TIME:      case DPVAL_TIME:
                dpval.payload_pt1 += 0x100000000;
                break;
            case RAW_BINARY:    case DPVAL_BINARY:
                ++dpval.payload_pt1;
                break;
            case VOID:          default:
                throw std::domain_error
                (
                        "Cannot increment a DPVAL without type information."
                );
        }
        if (update_timestamp) {dpval.update_timestamp();}
    }

    /**
     * <p>A comparison between two <tt>DataPointCompositeObject</tt>. The return
     * value is an <tt>OR</tt> mask of the following flags:</p>
     * <table>
     *     <tr><th>Flag</th><th>Meaning</th></tr>
     *     <tr><td><tt>DPVAL::BAD_DPID_FLAG</tt></td>
     *         <td>The <tt>DataPointIdentifier</tt> components of the given
     *         DPCOMs didn't match. (Either their <em>alias</em>,
     *         <em>payload type</em>, or both were different)</td>
     *     </tr>
     *     <tr><td><tt>DPVAL::BAD_FLAGS_FLAG</tt></td>
     *         <td>The <em>flags</em> of the <tt>DataPointValue</tt>
     *         components were different.</td>
     *     </tr>
     *     <tr><td><tt>DPVAL::BAD_TIMESTAMP_FLAG</tt></td>
     *         <td>The actual <em>timestamp</em> (in the <tt>DataPointValue</tt>
     *         component of the second argument) was less than what was expected
     *         (i.e. stored in the first argument).</td>
     *     </tr>
     *     <tr><td><tt>DPVAL::BAD_PAYLOAD_FLAG</tt></td>
     *         <td>The <em>payloads</em> of the <tt>DataPointValue</tt>
     *         components were different.</td>
     *     </tr>
     * </table>
     *
     * @param expected          The left-hand side operand for equality
     * comparison.
     * @param actual            The right-hand side operand for the equality
     * comparison.
     * @param payload_length    The amount of bytes from the beginning of the
     * payloads for equality comparison. The maximum value is 56. Greater values
     * are also interpreted as 56. <em>This function uses <tt>std::memcmp</tt>
     * for the payload comparison, and it's on the user's responsibility to deal
     * with possible trailing garbage data.</em>
     *
     * @return An <tt>OR</tt> mask of the four flags described above.
     *
     * @see ADAPRO::ADAPOS::DataPointIdentifier
     * @see ADAPRO::ADAPOS::DataPointValue
     */
    static inline uint16_t verify
    (
            const DPCOM& expected,
            const DPCOM& actual,
            const size_t payload_length = 56
    )
    noexcept
    {
        return (expected.id == actual.id
                    ? 0 : DPVAL::BAD_DPID_FLAG) |
            ((expected.data.flags == actual.data.flags)
                    ? 0 : DPVAL::BAD_FLAGS_FLAG) |
            (((((uint64_t) expected.data.sec) * 1000 + expected.data.msec) <=
                    (((uint64_t) actual.data.sec) * 1000 + actual.data.msec))
                    ? 0 : DPVAL::BAD_TIMESTAMP_FLAG) |
            (memcmp((void*) &expected.data.payload_pt1,
                (void*) &actual.data.payload_pt1, payload_length) == 0
                    ? 0 : DPVAL::BAD_PAYLOAD_FLAG);
    }
}
}

#endif /* ADAPRO_DPCOM_UTILS_HPP */

