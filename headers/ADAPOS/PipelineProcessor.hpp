/*
 * File:   PipelineProcessor.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 14 February 2017, 9:10
 */

#ifndef ADAPRO_PIPELINE_PROCESSOR_HPP
#define ADAPRO_PIPELINE_PROCESSOR_HPP

#include <vector>
#include <deque>
#include <atomic>
#include <mutex>
#include <map>
#include <string>
#include <functional>
#include "../control/Worker.hpp"
#include "../control/Supervisor.hpp"
#include "../control/Metronome.hpp"
#include "../control/AlignedAllocator.hpp"
#include "../control/Logger.hpp"
#include "../data/State.hpp"
#include "../data/Parameters.hpp"
#include "../data/Typedefs.hpp"
#include "DataPointCompositeObject.hpp"
#include "DataPointIdentifier.hpp"
#include "DataPointValue.hpp"
#include "DataPointEventRecord.hpp"
#include "Typedefs.hpp"

namespace ADAPRO
{
namespace ADAPOS
{
    /**
     * <p>
     * <tt>PipelineProcessor</tt> is the base class used in ADAPRO/ADAPOS
     * Producer-Consumer model. This model assumes exactly one pair of Producer
     * and Consumer instances for every registry-buffer pair.
     * </p>
     * <p>
     * In short, <tt>Producer</tt> and <tt>Consumer</tt>, the classes inheriting
     * <tt>PipelineProcessor</tt> operate on a shared ring <em>buffer</em>
     * containing a stream of updates to ADAPOS services. Each update event is
     * contained in a <tt>DataPointEventRecord</tt> (DPER) object. For
     * performance (and logical) reasons, the updates are separated from the
     * service identifiers.
     * </p>
     * <p>
     * Identifiers are <tt>DataPointIdentifier</tt> (DPID) objects stored in a
     * data structure known as <em>registry</em>. There can be new DPIDs coming
     * in to the system during runtime, but existing DPIDs are never allowed to
     * change. On each update, <tt>Producer</tt> writes a into the shared
     * buffer. <tt>Consumer</tt> keeps reading these values, and can retrieve
     * the DPID object for every update from a shared deque using the index
     * provided by <tt>Producer</tt>, if necessary.
     * </p>
     *
     * @see ADAPRO::ADAPOS::Producer
     * @see ADAPRO::ADAPOS::Consumer
     * @see ADAPRO::ADAPOS::DataPointEventRecord
     * @see ADAPRO::ADAPOS::DataPointValue
     * @see ADAPRO::ADAPOS::DataPointIdentifier
     */
    class PipelineProcessor: public ADAPRO::Control::Worker
    {
        friend class Producer;
        friend class Consumer;

        PipelineProcessor() = delete;

        /**
         * The DPIDs shared between two PipeLineProcessor instances.
         */
        registry_t& registry;

        /**
         * The shared ring buffer
         */
        buffer_t buffer;

        /**
         * The common size of data buffer and index buffer.
         */
        const size_t buffer_size;

        /**
         * The current indec of index/data buffer that is being accessed.
         */
        size_t current_index;

        /**
         * Constructor for PipelineProcessor. The buffer must be initialized
         * with default-constructed elements. The buffer will be used in a
         * circular fashion; The elements are accessed sequentially, in
         * increasing order in respect to their memory address. When the last
         * element is reached, the next element to be accessed is in the
         * beginning of the buffer.
         *
         * @param logger                The Logger instance of the ADAPRO
         * Session.
         * @param buffer                The fixed-size shared ring buffer.
         * @param buffer_size           Size of the ring buffer.
         * @param registry              The deque containing the currently known
         * DPIDs. When an event belonging to a new service is produced, its
         * identifier is appended to the registry. Consumer may safely read from
         * any index of the registry at any time.
         * @param supervisor            The ADAPRO Supervisor instance.
         * @param name                  Name of the PipelineProcessor instance.
         * @param configuration         The configuration map of the ADAPRO
         * process.
         * @param preferred_core        Preferred CPU core number.
         * @param transition_callback   The transition callback.
         *
         * @see ADAPRO::Control::Thread
         * @see ADAPRO::Control::Configurable
         * @see ADAPRO::Control::Supervisor
         * @see ADAPRO::Control::Session
         */
        PipelineProcessor
        (
                ADAPRO::Control::Logger& logger,
                buffer_t buffer,
                const size_t buffer_size,
                registry_t& registry,
                ADAPRO::Control::Supervisor& supervisor,
                const std::string&& name,
                const ADAPRO::Data::config_t& configuration,
                const int preferred_core = -1,
                const ADAPRO::Data::trans_cb_t transition_callback =
                        ADAPRO::Control::Thread::NOP_CB
        )
        noexcept:
                Worker
                (
                        logger,
                        supervisor,
                        std::move(name),
                        configuration,
                        preferred_core,
                        transition_callback
                ),
                registry(registry),
                buffer(buffer),
                buffer_size(buffer_size),
                current_index(0)
        {}

        virtual ~PipelineProcessor() noexcept {}

    public:

        /**
         * Returns a const reference to a DPID object with the given index.
         * <em>No bounds checking is performed.</em>
         *
         * @param index Index of the DPID object.
         * @return      A read-only reference to the DPID object.
         */
        inline const DPID& get_dpid(const size_t index) const noexcept
        {
            return registry[index];
        }

        /**
         * Returns the number of DPID objects currently available in the DPID
         * deque.
         *
         * @return Number of DPID objects present in the deque.
         */
        inline size_t get_dpid_count() const noexcept
        {
            return registry.size();
        }
    };
}
}

#endif /* ADAPRO_PIPELINE_PROCESSOR_HPP */

