/*
 * File:   DPIDUtils.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 August 2016, 11:51
 */
#ifndef ADAPRO_DPID_UTILS_HPP
#define ADAPRO_DPID_UTILS_HPP

#include <string>
#include <deque>
#include <list>
#include <memory>
#include "../control/Logger.hpp"
#include "DataPointIdentifier.hpp"
#include "Typedefs.hpp"

namespace ADAPRO
{
namespace ADAPOS
{
    /**
     * Reads DPIDs from a file with the given list of paths. The first
     * accessible file in the list will be used for importing the DPIDs. If none
     * of the files are accessible, a <tt>std::ios_base::failure</tt> is thrown.
     * <em>Note that the service file format was changed in ADAPRO 1.3.0 and in
     * 4.0.0 again, and is not compatible with the older formats.</em>
     *
     * @param logger                The Logger instance of the ADAPRO Session.
     * @param dpids                 A deque used for storing the DPID objects.
     * @param service_file_name     Path of the service file that contains the
     * aliases and types of DIM services mediated by ADAPOS.
     * @see ADAPRO::Library::import_map
     */
    void import_dpids
    (
            ADAPRO::Control::Logger& logger,
            registry_t& dpids,
            const std::list<std::string>& filenames
    );

    /**
     * Reads DPIDs from a file with the given path. <em>Note that the service
     * file format was changed in ADAPRO 1.3.0 and in 4.0.0 again, and is not
     * compatible with the older formats. </em>
     *
     * @param logger                The Logger instance of the ADAPRO Session.
     * @param dpids                 A deque used for storing the DPID objects.
     * @param service_file_name     Path of the service file that contains the
     * aliases and types of DIM services mediated by ADAPOS.
     * @see ADAPRO::Library::import_map
     */
    inline void import_dpids
    (
            ADAPRO::Control::Logger& logger,
            registry_t& dpids,
            const std::string& service_file_name
    )
    {
        import_dpids(logger, dpids, std::list<std::string>{service_file_name});
    }

    /**
     * Writes DPIDs into a service file. <em>Note that the service file format
     * was changed in ADAPRO 1.3.0 and in 4.0.0 again, and is not compatible
     * with the older formats.</em>
     *
     * @param dpids                DPIDs to be written into the file.
     * @param service_file_name    Location of the service file to be created.
     * @see ADAPRO::Library::expport_map
     * @throws std::invalid_argument If the registry contained DPIDs with
     * <tt>DeliveryType::VOID</tt>.
     */
    void export_dpids
    (
            const registry_t& dpids,
            const std::string & service_file_name
    );
}
}

#endif /* ADAPRO_DPID_UTILS_HPP */

