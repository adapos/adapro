/*
 * File:   Producer.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 14 February 2017, 17:47
 */

#ifndef ADAPRO_PRODUCER_HPP
#define ADAPRO_PRODUCER_HPP

#include <cstdint>
#include <vector>
#include <atomic>
#include <deque>
#include <map>
#include <string>
#include "../control/Supervisor.hpp"
#include "../control/AlignedAllocator.hpp"
#include "../library/Clock.hpp"
#include "../control/Logger.hpp"
#include "../data/Typedefs.hpp"
#include "PipelineProcessor.hpp"
#include "DataPointCompositeObject.hpp"
#include "DataPointIdentifier.hpp"
#include "DataPointValue.hpp"
#include "DataPointEventRecord.hpp"
#include "Typedefs.hpp"

namespace ADAPRO
{
namespace ADAPOS
{
    /**
     * Producer is the <tt>PipelineProcessor</tt> responsible for writing
     * <tt>DataPointValue</tt> objects and the indices for their corresponding
     * <tt>DataPointIdentifier</tt> objects to the shared buffers, so that
     * <tt>Consumer</tt> can process this data.
     *
     * @see ADAPRO::ADAPOS::PipelineProcessor
     * @see ADAPRO::ADAPOS::Consumer
     */
    class Producer: public PipelineProcessor
    {
        Producer() = delete;

        /**
         * A timestamp indicating the previous buffer overflow error message
         * printed.
         */
        uint64_t latest_overflow_ts = 0;

        /**
         * Checks for ring buffer overflow. If the given DPVAL object is dirty
         * when being updated, it means that the Consumer hadn't processed it
         * yet, so the previous value will be lost. If this is the case, the
         * <tt>BUFFER_OVERFLOW_FLAG</tt> returned. Also, an error message will
         * be printed at most once per minute.
         *
         * @param dpval The DPVAL object to be checked.
         * @return      <tt>BUFFER_OVERFLOW_FLAG</tt> if overflow was detected;
         * <tt>0</tt> otherwise.
         *
         * @see ADAPRO::ADAPOS::DPVAL:BUFFER_OVERFLOW_FLAG
         */
        inline uint16_t check_for_overflow(const DPVAL& dpval) noexcept;

    protected:

        /**
         * Produces a new service and informs the Consumer. When updating an
         * already existing service with a new DPVAL, <tt>update</tt> must be
         * used instead. <em>The <tt>Consumer</tt> will be notified of the new
         * service by setting the <tt>NEW_FLAG</tt> on the given DPVAL object.
         * </em>
         *
         * @param dpid  The DPID object that identifies a new ADAPOS service.
         * @param dpval The DPVAL containing first value of the new service.
         * <em>The operational flags (<tt>NEW</tt>, <tt>DIRTY</tt>,
         * <tt>WRITE</tt>, <tt>READ</tt>) are used for inter-Thread
         * communication between <tt>Producer</tt> and <tt>Consumer</tt>. Both
         * parties may freely modify these flags.</em> Other parts of the DPVAL
         * object are guaranteed to remain unchanged (by these two Threads).
         *
         * @see ADAPRO::ADAPOS::Producer::update
         * @see ADAPRO::ADAPOS::DataPointValue
         */
        void produce (const DPID& dpid, const DPVAL& dpval) noexcept;

        /**
         * Ibid.
         *
         * @param dpcom The DPCOM containing all the information related to the
         * new ADAPOS service.
         */
        void produce(const DataPointCompositeObject& dpcom) noexcept
        {
            produce(dpcom.id, dpcom.data);
        }

        /**
         * Writes the given parameters to the shared buffers in an atomic,
         * mutually excluded transaction. <em>This procedure must be called when
         * the DPID deque already contains an object at the given index.</em>
         * When adding a new service, <tt>produce</tt> must be invoked instead.
         *
         * @param dpid_index    Index in the image that the next parameter
         * belongs to.
         * @param dpval         The DPVAL object to be added in the image.
         * <em>The operational flags (<tt>NEW</tt>, <tt>DIRTY</tt>,
         * <tt>WRITE</tt>, <tt>READ</tt>) are used for inter-Thread
         * communication between <tt>Producer</tt> and <tt>Consumer</tt>. Both
         * parties may freely modify these flags.</em> Other parts of the DPVAL
         * object are guaranteed to remain unchanged (by these two Threads).
         *
         * @see ADAPRO::ADAPOS::Producer::produce
         */
        void update(const size_t dpid_index, const DPVAL& dpval) noexcept;


        /**
         * Sets the <tt>TERMINATOR_FLAG</tt> of a DPVAL in the shared ring
         * buffer to signal the end of trasmission to Consumer. This procedure
         * can be safely discarded (in terms of RAII) in the inheriting class.
         */
        virtual void finish() override;

        virtual void handle_exception(const char* message) noexcept override;

        /**
         * Constructor for Producer.
         *
         * @param logger                The Logger instance of the ADAPRO
         * Session.
         * @param buffer                The fixed-size shared ring buffer.
         * @param buffer_size           Size of the ring buffer.
         * @param registry              The deque containing the currently known
         * DPIDs. When an event belonging to a new service is produced, its
         * identifier is appended to the registry. Consumer may safely read from
         * any index of the registry at any time.
         * @param supervisor            The ADAPRO Supervisor instance.
         * @param name                  Name of the PipelineProcessor instance.
         * @param configuration         The configuration map of the ADAPRO
         * process.
         * @param preferred_core        Preferred CPU core number.
         * @param transition_callback   The transition callback.
         */
        Producer
        (
                ADAPRO::Control::Logger& logger,
                buffer_t buffer,
                const size_t buffer_size,
                registry_t& registry,
                ADAPRO::Control::Supervisor& supervisor,
                const std::string&& name,
                const ADAPRO::Data::config_t& configuration =
                        ADAPRO::Data::DEFAULT_CONFIGURATION,
                const int preferred_core = -1,
                const ADAPRO::Data::trans_cb_t transition_callback =
                        ADAPRO::Control::Thread::NOP_CB
        )
        noexcept;

        virtual ~Producer() noexcept {};
    };
}
}

#endif /* ADAPRO_PRODUCER_HPP */

