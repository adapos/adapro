/*
 * File:   ConfigurationUtils.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 17 January 2017, 15:20
 */

#ifndef ADAPRO_CONFIGURATION_UTILS_HPP
#define ADAPRO_CONFIGURATION_UTILS_HPP

#include <string>
#include <regex>
#include <list>
#include <memory>
#include <functional>
#include <map>
#include "../control/Logger.hpp"
#include "../data/Parameters.hpp"
#include "../data/Typedefs.hpp"

namespace ADAPRO
{
namespace Library
{
    /**
     * <p>Imports the map of key-value pairs from the given file, if possible.
     * If not, an exception is thrown to indicate the error.</p>
     * <p>The expected format of the key-value file is the following:</p>
     * <pre>
     * &lt;key-value-file&gt;   ::= &lt;line&gt; "\\n" &lt;key-value-file&gt;
     * &lt;line&gt;             ::= &lt;key&gt; &lt;whitespace&gt; &lt;key&gt; | ""
     * </pre>
     * <p>
     * where
     * </p>
     * <ol>
     *     <li><tt>&lt;key&gt;</tt> needs to satisfy the regular expression
     *         <tt>[a-zA-Z0-9_]+</tt>;</li>
     *     <li><tt>&lt;whitespace&gt;</tt> must be a string with length one of
     *         more, consisting of ASCII characters <tt>0x20</tt> (space) and
     *         <tt>0x09</tt> (horizontal tab); and</li>
     *     <li><tt>&lt;value&gt;</tt> must not contain whitespace characters
     *         (including newline, carriage return, etc.).</li>
     * </ol>
     * <p>
     * The character '#' and its successors in a single line will be ignored.
     * Each key can be only associated with one value.
     * </p>
     *
     * @param logger                The Logger instance for output.
     * @param initial_configuration The map into which the key-value pairs
     * loaded from the configuratoin file will be stored. Any existing values
     * will be overwritten in case of collision.
     * @param filenames             A vector containing filename candidates for
     * importing the map. This function will proceed with the first accessible
     * filename on the list or throw an exception if none of them can be
     * accessed.
     * @param allow_new_keys        If <tt>true</tt>, then the file is allowed
     * to contain keys that were not in the map already. Otherwise, an exception
     * is thrown if a previously unknown key is encountered. <em>This function
     * treats keys associated with empty string in the map, in the same way as
     * non-existing keys.</em>
     * @param key_predicate         A regular expression that will be used for
     * deciding whether or to accpet an individual configuration parameter key.
     * <em>This regular expression must not accept strings containing
     * whitespace (<tt>[:space:]</tt>).</em>
     * @param relation              <p>Relation that each key-value pair needs
     * to satisfy. Only the key-value pairs satisfying the relation will be
     * added to the map.</p>
     * <p>The implementation of the relation is expected to be a function
     * (e.g. a lambda expression) returning <tt>true</tt> for every pair of
     * <tt>std::string</tt> satisfying some conditions and <tt>false</tt> for
     * others. By default, this relation is simply the cartesian product of
     * <tt>std::string</tt> and <tt>std::string</tt> (i.e. a relation accepting
     * every key-value pair).</p><p>Note that a relation (in mathematical sense)
     * is not capable of enforcing restrictions on values that depend on
     * multiple keys.</p>
     * @param case_insensitive          The indexing operation of the map will
     * be case sensitive if and only if this parameter is <tt>true</tt>.
     * @return The key-value map read from the file.
     * @throws std::domain_error        This exception is thrown in the
     * following situations:
     * <ol>
     *     <li>If the file fails to satisfy the conditions stated above;</li>
     *     <li>if the file contained unknown keys that were forbidden; or</li>
     *     <li>if multiple definitions for the same key were found in the file.</li>
     * </ol>
     * @throws std::ios_base::failure   If none of the key-value files could be
     * accessed.
     */
    void import_configuration
    (
            ADAPRO::Control::Logger& logger,
            ADAPRO::Data::config_t& initial_configuration,
            const std::list<std::string>& filenames,
            const bool allow_new_keys = false,
            const std::regex& key_predicate = ADAPRO::Data::REGEX_WORD,
            const std::function<bool(const std::string&, const std::string&)>&
                    relation = ADAPRO::Data::DEFAULT_RELATION
    );

    inline void import_configuration
    (
            ADAPRO::Control::Logger& logger,
            ADAPRO::Data::config_t& initial_configuration,
            const std::string& filename,
            const bool allow_new_keys = false,
            const std::regex& key_predicate = ADAPRO::Data::REGEX_WORD,
            const std::function<bool(const std::string&, const std::string&)>&
                    relation = ADAPRO::Data::DEFAULT_RELATION)
    {
        import_configuration
        (
                logger,
                initial_configuration,
                std::list<std::string>{filename},
                allow_new_keys,
                key_predicate,
                relation
        );
    }

    /**
     * Writes the given map to a file. The output will be written in a format
     * that is specified in the documentation entry of <tt>import_map</tt>.
     *
     * @param key_value_map The map to be exported.
     * @param filename      Name of the output file.
     * @param key_predicate A regular expresion that every key must satisfy.
     * <em>This regular expression must not accept whitespace
     * (<tt>[:space:]</tt>)</em>. This parameter for ensuring that the
     * output will be compatible with <tt>import_configuration</tt>, so it
     * should be the same as the key predicate used for importing
     * configurations.
     * @param tabulate      If <tt>true</tt>, then the values will be tabulated
     * with spaces to begin at index <em>n</em>+1 at each row, where <em>n</em>
     * is the length of the longest key. For example, tabulating a map
     * <tt>{{"foo", "bar"}, {"very_long", "23"}, {"x", "4"}}</tt> would yield a
     * a file with the following contents:
     * <pre>
     * foo       bar
     * very_long 23
     * x         4
     * </pre>
     * @throws std::invalid_argument If the map contained key-value pairs that
     * would produce output incompatible with <tt>import_map</tt>.
     * @see ADAPRO::Library::import_map
     */
    void export_configuration
    (
            const ADAPRO::Data::config_t& key_value_map,
            const std::string& filename,
            const std::regex& key_predicate = ADAPRO::Data::REGEX_WORD,
            const bool tabulate = true
    );

    /**
     * A version of <tt>export_map</tt> that prints the map into
     * <tt>stdout</tt>. The output will be written in a format slightly
     * different than what is specified in the documentation entry of
     * <tt>import_map</tt>.
     *
     * @param logger        The Logger instance for output.
     * @param title         A heading that is printed before the values.
     * @param key_value_map The map to be printed.
     * @see ADAPRO::Library::import_map
     * @see ADAPRO::Library::export_map
     */
    void print_configuration
    (
            ADAPRO::Control::Logger& logger,
            const std::string& title,
            const ADAPRO::Data::config_t& key_value_map
    )
    noexcept;
}
}

#endif /* ADAPRO_CONFIGURATION_UTILS_HPP */

