/* 
 * File:   MetronomeFactory.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 May 2017, 13:28
 */
#ifndef ADAPRO_METRONOME_FACTORY_HPP
#define ADAPRO_METRONOME_FACTORY_HPP

#include <cstdint>
#include "../control/Logger.hpp"
#include "../control/Metronome.hpp"

namespace ADAPRO
{
namespace Library
{
    /**
     * A factory function with memoization in respect to the tick length. 
     * Returns a reference to a Metronome instance with the given Logger and
     * tick length. The affinity will be set only for a newly created Metronome
     * instance. If a cached Metronome is used, then the last argument will be
     * ignored. 
     * 
     * @param logger            Logger of the current ADAPRO Session.
     * @param tick_length       Metronome tick length in milliseconds.
     * @param preferred_core    Preferred core for the Metronome. This argument
     * may be ignored.
     * @return A reference to a Metronome instance.
     * 
     * @see ADAPRO::Control::Metronome
     */
    ADAPRO::Control::Metronome& get_metronome
    (
            ADAPRO::Control::Logger& logger, 
            const uint32_t tick_length = 1000,
            const int preferred_core = -1
    ) 
    noexcept;
}
}

#endif /* ADAPRO_METRONOME_FACTORY_HPP */

