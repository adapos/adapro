/*
 * File:   MetronomeTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 November 2016, 14:10:29
 */

#ifndef METRONOMETEST_HPP
#define METRONOMETEST_HPP

#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Logger.hpp"

class MetronomeTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(MetronomeTest);

    CPPUNIT_TEST(test_synchronize);

    CPPUNIT_TEST_SUITE_END();

public:
    MetronomeTest();
    virtual ~MetronomeTest();
    void setUp();
    void tearDown();

private:
    static ADAPRO::Control::Logger LOGGER;
    
    void test_synchronize();
    void test_aborted_synchronize();
};

#endif /* METRONOMETEST_HPP */

