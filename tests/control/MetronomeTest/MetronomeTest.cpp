/*
 * File:   MetronomeTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 2 November 2016, 14:10:29
 */

#include <cstdint>
#include <thread>
#include <chrono>
#include "MetronomeTest.hpp"
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Metronome.hpp"
#include "../../../headers/library/Clock.hpp"
#include "../../../headers/data/State.hpp"
#include "../../../headers/data/LoggingLevel.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;

CPPUNIT_TEST_SUITE_REGISTRATION(MetronomeTest);

ADAPRO::Control::Logger MetronomeTest::LOGGER(
        ADAPRO::Data::LoggingLevel::INFO);

MetronomeTest::MetronomeTest() {}

MetronomeTest::~MetronomeTest() {}

void MetronomeTest::setUp() {}

void MetronomeTest::tearDown() {}

void MetronomeTest::test_synchronize()
{
    Metronome m1(LOGGER);
    const bool condition_1 = m1.name == "Metronome<1000>-0";
    const bool condition_2 = Metronome::S_NO == 1;
    uint64_t beginning, duration;
    beginning = ADAPRO::Library::now();
    m1.synchronize();
    duration = ADAPRO::Library::now() - beginning;
    const bool condition_3(duration >= 500);
    LOGGER.print("m1.synchronize() took " + to_string(duration) + " ms.", INFO);

    Metronome m2(LOGGER, 100);
    const bool condition_4(m2.name == "Metronome<100>-1");
    const bool condition_5(Metronome::S_NO == 2);
    beginning = ADAPRO::Library::now();
    m2.synchronize(5);
    duration = ADAPRO::Library::now() - beginning;
    const bool condition_6(duration >= 500);
    LOGGER.print("m2.synchronize() took " + to_string(duration) + " ms.", INFO);

    CPPUNIT_ASSERT(condition_1);
    CPPUNIT_ASSERT(condition_2);
    CPPUNIT_ASSERT(condition_3);
    CPPUNIT_ASSERT(condition_4);
    CPPUNIT_ASSERT(condition_5);
    CPPUNIT_ASSERT(condition_6);
}

void MetronomeTest::test_aborted_synchronize()
{
    class Foo final: public Thread
    {
        Metronome& m;
    protected:
        virtual void prepare() override {}
        virtual void execute() override {m.synchronize(); ++counter;}
        virtual void finish() override {}
    public:
        uint32_t counter;
        Foo(Logger& logger, string&& name, Metronome& m):
                Thread(logger, std::move(name)), m(m), counter(0) {};
    };

    Metronome m(LOGGER, 1000);
    Foo foo_1(LOGGER, std::move("Foo-1"), m);
    Foo foo_2(LOGGER, std::move("Foo-2"), m);
    m.synchronize();
    foo_1.start(true);
    foo_2.start(true);
    m.abort("Simulating abnormal shutdown.");
    foo_1.stop(false);
    foo_2.stop(false);
    foo_1.wait_for_state(STOPPED);
    foo_2.wait_for_state(STOPPED);

    CPPUNIT_ASSERT(foo_1.counter == 0);
    CPPUNIT_ASSERT(foo_2.counter == 0);
}
