/*
 * File:   SupervisorTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 January 2017, 9:30
 */

#include <cstdint>
#include <iostream>
#include <string>
#include <memory>
#include <list>
#include <map>
#include <utility>
#include <thread>
#include <chrono>
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Worker.hpp"
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/LoggingLevel.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "SupervisorTest.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(SupervisorTest);

using namespace std;
using namespace ADAPRO::Control;

Logger SupervisorTest::LOGGER(ADAPRO::Data::LoggingLevel::ERROR);

SupervisorTest::SupervisorTest() {}

SupervisorTest::~SupervisorTest() {}

void SupervisorTest::setUp() {}

void SupervisorTest::tearDown() {
}

void SupervisorTest::test_default_constructor()
{
    Supervisor s(LOGGER);
    s.start(true);
    s.stop(true);
}

#define MAKE_THREAD_CLASS(type, sleep_duration)\
    class type: public Worker\
    {\
    protected:\
        virtual void prepare() override {}\
        virtual void execute() override {\
            this_thread::sleep_for(chrono::milliseconds(sleep_duration));\
        }\
        virtual void finish() override {}\
    public:\
        type(Logger& logger, Supervisor& supervisor, const ADAPRO::Data::config_t& configuration) noexcept:\
            Worker(LOGGER, supervisor, std::move(#type), configuration) {}\
        virtual ~type() noexcept {}\
    };\
    ADAPRO::Data::worker_factory_t make_##type = \
        [](Logger& logger, Supervisor& supervisor,\
                const ADAPRO::Data::config_t& configuration) {\
            return unique_ptr<Worker>(new type(logger, supervisor, configuration));\
        };

void SupervisorTest::test_serial_lifetime()
{
    MAKE_THREAD_CLASS(A, 10);
    MAKE_THREAD_CLASS(B, 10);
    MAKE_THREAD_CLASS(C, 10);
    ADAPRO::Data::config_t configuration(ADAPRO::Data::DEFAULT_CONFIGURATION);
    configuration[get<0>(ADAPRO::Data::SERIALIZE_COMMANDS)] = "TRUE";
    list<ADAPRO::Data::worker_factory_t> factories{make_A, make_B, make_C};
    uint32_t state(0);
    Supervisor s(LOGGER, &state, factories, configuration);

    s.start(true);
    s.print_report();
    this_thread::sleep_for(chrono::milliseconds(100));
    s.pause(true);
    s.print_report();
    this_thread::sleep_for(chrono::milliseconds(100));
    s.resume(true);
    s.print_report();
    this_thread::sleep_for(chrono::milliseconds(100));
    s.stop(true);
    s.print_report();
}

void SupervisorTest::test_parallel_lifetime()
{
    MAKE_THREAD_CLASS(D, 10);
    MAKE_THREAD_CLASS(E, 10);
    MAKE_THREAD_CLASS(F, 10);
    ADAPRO::Data::config_t configuration(ADAPRO::Data::DEFAULT_CONFIGURATION);
    configuration[get<0>(ADAPRO::Data::SERIALIZE_COMMANDS)] = "FALSE";
    list<ADAPRO::Data::worker_factory_t> factories{make_D, make_E, make_F};
    uint32_t state(0);
    Supervisor s(LOGGER, &state, factories, configuration);

    s.start(true);
    s.print_report();
    this_thread::sleep_for(chrono::milliseconds(100));
    s.pause(true);
    s.print_report();
    this_thread::sleep_for(chrono::milliseconds(100));
    s.resume(true);
    s.print_report();
    this_thread::sleep_for(chrono::milliseconds(100));
    s.stop(true);
    s.print_report();
}

void SupervisorTest::test_watchdog_mechanism()
{
    MAKE_THREAD_CLASS(D, 3000);
    ADAPRO::Data::config_t configuration(ADAPRO::Data::DEFAULT_CONFIGURATION);
    list<ADAPRO::Data::worker_factory_t> factories{make_D};
    uint32_t state(0);
    Supervisor s(LOGGER, &state, factories, configuration);

    s.start(true);
    this_thread::sleep_for(chrono::seconds(3));
    const uint64_t missed_checkpoints(s.workers.front()->missed_checkpoints);
    s.stop(true);
    s.print_report();

    CPPUNIT_ASSERT(missed_checkpoints >= 2 && missed_checkpoints <= 3);
}
