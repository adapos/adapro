/*
 * File:   ThreadTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 26.7.2015, 21:54:34
 */

#ifndef THREADTEST_HPP
#define	THREADTEST_HPP

#include <mutex>
#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/data/State.hpp"

class TestThread final: public ADAPRO::Control::Thread
{
    protected:

    virtual void prepare() override {}

    virtual void execute() override
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    virtual void finish() override {}

    public:

    TestThread(ADAPRO::Control::Logger& logger, const std::string&& name)
    noexcept:
            ADAPRO::Control::Thread(logger, std::move(name)) {}

    ~TestThread() {}

};

class ThreadTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(ThreadTest);

    CPPUNIT_TEST(test_get_state);
    CPPUNIT_TEST(test_get_command);
    CPPUNIT_TEST(test_state_in);
    CPPUNIT_TEST(test_command_in);
    CPPUNIT_TEST(test_report);
    CPPUNIT_TEST(test_destructor);
    CPPUNIT_TEST(test_start_stop);
    CPPUNIT_TEST(test_start_pause_stop);
    CPPUNIT_TEST(test_pause_resume);
    CPPUNIT_TEST(test_abort);
    CPPUNIT_TEST(test_abort_string);
    CPPUNIT_TEST(test_trap);
    CPPUNIT_TEST(test_trap_string);
    CPPUNIT_TEST(test_unhandled_exception_1);
    CPPUNIT_TEST(test_unhandled_exception_2);
    CPPUNIT_TEST(test_stop_aborted_thread);
    CPPUNIT_TEST(test_wait_for_state);
    CPPUNIT_TEST(test_wait_for_state_with_timeout);
    CPPUNIT_TEST(test_wait_for_state_mask);
    CPPUNIT_TEST(test_wait_for_state_mask_with_timeout);
    CPPUNIT_TEST(test_transition_callback);
    CPPUNIT_TEST(test_propagating_state_transition_callback);
    CPPUNIT_TEST(test_life_cycle);
    CPPUNIT_TEST(test_set_affinity);
    CPPUNIT_TEST(test_spinlock);
    CPPUNIT_TEST(test_spinlock_precision);

    CPPUNIT_TEST_SUITE_END();
public:
    ThreadTest();
    virtual ~ThreadTest();
    void setUp();
    void tearDown();

private:
    static ADAPRO::Control::Logger LOGGER;

    void test_get_state();
    void test_get_command();
    void test_state_in();
    void test_command_in();
    void test_report();
    void test_destructor();
    void test_start_stop();
    void test_start_pause_stop();
    void test_pause_resume();
    void test_abort();
    void test_abort_string();
    void test_trap();
    void test_trap_string();
    void test_unhandled_exception_1();
    void test_unhandled_exception_2();
    void test_stop_aborted_thread();
    void test_wait_for_state();
    void test_wait_for_state_with_timeout();
    void test_wait_for_state_mask();
    void test_wait_for_state_mask_with_timeout();
    void test_transition_callback();
    void test_propagating_state_transition_callback();
    void test_life_cycle();
    void test_set_affinity();
    void test_spinlock();
    void test_spinlock_precision();
};

#endif	/* THREADTEST_HPP */
