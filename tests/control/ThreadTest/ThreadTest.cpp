/*
 * File:   ThreadTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 26.7.2015, 21:54:34
 */

#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>
#include <atomic>
#include <stdexcept>
#include <sched.h>
#include <queue>
#include <list>
#include <unordered_map>
#include <utility>
#include "ThreadTest.hpp"
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/library/Clock.hpp"
#include "../../../headers/data/State.hpp"
#include "../../../headers/data/Command.hpp"
#include "../../../headers/data/LoggingLevel.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;

CPPUNIT_TEST_SUITE_REGISTRATION(ThreadTest);

Logger ThreadTest::LOGGER(WARNING);

ThreadTest::ThreadTest() {}

ThreadTest::~ThreadTest() {}

void ThreadTest::setUp() {}

void ThreadTest::tearDown() {}

void ThreadTest::test_get_state()
{
    TestThread t(LOGGER, std::move("test1"));
    CPPUNIT_ASSERT(READY == t.get_state());
}

void ThreadTest::test_get_command()
{
    TestThread t(LOGGER, std::move("test2"));
    CPPUNIT_ASSERT(CONTINUE == t.get_command());
}

void ThreadTest::test_state_in()
{
    TestThread t(LOGGER, std::move("test3"));
    CPPUNIT_ASSERT(!t.state_in(RUNNING | PAUSED));
    CPPUNIT_ASSERT(t.state_in(READY));
}

void ThreadTest::test_command_in()
{
    TestThread t(LOGGER, std::move("test4"));
    CPPUNIT_ASSERT(!t.command_in(PAUSE | STOP));
    CPPUNIT_ASSERT(t.command_in(CONTINUE));
}

void ThreadTest::test_report()
{
    TestThread t(LOGGER, std::move("test5"));
    const bool condition1 = t.report() == "test5: state = READY; command = CONTINUE";
    t.start(true);
    const bool condition2 = t.report() == "test5: state = RUNNING; command = CONTINUE";
    t.pause(true);
    const bool condition3 = t.report() == "test5: state = PAUSED; command = PAUSE";
    t.resume(true);
    this_thread::sleep_for(chrono::milliseconds(20));
    const bool condition4 = t.report() == "test5: state = RUNNING; command = CONTINUE";
    t.stop(true);
    const bool condition5 = t.report() == "test5: state = STOPPED; command = STOP";
    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
    CPPUNIT_ASSERT(condition3);
    CPPUNIT_ASSERT(condition4);
    CPPUNIT_ASSERT(condition5);
}

void ThreadTest::test_destructor()
{
    {
        TestThread t1(LOGGER, std::move("test6"));
    }
    {
        TestThread t3(LOGGER, std::move("test7"));
        t3.start(true);
        t3.stop(true);
    }
}


void ThreadTest::test_start_stop()
{
    TestThread t(LOGGER, std::move("test8"));
    t.start(true);
    const bool condition1 = CONTINUE    == t.get_command();
    const bool condition2 = RUNNING     == t.get_state();
    t.stop(true);
    const bool condition3 = STOP        == t.get_command();
    const bool condition4 = STOPPED     == t.get_state();
    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
    CPPUNIT_ASSERT(condition3);
    CPPUNIT_ASSERT(condition4);
}

void ThreadTest::test_start_pause_stop()
{
    TestThread t(LOGGER, std::move("test9"));
    t.start(true);
    t.pause(true);
    const bool condition1 = PAUSE       == t.get_command();
    const bool condition2 = PAUSED      == t.get_state();
    t.stop(true);
    const bool condition3 = STOP        == t.get_command();
    const bool condition4 = STOPPED     == t.get_state();
    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
    CPPUNIT_ASSERT(condition3);
    CPPUNIT_ASSERT(condition4);
}

void ThreadTest::test_pause_resume()
{
    class Bar final: public ADAPRO::Control::Thread
    {
        atomic<uint64_t> counter;
    protected:
        virtual void prepare() override {}
        virtual void execute() override
        {
            counter.store(
                    counter.load(memory_order_consume) + 1,
                    memory_order_release
            );
        }
        virtual void finish() override {}
    public:
        Bar(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name)),
                counter(0) {}
        uint64_t get_count() {return counter.load(memory_order_consume);}
        virtual ~Bar() {};
    };

    Bar bar(std::move("test10"));
    const bool condition1 = bar.get_count() == 0;
    bar.start(true);
    this_thread::sleep_for(chrono::milliseconds(10));
    uint64_t count;
    bar.pause(true);
    const uint64_t condition2 = (count = bar.get_count());
    this_thread::sleep_for(chrono::milliseconds(10));
    const bool condition3 = count == bar.get_count();
    bar.resume(true);
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition4 = count < bar.get_count();
    bar.stop(true);

    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
    CPPUNIT_ASSERT(condition3);
    CPPUNIT_ASSERT(condition4);
}

void ThreadTest::test_abort()
{
    class Qux final: public ADAPRO::Control::Thread
    {
    public:
        int count = 0;
        Qux(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Qux() {}
    protected:
        virtual void prepare() override {}
        virtual void execute() override {abort((const char*) "Abort test exception."); ++count;}
        virtual void finish() override {}
    };
    Qux t(std::move("test11"));
    t.start(true);
    t.wait_for_state(ABORTED);
    CPPUNIT_ASSERT(t.count == 1);
}

void ThreadTest::test_abort_string()
{
    class Quz final: public ADAPRO::Control::Thread
    {
    public:
        int count = 0;
        Quz(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Quz() {}
    protected:
        virtual void prepare() override {}
        virtual void execute() override {abort(string("Abort test exception.")); ++count;}
        virtual void finish() override {}
    };
    Quz t(std::move("test12"));
    t.start(true);
    t.wait_for_state(ABORTED);
    CPPUNIT_ASSERT(t.count == 1);
}

void ThreadTest::test_trap()
{
    class Quux final: public ADAPRO::Control::Thread
    {
    public:
        int count = 0;
        Quux(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Quux() {}
    protected:
        virtual void prepare() override {}
        virtual void execute() override {trap((const char*) "Trap test exception."); ++count;}
        virtual void finish() override {}
    };
    Quux t(std::move("test13"));
    t.start(true);
    t.wait_for_state(ABORTED);
    CPPUNIT_ASSERT(t.count == 0);
}

void ThreadTest::test_trap_string()
{
    class Xyz final: public ADAPRO::Control::Thread
    {
    public:
        int count = 0;
        Xyz(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Xyz() {}
    protected:
        virtual void prepare() override {}
        virtual void execute() override {trap(string("Trap test exception.")); ++count;}
        virtual void finish() override {}
    };
    Xyz t(std::move("test14"));
    t.start(true);
    t.wait_for_state(ABORTED);
    CPPUNIT_ASSERT(t.count == 0);
}

void ThreadTest::test_unhandled_exception_1()
{
    class Fizz final: public ADAPRO::Control::Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {throw domain_error("Throw test domain_error.");}
        virtual void finish() override {}
    public:
        Fizz(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Fizz() {}
    };
    Fizz t(std::move("test15"));
    t.start(true);
    t.wait_for_state(ABORTED);
}

void ThreadTest::test_unhandled_exception_2()
{
    class Buzz final: public ADAPRO::Control::Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {throw 4;}
        virtual void finish() override {}
    public:
        Buzz(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Buzz() {}
    };
    Buzz t(std::move("test16"));
    t.start(true);
    t.wait_for_state(ABORTED);
}

void ThreadTest::test_stop_aborted_thread()
{
    class Foo final: public ADAPRO::Control::Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {throw runtime_error("Throw test exception.");}
        virtual void finish() override {}
    public:
        Foo(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Foo() {}
    };

    Foo t(std::move("test17"));
    t.start(true);
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition1 = CONTINUE == t.get_command();
    const bool condition2 = ABORTED  == t.get_state();
    t.wait_for_state(ABORTED);
    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
}

void ThreadTest::test_wait_for_state()
{
    TestThread t(LOGGER, std::move("test18"));
    t.wait_for_state(READY);
    t.start(true);
    t.wait_for_state(RUNNING);
    t.pause(true);
    t.wait_for_state(PAUSED);
    t.resume(true);
    t.wait_for_state(RUNNING);
    t.stop();
    t.wait_for_state(STOPPING);
    t.wait_for_state(STOPPED);
}

void ThreadTest::test_wait_for_state_with_timeout()
{
    class Bor final: public ADAPRO::Control::Thread
    {
        virtual void prepare() override {}
        virtual void execute() override
        {
            this_thread::sleep_for(chrono::seconds(1));
        }
        virtual void finish() override {}
    public:
        Bor(const string&& name) noexcept:
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Bor() noexcept {}
    };

    Bor t("test19");
    t.start(true);
    this_thread::sleep_for(chrono::milliseconds(10));
    t.stop(false);
    bool timeout(false);
    try
    {
        t.wait_for_state(STOPPED, chrono::milliseconds(10));
    }
    catch (const std::runtime_error& e)
    {
        cout << "    wait_for_state timed out." << endl;
        timeout = true;
    }
    t.wait_for_state(STOPPED);
    CPPUNIT_ASSERT(timeout);
}

void ThreadTest::test_wait_for_state_mask()
{
    TestThread t(LOGGER, std::move("test20"));
    t.start(true);
    t.wait_for_state_mask((uint8_t) RUNNING | PAUSED);
    t.pause(true);
    t.wait_for_state_mask((uint8_t) PAUSED);
    t.resume(true);
    t.wait_for_state_mask((uint8_t) RUNNING);
    t.stop();
    t.wait_for_state_mask((uint8_t) STOPPED | ABORTED);
}

void ThreadTest::test_wait_for_state_mask_with_timeout()
{
    class Zyx final: public ADAPRO::Control::Thread
    {
        virtual void prepare() override {}
        virtual void execute() override
        {
            this_thread::sleep_for(chrono::seconds(1));
        }
        virtual void finish() override {}
    public:
        Zyx(const string&& name) noexcept:
                ADAPRO::Control::Thread(LOGGER, std::move(name)) {}
        virtual ~Zyx() noexcept {}
    };

    Zyx t("test21");
    t.start(true);
    this_thread::sleep_for(chrono::milliseconds(10));
    t.stop(false);
    bool timeout(false);
    try
    {
        t.wait_for_state_mask((uint8_t) STOPPED | ABORTED, chrono::milliseconds(10));
    }
    catch (const std::runtime_error& e)
    {
        cout << "    wait_for_state timed out." << endl;
        timeout = true;
    }
    t.wait_for_state_mask((uint8_t) STOPPED);
    CPPUNIT_ASSERT(timeout);
}

void ThreadTest::test_transition_callback()
{
    class Baz final: public ADAPRO::Control::Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {}
        virtual void finish() override {}
    public:
        atomic_int counter;
        Baz(const std::string&& name):
                ADAPRO::Control::Thread(LOGGER, std::move(name),
                        [this](const State s){atomic_fetch_add(&counter, 1);}),
                counter(0) {}
        virtual ~Baz() {}
    };
    Baz t(std::move("test22"));
    const bool condition1 = 0 == t.counter.load(memory_order_consume);
    t.start(true);
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition2 = 2 == t.counter.load(memory_order_consume);
    t.pause(true);
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition3 = 3 == t.counter.load(memory_order_consume);
    t.resume(true);
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition4 = 4 == t.counter.load(memory_order_consume);
    t.stop(true);
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition5 = 6 == t.counter.load(memory_order_consume);

    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
    CPPUNIT_ASSERT(condition3);
    CPPUNIT_ASSERT(condition4);
    CPPUNIT_ASSERT(condition5);
}

void ThreadTest::test_propagating_state_transition_callback()
{
    class Fuu final: public Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override
        {
            this_thread::sleep_for(chrono::milliseconds(10));
        }
        virtual void finish() override {}
    public:
        TestThread subordinate;
        Fuu(const std::string&& name, const std::string&& subordinate_name):
                Thread(LOGGER, std::move(name),
                STATE_PROPAGATION_CB({&subordinate})),
                subordinate(LOGGER, std::move(subordinate_name)) {};

        virtual string report() noexcept override
        {
            return Thread::report() + "\n        " + subordinate.report();
        }
    };

    Fuu t("test23", "test24");
    const bool condition1(t.get_state() == READY);
    const bool condition2(t.get_state() == t.subordinate.get_state());
    t.start(true);
    const bool condition3(t.get_state() == RUNNING);
    const bool condition4(t.get_state() == t.subordinate.get_state());
    t.pause(true);
    this_thread::sleep_for(chrono::milliseconds(20));
    const bool condition5(t.get_state() == PAUSED);
    const bool condition6(t.get_state() == t.subordinate.get_state());
    t.resume(true);
    this_thread::sleep_for(chrono::milliseconds(20));
    const bool condition7(t.get_state() == RUNNING);
    const bool condition8(t.get_state() == t.subordinate.get_state());
    t.stop(true);
    const bool condition9(t.get_state() == STOPPED);
    const bool condition10(t.get_state() == t.subordinate.get_state());
    cout << t.report() << endl;

    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
    CPPUNIT_ASSERT(condition3);
    CPPUNIT_ASSERT(condition4);
    CPPUNIT_ASSERT(condition5);
    CPPUNIT_ASSERT(condition6);
    CPPUNIT_ASSERT(condition7);
    CPPUNIT_ASSERT(condition8);
    CPPUNIT_ASSERT(condition9);
    CPPUNIT_ASSERT(condition10);
}

void ThreadTest::test_life_cycle()
{
    TestThread t(LOGGER, std::move("test25"));
    const bool condition1 = CONTINUE == t.get_command();
    const bool condition2 = READY    == t.get_state();
    t.start(true);
    const bool condition3 = CONTINUE == t.get_command();
    const bool condition4 = RUNNING  == t.get_state();
    t.stop(true);
    const bool condition5 = STOP     == t.get_command();
    const bool condition6 = STOPPED  == t.get_state();

    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
    CPPUNIT_ASSERT(condition3);
    CPPUNIT_ASSERT(condition4);
    CPPUNIT_ASSERT(condition5);
    CPPUNIT_ASSERT(condition6);
}

void ThreadTest::test_set_affinity()
{
    mutex mutex_obj;
    using pair_t = pair<const size_t, const size_t>;
    queue<pair_t> threads_and_cores;
    class Boz final: public ADAPRO::Control::Thread
    {
        const size_t number;
        queue<pair_t>& threads_and_cores;
        mutex& mutex_obj;
    protected:
        virtual void prepare() override {}
        virtual void execute() override
        {
            {
                lock_guard<mutex> lock(mutex_obj);
                threads_and_cores.push(
                        pair<const size_t, const size_t>(number, (size_t) sched_getcpu()));
            }
            this_thread::sleep_for(chrono::milliseconds(10));
        }
        virtual void finish() override {}
    public:
        Boz(const string&& name, const size_t number,
                queue<pair_t>& threads_and_cores, mutex& mutex_obj) noexcept:
                ADAPRO::Control::Thread(LOGGER, std::move(name), number),
                number(number),
                threads_and_cores(threads_and_cores),
                mutex_obj(mutex_obj) {}
        virtual ~Boz() noexcept {}
    };

    Boz t(std::move("test26"), 0, threads_and_cores, mutex_obj);
    Boz u(std::move("test27"), 1, threads_and_cores, mutex_obj);
    Boz v(std::move("test28"), 2, threads_and_cores, mutex_obj);
    t.start(true);
    u.start(true);
    v.start(true);
    this_thread::sleep_for(chrono::seconds(10));
    t.stop(true);
    u.stop(true);
    v.stop(true);
    unordered_map<size_t, size_t> expected{{0,0}, {1,1}, {2,2}};
    while (!threads_and_cores.empty())
    {
        pair_t actual = threads_and_cores.front();
        CPPUNIT_ASSERT(expected[actual.first] == actual.second);
//        cout << '(' << actual.first << ',' << actual.second << ')' << endl;
        threads_and_cores.pop();
    }
}

void ThreadTest::test_spinlock()
{
    chrono::high_resolution_clock::time_point beginning =
            chrono::high_resolution_clock::now();
    Thread::SPINLOCK(chrono::nanoseconds(1000));
    chrono::nanoseconds elapsed = chrono::duration_cast<chrono::nanoseconds>(
            chrono::high_resolution_clock::now() - beginning
    );
    cout << "Slept for " << elapsed.count() << "ns." << endl;
    CPPUNIT_ASSERT(elapsed.count() >= 1000);
}

void ThreadTest::test_spinlock_precision()
{
    chrono::high_resolution_clock::time_point beginning;
    uint64_t min = 1000000000, max = 0, sum = 0;
    __attribute__((unused)) uint64_t measurements[1000]; // This is not unused, but GCC thinks so!
    for (size_t i = 0; i < 1000; ++i)
    {
        beginning = chrono::high_resolution_clock::now();
        Thread::SPINLOCK(chrono::nanoseconds(1000));
        const uint64_t interval = chrono::duration_cast<chrono::nanoseconds>(
                chrono::high_resolution_clock::now() - beginning
        ).count();
//        cout << "Slept for " << interval << " ns." << endl;
        sum += interval;
        if (interval < min)
        {
            min = interval;
        }
        else if (interval > max)
        {
            max = interval;
        }
        measurements[i] = interval;
    }

    double avg = sum / 1000.0;
    cout << "Minimum spinlock duration: " << min << " ns." << endl;
    cout << "Maximum spinlock duration: " << max << " ns." << endl;
    cout << "Average spinlock duration: " << avg << " ns." << endl;
}
