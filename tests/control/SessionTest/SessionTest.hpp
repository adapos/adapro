/*
 * File:   SessionTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 24 March 2017, 14:25:13
 */

#ifndef SESSIONTEST_HPP
#define SESSIONTEST_HPP

#include <cstdint>
#include <thread>
#include <atomic>
#include <cppunit/extensions/HelperMacros.h>

class SessionTest : public CPPUNIT_NS::TestFixture 
{
    CPPUNIT_TEST_SUITE(SessionTest);

    CPPUNIT_TEST(test_worker_crash);
    CPPUNIT_TEST(test_SIGSEGV_handler);
    CPPUNIT_TEST(test_SIGABRT_handler);
    CPPUNIT_TEST(test_SIGINT_handler);
    CPPUNIT_TEST(test_SIGHUP_handler);
//    CPPUNIT_TEST(test_unsupported_signal);
//    CPPUNIT_TEST(test_unhandled_exception);
    CPPUNIT_TEST(test_good_session);
    CPPUNIT_TEST(test_good_session_with_configuration_file_access);

    CPPUNIT_TEST_SUITE_END();

public:
    SessionTest();
    virtual ~SessionTest();
    void setUp();
    void tearDown();

private:
    static std::thread EXCEPTION_THROWER;
    std::thread helper;
    static atomic<size_t> SIGSEGV_COUNT;
    static atomic<size_t> SIGABRT_COUNT;
    static atomic<size_t> SIGINT_COUNT;
    static atomic<size_t> SIGHUP_COUNT;
    static atomic<uint8_t> EXIT_CODE;
    static atomic_bool THROW_PERMITTED;
    
    inline static void GIVE_THROW_PERMISSION() noexcept
    {
        EXCEPTION_THROWER.detach();
        THROW_PERMITTED.store(true, memory_order_release);
    };
    
    void test_worker_crash();
    void try_join(thread& helper);

    void test_SIGSEGV_handler();
    void test_SIGABRT_handler();
    void test_SIGINT_handler();
    void test_SIGHUP_handler();
    void test_unsupported_signal();
    void test_unhandled_exception();
    void test_good_session();
    void test_good_session_with_configuration_file_access();
};

#endif /* SESSIONTEST_HPP */

