/*
 * File:   SessionTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 24 March 2017, 14:25:13
 */

#include <cstdint>
#include <csignal>
#include <string>
#include <list>
#include <map>
#include <utility>
#include <functional>
#include <regex>
#include <memory>
#include <thread>
#include <chrono>
#include <atomic>
#include <iostream>
#include <exception>
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/control/Session.hpp"
#include "../../../headers/data/Context.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/StatusFlags.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "SessionTest.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(SessionTest);

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

std::thread SessionTest::EXCEPTION_THROWER([](){
    while (!THROW_PERMITTED.load(memory_order_consume))
    {
        this_thread::sleep_for(chrono::milliseconds(10));
    }
//    throw runtime_error("Test exception.");
    throw runtime_error("");
//    terminate();
});

atomic<size_t> SessionTest::SIGSEGV_COUNT(0);
atomic<size_t> SessionTest::SIGABRT_COUNT(0);
atomic<size_t> SessionTest::SIGINT_COUNT(0);
atomic<size_t> SessionTest::SIGHUP_COUNT(0);
atomic<uint8_t> SessionTest::EXIT_CODE(0);
atomic_bool SessionTest::THROW_PERMITTED(false);

SessionTest::SessionTest() {}

SessionTest::~SessionTest() {}

void SessionTest::setUp()
{
    Session::SHUTDOWN_COUNTER = 0;
    EXIT_CODE.store(0, memory_order_release);
    SIGSEGV_COUNT.store(0, memory_order_release);
    SIGABRT_COUNT.store(0, memory_order_release);
    SIGINT_COUNT.store(0, memory_order_release);
    SIGHUP_COUNT.store(0, memory_order_release);
}

void SessionTest::tearDown() {}

#define BOILERPLATE(n, update_configuration, action) \
    class TestWorker##n final : public Worker\
    {\
    protected:\
        virtual void prepare() override {}\
        virtual void execute() override\
        {\
            this_thread::sleep_for(chrono::milliseconds(100));\
            action;\
        }\
        virtual void finish() override {}\
    public:\
        TestWorker##n(Logger& logger, Supervisor& s) noexcept: \
                Worker(logger, s, std::move("TestWorker"#n)) {}\
        virtual ~TestWorker##n() noexcept\
        {\
            print("Destructor called.");\
        }\
    };\
    helper = thread([](){\
            const int argc(2);\
            const char* argv[2]{"executable", "tests/resources/empty.conf"};\
            const list<ADAPRO::Data::worker_factory_t> worker_factories\
            {\
                [](Logger& logger, Supervisor& supervisor,\
                        const config_t& configuration)\
                {\
                    return make_unique<TestWorker##n>(logger, supervisor);\
                }\
            };\
            const list<string> configuration_paths\
            {\
                "tests/resources/unreachable.conf"\
            };\
            config_t configuration(DEFAULT_CONFIGURATION);\
            configuration[get<0>(MAIN_CORE)] = "1";\
            configuration[get<0>(LOGGING_MASK)] = "FEW";\
            configuration[get<0>(DIM_SERVER_ENABLED)] = "FALSE";\
            const map<uint8_t, function<void(void)>> signal_callbacks\
            {\
                {SIGSEGV, [](){++SIGSEGV_COUNT;}},\
                {SIGABRT, [](){++SIGABRT_COUNT;}},\
                {SIGINT, [](){++SIGINT_COUNT;}},\
                {SIGHUP, [](){++SIGHUP_COUNT;}}\
            };\
            const function<void(const uint8_t)> exit_handler = [](const uint8_t code){\
                    Session::PRINT("Exit code: " + to_string(code), USER_DEBUG);\
                    EXIT_CODE.store(code, memory_order_seq_cst);\
            };\
            Session session(Context(\
                    "Context"#n,\
                    argc,\
                    argv,\
                    worker_factories,\
                    configuration_paths,\
                    configuration,\
                    update_configuration,\
                    false,\
                    DEFAULT_RELATION,\
                    signal_callbacks,\
                    exit_handler\
            ));\
            EXIT_CODE.store(session.run(), memory_order_release);});

void SessionTest::try_join(thread& helper)
{
    if (helper.joinable())
    {
        helper.join();
    }
    else
    {
        cerr << "helper was not joinable." << endl;
        helper.detach();
    }
}

void SessionTest::test_worker_crash()
{
    BOILERPLATE(1, false, trap("test"));
    try_join(helper);
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == FLAG_WORKER);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 0);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 0);
}

void SessionTest::test_SIGSEGV_handler()
{
    BOILERPLATE(2, false, thread([](){raise(SIGSEGV);}).detach(); pause(false););
    try_join(helper);
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == FLAG_SIGSEGV);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 1);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 0);
}

void SessionTest::test_SIGABRT_handler()
{
    BOILERPLATE(3, false, thread([](){raise(SIGABRT);}).detach(); pause(false););
    try_join(helper);
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == FLAG_SIGABRT);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 0);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 1);
}

void SessionTest::test_SIGINT_handler()
{
    BOILERPLATE(4, false, thread([](){raise(SIGINT);}).detach(); pause(false););
    try_join(helper);
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == FLAG_SIGINT);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 1);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 0);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 0);
}

void SessionTest::test_SIGHUP_handler()
{
    BOILERPLATE(5, false, thread([](){raise(SIGHUP);}).detach(); pause(false););
    try_join(helper);
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == FLAG_SIGHUP);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 1);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 0);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 0);
}

void SessionTest::test_unsupported_signal()
{
    BOILERPLATE(6, false, thread([](){raise(SIGFPE);}).detach(); pause(false););
    try_join(helper);
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == FLAG_SIGHUP);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 0);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 0);
}

void SessionTest::test_unhandled_exception()
{
    BOILERPLATE(7, false, GIVE_THROW_PERMISSION(); pause(false););
    try_join(helper);
//    cout << "EXIT_CODE = " << to_string(EXIT_CODE.load(memory_order_consume)) << endl;
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == FLAG_UNHANDLED_EXCEPTION);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 0);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 0);
}

void SessionTest::test_good_session()
{
    static size_t I(0);
    BOILERPLATE(8, false, if (++I < 3) {print("Tick");} else {stop(false);});
    try_join(helper);
    CPPUNIT_ASSERT(I                == 3);
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == 0);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 0);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 0);
}

void SessionTest::test_good_session_with_configuration_file_access()
{
    static size_t J(0);
    BOILERPLATE(8, true, if (++J < 3) {print("Tick");} else {stop(false);});
    try_join(helper);
    CPPUNIT_ASSERT(J                == 3);
    CPPUNIT_ASSERT(EXIT_CODE.load(memory_order_consume)     == 0);
    CPPUNIT_ASSERT(SIGINT_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGHUP_COUNT.load(memory_order_consume)  == 0);
    CPPUNIT_ASSERT(SIGSEGV_COUNT.load(memory_order_consume) == 0);
    CPPUNIT_ASSERT(SIGABRT_COUNT.load(memory_order_consume) == 0);
}
