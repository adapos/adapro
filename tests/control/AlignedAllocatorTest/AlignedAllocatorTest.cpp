/*
 * File:   AlignedAllocatorTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 22 February 2017, 13:12:01
 */

#include <vector>
#include "../../../headers/control/AlignedAllocator.hpp"
#include "../../../headers/ADAPOS/DataPointCompositeObject.hpp"
#include "../../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../../headers/ADAPOS/DataPointValue.hpp"
#include "../../../headers/ADAPOS/DataPointEventRecord.hpp"
#include "../../../headers/ADAPOS/Typedefs.hpp"
#include "AlignedAllocatorTest.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::ADAPOS;

CPPUNIT_TEST_SUITE_REGISTRATION(AlignedAllocatorTest);

AlignedAllocatorTest::AlignedAllocatorTest() {}

AlignedAllocatorTest::~AlignedAllocatorTest() {}

void AlignedAllocatorTest::setUp() {}

void AlignedAllocatorTest::tearDown() {
}

void AlignedAllocatorTest::test()
{
    char* garbage(new char[22]);
    vector<DPID> unaligned_dpids(7);
//    CPPUNIT_ASSERT((((uint64_t) unaligned_dpids.data()) & 0x3F) == 0); // May or may not be aligned.
    vector<DPID, AlignedAllocator<DPID>> aligned_dpids(11);
    CPPUNIT_ASSERT((((uint64_t) aligned_dpids.data()) & 0x3F) == 0);

    vector<DPVAL, AlignedAllocator<DPVAL>> aligned_dpvals(13);
    CPPUNIT_ASSERT((((uint64_t) aligned_dpvals.data()) & 0x3F) == 0);

    vector<DPCOM, AlignedAllocator<DPCOM>> aligned_dpcoms(17);
    CPPUNIT_ASSERT((((uint64_t) aligned_dpcoms.data()) & 0x7F) == 0);

    vector<DPER, AlignedAllocator<DPER>> aligned_dpers(17);
    CPPUNIT_ASSERT((((uint64_t) aligned_dpers.data()) & 0x7F) == 0);

    delete[] garbage;
}
