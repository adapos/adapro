/*
 * File:   LoggerTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 31 January 2017, 14:05:43
 */

#include <cstdio>
#include <fstream>
#include <string>
#include <regex>
#include <iostream>
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/data/LoggingLevel.hpp"
#include "LoggerTest.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

CPPUNIT_TEST_SUITE_REGISTRATION(LoggerTest);

LoggerTest::LoggerTest() {}

LoggerTest::~LoggerTest() {}

void LoggerTest::setUp() {}

void LoggerTest::tearDown() {}

void LoggerTest::test()
{
    class TestThread final : public Thread
    {
    protected:
        virtual void prepare() override {}
        virtual void execute() override {}
        virtual void finish() override {}
    public:
        TestThread(Logger& logger) noexcept: Thread(logger, std::move("ScapeGoat")) {}
        virtual ~TestThread() noexcept {}
    };

    Logger* logger_ptr = new Logger(logging_mask("DISWEF"), "tests/resources/output.log");
    TestThread t(*logger_ptr);

    logger_ptr->print("This is a hidden test message.", ADAPRO_DEBUG);
    logger_ptr->print("This is a test message.",        USER_DEBUG);
    logger_ptr->print("Another test message.",          INFO);
    logger_ptr->print("Third test message.",            SPECIAL);
    logger_ptr->print(t, "It was my fault!",            WARNING);
    logger_ptr->print("Testing the error stream.",      ERROR);
    logger_ptr->print("The final message.",             FATAL);
    delete logger_ptr;

    const regex regexes[]
    {
        regex(".*\\[\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\] \\(D\\) "
                "This is a test message\\..*"),
        regex("^\\[\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\] \\(I\\) "
                "Another test message\\.$"),
        regex(".*\\[\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\] \\(S\\) "
                "Third test message\\..*"),
        regex(".*\\[\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\] \\(W\\) "
                "<ScapeGoat,READY,CONTINUE,\\d+> It was my fault!.*"),
        regex(".*\\[\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\] \\(E\\) "
                "Testing the error stream\\..*"),
        regex(".*\\[\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\] \\(F\\) "
                "The final message\\..*")
    };
    ifstream ifs("tests/resources/output.log");
    CPPUNIT_ASSERT(ifs.is_open());
    size_t i(0);
    string line;
    bool satisfies_regexes(true);
    while (getline(ifs, line))
    {
        satisfies_regexes = satisfies_regexes &&
                regex_match(line, regexes[i]);
        ++i;
    }
    CPPUNIT_ASSERT(remove("tests/resources/output.log") == 0);
    CPPUNIT_ASSERT(i == 6);
    CPPUNIT_ASSERT(satisfies_regexes);
}