
/*
 * File:   LoggerTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 31 January 2017, 14:05:43
 */

#ifndef LOGGERTEST_HPP
#define LOGGERTEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class LoggerTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(LoggerTest);

    CPPUNIT_TEST(test);

    CPPUNIT_TEST_SUITE_END();

public:
    LoggerTest();
    virtual ~LoggerTest();
    void setUp();
    void tearDown();

private:
    void test();
};

#endif /* LOGGERTEST_HPP */

