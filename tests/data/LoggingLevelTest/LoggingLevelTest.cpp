
/*
 * File:   LoggingLevelTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:41:51
 */

#include <string>
#include <stdexcept>
#include "LoggingLevelTest.hpp"
#include "../../../headers/library/GenericFunctions.hpp"
#include "../../../headers/data/LoggingLevel.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(LoggingLevelTest);

using namespace std;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

LoggingLevelTest::LoggingLevelTest() {}

LoggingLevelTest::~LoggingLevelTest() {}

void LoggingLevelTest::setUp() {}

void LoggingLevelTest::tearDown() {}

void LoggingLevelTest::test_read()
{
    CPPUNIT_ASSERT(ADAPRO_DEBUG == read<LoggingLevel>("ADAPRO"));
    CPPUNIT_ASSERT(USER_DEBUG   == read<LoggingLevel>("DEBUG"));
    CPPUNIT_ASSERT(INFO         == read<LoggingLevel>("INFO"));
    CPPUNIT_ASSERT(SPECIAL      == read<LoggingLevel>("SPECIAL"));
    CPPUNIT_ASSERT(WARNING      == read<LoggingLevel>("WARNING"));
    CPPUNIT_ASSERT(ERROR        == read<LoggingLevel>("ERROR"));
    CPPUNIT_ASSERT(FATAL        == read<LoggingLevel>("FATAL"));
    bool exception_1(false);
    try
    {
        read<LoggingLevel>("INVALID");
    }
    catch (const domain_error& e)
    {
        exception_1 = true;
    }
    CPPUNIT_ASSERT(exception_1);
    bool exception_2(false);
    try
    {
        read<LoggingLevel>("debug");
    }
    catch (const domain_error& e)
    {
        exception_2 = true;
    }
    CPPUNIT_ASSERT(exception_2);
}

void LoggingLevelTest::test_symbol()
{
    CPPUNIT_ASSERT('A' == symbol(ADAPRO_DEBUG));
    CPPUNIT_ASSERT('D' == symbol(USER_DEBUG));
    CPPUNIT_ASSERT('I' == symbol(INFO));
    CPPUNIT_ASSERT('S' == symbol(SPECIAL));
    CPPUNIT_ASSERT('W' == symbol(WARNING));
    CPPUNIT_ASSERT('E' == symbol(ERROR));
    CPPUNIT_ASSERT('F' == symbol(FATAL));
    bool exception(false);
    try
    {
        symbol((LoggingLevel) 0x07);
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

