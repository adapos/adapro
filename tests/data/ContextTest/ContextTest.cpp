/*
 * File:   ContextTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 22 May 2017, 12:22:26
 */

#include <string>
#include <list>
#include <functional>
#include <memory>
#include "ContextTest.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/Worker.hpp"
#include "../../../headers/control/Session.hpp"
#include "../../../headers/data/Context.hpp"
#include "../../../headers/data/Typedefs.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(ContextTest);

ContextTest::ContextTest() {}

ContextTest::~ContextTest() {}

void ContextTest::setUp() {}

void ContextTest::tearDown() {
}

void ContextTest::test_ctor_dtor()
{
    ADAPRO::Data::Context ctx
    (
            "ContextTest",
            0,
            nullptr,
            std::list<ADAPRO::Data::worker_factory_t>(),
            std::list<std::string>()
    );
}
