
/*
 * File:   ReportTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:26:47
 */

#include <string>
#include <stdexcept>
#include "ReportTest.hpp"
#include "../../../headers/library/GenericFunctions.hpp"
#include "../../../headers/data/Report.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(ReportTest);

using namespace std;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

ReportTest::ReportTest() {}

ReportTest::~ReportTest() {}

void ReportTest::setUp() {}

void ReportTest::tearDown() {}

void ReportTest::test_show()
{
    CPPUNIT_ASSERT("threads"        == show<Report>(THREADS));
    CPPUNIT_ASSERT("configuration"  == show<Report>(CONFIGURATION));
    CPPUNIT_ASSERT("environment"    == show<Report>(ENVIRONMENT));
    CPPUNIT_ASSERT("threads"        == show<Report>((Report) 0x00));
    CPPUNIT_ASSERT("configuration"  == show<Report>((Report) 0x01));
    CPPUNIT_ASSERT("environment"    == show<Report>((Report) 0x02));
    bool exception(false);
    try
    {
        show<Report>((Report) 0x07);
    }
    catch (const domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}
