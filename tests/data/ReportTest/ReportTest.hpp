
/*
 * File:   ReportTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:26:47
 */

#ifndef REPORTTEST_HPP
#define REPORTTEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class ReportTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(ReportTest);

    CPPUNIT_TEST(test_show);

    CPPUNIT_TEST_SUITE_END();

public:
    ReportTest();
    virtual ~ReportTest();
    void setUp();
    void tearDown();

private:
    void test_show();
};

#endif /* REPORTTEST_HPP */

