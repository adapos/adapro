
/*
 * File:   CommandTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:16:16
 */

#ifndef COMMANDTEST_HPP
#define COMMANDTEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class CommandTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(CommandTest);

    CPPUNIT_TEST(test_show);

    CPPUNIT_TEST_SUITE_END();

public:
    CommandTest();
    virtual ~CommandTest();
    void setUp();
    void tearDown();

private:
    void test_show();
};

#endif /* COMMANDTEST_HPP */

