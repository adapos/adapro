
/*
 * File:   ParametersTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 7 June 2017, 12:38:10
 */

#include "ParametersTest.hpp"
#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <utility>
#include <functional>
#include "../../../headers/data/Parameters.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(ParametersTest);

using namespace std;
using namespace ADAPRO::Data;

ParametersTest::ParametersTest() {}

ParametersTest::~ParametersTest() {}

void ParametersTest::setUp() {}

void ParametersTest::tearDown() {}

void ParametersTest::test_regexes()
{
    CPPUNIT_ASSERT( regex_match("-1",               REGEX_INTEGER));
    CPPUNIT_ASSERT( regex_match("0",                REGEX_INTEGER));
    CPPUNIT_ASSERT( regex_match("1",                REGEX_INTEGER));
    CPPUNIT_ASSERT( regex_match("2047",             REGEX_INTEGER));
    CPPUNIT_ASSERT(!regex_match("",                 REGEX_INTEGER));
    CPPUNIT_ASSERT(!regex_match("12\n",             REGEX_INTEGER));
    CPPUNIT_ASSERT(!regex_match("\n12",             REGEX_INTEGER));
    CPPUNIT_ASSERT(!regex_match("false",            REGEX_INTEGER));
    CPPUNIT_ASSERT(!regex_match("-1.0",             REGEX_INTEGER));

    CPPUNIT_ASSERT(!regex_match("-1",               REGEX_UNSIGNED));
    CPPUNIT_ASSERT( regex_match("0",                REGEX_UNSIGNED));
    CPPUNIT_ASSERT( regex_match("1",                REGEX_UNSIGNED));
    CPPUNIT_ASSERT( regex_match("2047",             REGEX_UNSIGNED));
    CPPUNIT_ASSERT(!regex_match("",                 REGEX_UNSIGNED));
    CPPUNIT_ASSERT(!regex_match("12\n",             REGEX_UNSIGNED));
    CPPUNIT_ASSERT(!regex_match("\n12",             REGEX_UNSIGNED));
    CPPUNIT_ASSERT(!regex_match("false",            REGEX_UNSIGNED));
    CPPUNIT_ASSERT(!regex_match("-1.0",             REGEX_UNSIGNED));

    CPPUNIT_ASSERT( regex_match("0.0.0.0",          REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT( regex_match("192.168.10.1",     REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("",                 REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("false",            REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("1.2.3",            REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("\n192.168.10.1",   REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("192.168.10.1\n",   REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("292.168.10.1",     REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("192.268.10.1",     REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("192.168.300.1",    REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("192.168.10.256",   REGEX_IPV4_ADDRESS));
    CPPUNIT_ASSERT(!regex_match("3232238081",       REGEX_IPV4_ADDRESS));

    CPPUNIT_ASSERT( regex_match("757",              REGEX_HOSTNAME));
    CPPUNIT_ASSERT( regex_match("localhost",        REGEX_HOSTNAME));
    CPPUNIT_ASSERT( regex_match("test.domain",      REGEX_HOSTNAME));
    CPPUNIT_ASSERT( regex_match("test-domain",      REGEX_HOSTNAME));
    CPPUNIT_ASSERT( regex_match("test_domain",      REGEX_HOSTNAME));
    CPPUNIT_ASSERT( regex_match("t35t.d0ma1n",      REGEX_HOSTNAME));
    CPPUNIT_ASSERT( regex_match("192.168.10.1",     REGEX_HOSTNAME));
    CPPUNIT_ASSERT(!regex_match("",                 REGEX_HOSTNAME));
    CPPUNIT_ASSERT(!regex_match("test/domain",      REGEX_HOSTNAME));
    CPPUNIT_ASSERT(!regex_match("\ntest.domain",    REGEX_HOSTNAME));
    CPPUNIT_ASSERT(!regex_match("test.domain\n",    REGEX_HOSTNAME));
    CPPUNIT_ASSERT(!regex_match("test\tdomain",     REGEX_HOSTNAME));
    CPPUNIT_ASSERT(!regex_match("bad@test.value",   REGEX_HOSTNAME));

    CPPUNIT_ASSERT( regex_match("757",              REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT( regex_match("localhost",        REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT(!regex_match("test.domain",      REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT( regex_match("test-domain",      REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT( regex_match("test_domain",      REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT(!regex_match("t35t.d0ma1n",      REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT(!regex_match("192.168.10.1",     REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT(!regex_match("",                 REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT( regex_match("test/domain",      REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT(!regex_match("\ntest.domain",    REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT(!regex_match("test.domain\n",    REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT(!regex_match("test\tdomain",     REGEX_DIM_SERVICE_NAME));
    CPPUNIT_ASSERT(!regex_match("bad@test.value",   REGEX_DIM_SERVICE_NAME));

    CPPUNIT_ASSERT( regex_match("757",              REGEX_WORD));
    CPPUNIT_ASSERT( regex_match("localhost",        REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("test.domain",      REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("test-domain",      REGEX_WORD));
    CPPUNIT_ASSERT( regex_match("test_domain",      REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("t35t.d0ma1n",      REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("192.168.10.1",     REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("",                 REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("test/domain",      REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("\ntest.domain",    REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("test.domain\n",    REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("test\tdomain",     REGEX_WORD));
    CPPUNIT_ASSERT(!regex_match("bad@test.value",   REGEX_WORD));

    CPPUNIT_ASSERT( regex_match(" ",                REGEX_WHITESPACE));
    CPPUNIT_ASSERT( regex_match(" ",                REGEX_WHITESPACE)); // NBSP
    CPPUNIT_ASSERT( regex_match("\t",               REGEX_WHITESPACE));
    CPPUNIT_ASSERT( regex_match("\n",               REGEX_WHITESPACE));
    CPPUNIT_ASSERT( regex_match("\r",               REGEX_WHITESPACE));
    CPPUNIT_ASSERT(!regex_match("",                 REGEX_WHITESPACE));
    CPPUNIT_ASSERT(!regex_match(".",                REGEX_WHITESPACE));

    CPPUNIT_ASSERT( regex_match("/test/path/",      REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("/test-path/",      REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("/test_path/",      REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("/test.path/",      REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("/t3st/path/",      REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("test/path/",       REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match(".test/path/",      REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("./.test/path/",    REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("./",               REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("../",              REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("../../foo/",       REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("../foo/../",       REGEX_DIRECTORY));
    CPPUNIT_ASSERT( regex_match("../foo/.././",     REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("",                 REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("..test/path/",     REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("\n/test/path/",    REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("/test/path/\n",    REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("/test/path./",     REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("/test/path",       REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("//test/path/",     REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("/test//path/",     REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("\\test\\path/",    REGEX_DIRECTORY));
    CPPUNIT_ASSERT(!regex_match("/test path/",      REGEX_DIRECTORY));
}

void ParametersTest::test_relations()
{
    CPPUNIT_ASSERT( get<2>(NICE)("-20"));
    CPPUNIT_ASSERT( get<2>(NICE)("0"));
    CPPUNIT_ASSERT( get<2>(NICE)("19"));
    CPPUNIT_ASSERT(!get<2>(NICE)(""));
    CPPUNIT_ASSERT(!get<2>(NICE)("xyz"));
    CPPUNIT_ASSERT(!get<2>(NICE)("5\n"));
    CPPUNIT_ASSERT(!get<2>(NICE)("\n5"));
    CPPUNIT_ASSERT(!get<2>(NICE)("5.0"));
    CPPUNIT_ASSERT(!get<2>(NICE)("20"));
    CPPUNIT_ASSERT(!get<2>(NICE)("-21"));
    CPPUNIT_ASSERT(!get<2>(NICE)("1 2"));

    CPPUNIT_ASSERT( get<2>(LOCK_MEMORY)("TRUE"));
    CPPUNIT_ASSERT( get<2>(LOCK_MEMORY)("FALSE"));
    CPPUNIT_ASSERT(!get<2>(LOCK_MEMORY)("0"));
    CPPUNIT_ASSERT(!get<2>(LOCK_MEMORY)("1"));
    CPPUNIT_ASSERT(!get<2>(LOCK_MEMORY)("MAYBE"));

    CPPUNIT_ASSERT( get<2>(LOGGING_MASK)("ADISWEF"));
    CPPUNIT_ASSERT( get<2>(LOGGING_MASK)("SADWIFE"));
    CPPUNIT_ASSERT( get<2>(LOGGING_MASK)("A"));
    CPPUNIT_ASSERT(!get<2>(LOGGING_MASK)(""));
    CPPUNIT_ASSERT(!get<2>(LOGGING_MASK)("AA"));
    CPPUNIT_ASSERT(!get<2>(LOGGING_MASK)("AB"));
    CPPUNIT_ASSERT(!get<2>(LOGGING_MASK)("ABC"));

    CPPUNIT_ASSERT( get<2>(DIM_DNS_NODE)("test-server.com"));    // hostname
    CPPUNIT_ASSERT( get<2>(DIM_DNS_NODE)("123.234.89.122"));     // IPv4
    CPPUNIT_ASSERT( get<2>(DIM_DNS_NODE)("123.234.89.122.223")); // "hostname"
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_NODE)(""));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_NODE)("test@server.com"));

    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)("-20"));
    CPPUNIT_ASSERT( get<2>(DIM_DNS_PORT)("0"));
    CPPUNIT_ASSERT( get<2>(DIM_DNS_PORT)("19"));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)(""));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)("xyz"));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)("5\n"));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)("\n5"));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)("5.0"));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)("65536"));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)("-21"));
    CPPUNIT_ASSERT(!get<2>(DIM_DNS_PORT)("1 2"));
}

void ParametersTest::test_default_configuration()
{
    const map<string, string> expected
    {
        {"ADAPRO_NICE",                 "0"                     },
        {"ADAPRO_LOCK_MEMORY",          "FALSE"                 },
        {"ADAPRO_DAEMON_ENABLED",       "FALSE"                 },
        {"ADAPRO_DAEMON_LOGFILE",       "/var/log/adapro.log"   },
        {"ADAPRO_LOGGING_MASK",         "DISWEF"                },
        {"ADAPRO_DIM_DNS_NODE",         "localhost"             },
        {"ADAPRO_DIM_DNS_PORT",         "2505"                  },
        {"ADAPRO_DIM_SERVER_NAME",      "ADAPRO"                },
        {"ADAPRO_DIM_SERVER_ENABLED",   "TRUE"                  },
        {"ADAPRO_MAIN_CORE",            "-1"                    },
        {"ADAPRO_SUPERVISOR_CORE",      "-1"                    },
        {"ADAPRO_DIM_CALLBACK_CORE",    "-1"                    },
        {"ADAPRO_DIM_CONTROLLER_CORE",  "-1"                    },
        {"ADAPRO_SERIALIZE_COMMANDS",   "TRUE"                  }
    };
    CPPUNIT_ASSERT(expected == DEFAULT_CONFIGURATION);
}
