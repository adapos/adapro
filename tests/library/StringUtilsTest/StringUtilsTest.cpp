/*
 * File:   StringUtilsTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 19.7.2015, 13:14:15
 */
#include <cstdio>
#include <string>
#include <vector>
#include <memory>
#include <map>
#include <regex>
#include <iostream>
#include <sstream>
#include <fstream>
#include <functional>
#include <chrono>
#include <unordered_map>
#include "StringUtilsTest.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/ADAPOS/DeliveryType.hpp"
#include "../../../headers/library/StringUtils.hpp"

using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION(StringUtilsTest);

StringUtilsTest::StringUtilsTest() {}

StringUtilsTest::~StringUtilsTest() {}

void StringUtilsTest::setUp()
{
    srand(16777213 * time(nullptr) % 2000000011);
}

void StringUtilsTest::tearDown() {}

void StringUtilsTest::test_random_string()
{
    // The probability for two exactly same strings is so low, that if this
    // assertion fails, it's fairly safe to assume a bug in the code:
    CPPUNIT_ASSERT(ADAPRO::Library::random_string(52) !=
            ADAPRO::Library::random_string(52));

//    cout << "--------------------------------------------------------------------------------" << endl;
//    cout << "Random sample with uniform distribution:" << endl;
//    for (size_t i = 0; i < 10; ++i)
//    {
//        cout << ADAPRO::Library::random_string(52) << endl;
//    }
//    cout << "--------------------------------------------------------------------------------" << endl;
}

void StringUtilsTest::test_random_string2()
{
    // The probability for two exactly same strings is so low, that if this
    // assertion fails, it's fairly safe to assume a bug in the code:
    CPPUNIT_ASSERT(ADAPRO::Library::random_string2(52) !=
            ADAPRO::Library::random_string2(52));

//    cout << "--------------------------------------------------------------------------------" << endl;
//    cout << "Random sample with 'ADAPOS' distribution:" << endl;
//    for (size_t i = 0; i < 10; ++i)
//    {
//        cout << ADAPRO::Library::random_string2(52) << endl;
//    }
//    cout << "--------------------------------------------------------------------------------" << endl;
}

void StringUtilsTest::test_hash_code()
{
    CPPUNIT_ASSERT(ADAPRO::Library::hash_code("xyz") ==
            ADAPRO::Library::hash_code("xyz"));
    CPPUNIT_ASSERT(ADAPRO::Library::hash_code("abc") ==
            ADAPRO::Library::hash_code("ABC"));
    CPPUNIT_ASSERT(ADAPRO::Library::hash_code("abc") !=
            ADAPRO::Library::hash_code("ABD"));
}

void run_statistics(const size_t string_length, function<string(size_t)> randomizer, const string& dist_name)
{
    cout << "--------------------------------------------------------------------------------" << endl;
    cout << "    Calculating hashes for " << string_length << "-char random "
            "strings using " << dist_name << " distribution." << endl;
    uint64_t min(1000000000);
    uint64_t max(0);
    uint64_t avg(0);
    uint16_t highest_collision_count(0);
    unordered_map<uint64_t, uint16_t> occurences;
    for (size_t i = 0; i < 1000000; ++i)
    {
        string random_string(randomizer(string_length));
        chrono::high_resolution_clock::time_point beginning(
                chrono::high_resolution_clock::now()
                );
        const uint64_t code(
                ADAPRO::Library::hash_code(random_string)
        );
        const uint64_t current(chrono::duration_cast<std::chrono::nanoseconds>(
                chrono::high_resolution_clock::now() - beginning
        ).count());
        if (current < min)
        {
            min = current;
        }
        else if (current > max)
        {
            max = current;
        }
        avg += current;
        const uint16_t collision_count(occurences[code] + 1);
        if (collision_count > highest_collision_count)
        {
            highest_collision_count = collision_count;
        }
        occurences[code] = collision_count;
    }
    cout << "    Average duration was " << (avg / 1000000) << " ns." << endl;
    cout << "    Lowest duration was " << min << " ns." << endl;
    cout << "    Highest duration was " << max << " ns." << endl;
    if (highest_collision_count > 1)
    {
        cout << "    Highest collision count was " << highest_collision_count << endl;
    }
}

void StringUtilsTest::test_hash_code_performance()
{
    chrono::high_resolution_clock::time_point beginning(
            chrono::high_resolution_clock::now()
            );
    ADAPRO::Library::hash_code("This string should have exactly 52 characters in it.");
    const uint64_t result(chrono::duration_cast<std::chrono::nanoseconds>(
            chrono::high_resolution_clock::now() - beginning
    ).count());
    cout << "    Calculating a constant 52-char hash took on " << result << " ns." << endl;

    run_statistics(20, ADAPRO::Library::random_string, "uniform");
    run_statistics(20, ADAPRO::Library::random_string2, "\'ADAPOS\'");
    run_statistics(52, ADAPRO::Library::random_string, "uniform");
    run_statistics(52, ADAPRO::Library::random_string2, "\'ADAPOS\'");
    cout << "--------------------------------------------------------------------------------" << endl;
}

void StringUtilsTest::test_convert_strings()
{
    char* c_strings[3];
    c_strings[0] = (char*) "foo";
    c_strings[1] = (char*) "Bar";
    c_strings[2] = (char*) "_BAZ_";
    unique_ptr<vector<string>> strings(ADAPRO::Library::convert_strings(3, c_strings));
    CPPUNIT_ASSERT(strcmp(c_strings[0], (*strings)[0].c_str()) == 0);
    CPPUNIT_ASSERT(strcmp(c_strings[1], (*strings)[1].c_str()) == 0);
    CPPUNIT_ASSERT(strcmp(c_strings[2], (*strings)[2].c_str()) == 0);
}

void StringUtilsTest::test_to_upper_case()
{
    string s("!fooBar_");
    ADAPRO::Library::to_upper_case(s);
    CPPUNIT_ASSERT(s == "!FOOBAR_");
}

void StringUtilsTest::test_to_lower_case()
{
    string s("!fooBar_");
    ADAPRO::Library::to_lower_case(s);
    CPPUNIT_ASSERT(s == "!foobar_");
}

void StringUtilsTest::test_split()
{
    const string input = "/a/b//c;d/e/";
    const unique_ptr<vector<string>> parts = ADAPRO::Library::split(input, '/');
    CPPUNIT_ASSERT(parts->size() == 6);
    CPPUNIT_ASSERT((*parts)[0].empty());
    CPPUNIT_ASSERT((*parts)[1].compare("a") == 0);
    CPPUNIT_ASSERT((*parts)[2].compare("b") == 0);
    CPPUNIT_ASSERT((*parts)[3].empty());
    CPPUNIT_ASSERT((*parts)[4].compare("c;d") == 0);
    CPPUNIT_ASSERT((*parts)[5].compare("e") == 0);
}

void StringUtilsTest::test_split_by_whitespace()
{
    const string input1 = "abc";
    const string input2 = "abc def";
    const string input3 = "abc    def";
    const string input4 = " abc def   ";
    const string input5 = " abc def   ghi";
    const unique_ptr<vector<string>> parts1 = ADAPRO::Library::split_by_whitespace(input1);
    const unique_ptr<vector<string>> parts2 = ADAPRO::Library::split_by_whitespace(input2);
    const unique_ptr<vector<string>> parts3 = ADAPRO::Library::split_by_whitespace(input3);
    const unique_ptr<vector<string>> parts4 = ADAPRO::Library::split_by_whitespace(input4);
    const unique_ptr<vector<string>> parts5 = ADAPRO::Library::split_by_whitespace(input5);
    CPPUNIT_ASSERT(parts1->size() == 1);
    CPPUNIT_ASSERT(parts2->size() == 2);
    CPPUNIT_ASSERT(parts3->size() == 2);
    CPPUNIT_ASSERT(parts4->size() == 2);
    CPPUNIT_ASSERT(parts5->size() == 3);
}

void StringUtilsTest::test_to_hex_big_endian()
{
    long* input1 = new long(0x0123456789ABCDEFL);
    char* input2 = (char*) "This is a test string.";
    CPPUNIT_ASSERT(ADAPRO::Library::to_hex_big_endian((char*) input1, 8) ==
            "01 23 45 67 89 AB CD EF");
    CPPUNIT_ASSERT(ADAPRO::Library::to_hex_big_endian((char*) input2, 21) ==
            "67 6E 69 72 74 73 20 74 73 65 74 20 61 20 73 69 20 73 69 68 54");
    delete input1;
//    delete[] input2;
}

void StringUtilsTest::test_to_hex_little_endian()
{
    long* input1 = new long(0x0123456789ABCDEFL);
    char* input2 = (char*) "This is a test string.";
    CPPUNIT_ASSERT(ADAPRO::Library::to_hex_little_endian((char*) input1, 8) ==
            "EF CD AB 89 67 45 23 01");
    CPPUNIT_ASSERT(ADAPRO::Library::to_hex_little_endian((char*) input2, 21) ==
            "54 68 69 73 20 69 73 20 61 20 74 65 73 74 20 73 74 72 69 6E 67");
    delete input1;
//    delete[] input2;
}