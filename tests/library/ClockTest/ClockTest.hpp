/*
 * File:   ClockTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 28 September 2016, 14:32:52
 */
#ifndef CLOCKTEST_HPP
#define CLOCKTEST_HPP

#include <chrono>
#include <cppunit/extensions/HelperMacros.h>

class ClockTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(ClockTest);

    CPPUNIT_TEST(test_time_since);
    CPPUNIT_TEST(test_epoch_time);
    CPPUNIT_TEST(test_now);
    CPPUNIT_TEST(test_timestamp);
    CPPUNIT_TEST(test_formatted_timestamp);
    CPPUNIT_TEST(test_fs_timestamp);

    CPPUNIT_TEST_SUITE_END();

public:
    ClockTest();
    virtual ~ClockTest();
    void setUp();
    void tearDown();

private:
    std::chrono::steady_clock::time_point beginning;
    
    void test_time_since();
    void test_epoch_time();
    void test_now();
    void test_timestamp();
    void test_formatted_timestamp();
    void test_fs_timestamp();
};

#endif /* CLOCKTEST_HPP */

