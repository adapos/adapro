/*
 * File:   ClockTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 28 September 2016, 14:32:52
 */
#include "ClockTest.hpp"
#include <string>
#include <cstdint>
#include <chrono>
#include <thread>
#include <regex>
#include <iostream>
#include "../../../headers/library/Clock.hpp"

using namespace std;
using namespace ADAPRO::Library;

CPPUNIT_TEST_SUITE_REGISTRATION(ClockTest);

ClockTest::ClockTest(): beginning(std::chrono::steady_clock::now()) {}

ClockTest::~ClockTest(){}

void ClockTest::setUp(){}

void ClockTest::tearDown(){}

void ClockTest::test_time_since()
{
    CPPUNIT_ASSERT(time_since(beginning) >= 0);
}

void ClockTest::test_epoch_time()
{
    CPPUNIT_ASSERT(epoch_time() > 1475145691411);
}

void ClockTest::test_now()
{
    uint64_t t1, t2, t3;
    t1 = now();
    this_thread::sleep_for(chrono::milliseconds(10));
    t2 = now();
    this_thread::sleep_for(chrono::milliseconds(10));
    t3 = now();
    CPPUNIT_ASSERT(t2 > 0);
    CPPUNIT_ASSERT(t3 > 0);
    CPPUNIT_ASSERT(t1 < t2);
    CPPUNIT_ASSERT(t2 < t3);
}

void ClockTest::test_timestamp()
{
    CPPUNIT_ASSERT(regex_match(timestamp(),
            regex("^[0-9]{4}(-[0-9]{2}){2} ([0-9]{2}:){2}[0-9]{2}$")));
    CPPUNIT_ASSERT(timestamp() > "2016-10-14 12:42:51");
}

void ClockTest::test_formatted_timestamp()
{
    CPPUNIT_ASSERT(regex_match(timestamp("%e %b %T"),
            regex("^([1-9]| )[0-9] (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) ([0-9]{2}:){2}[0-9]{2}$")));
}


void ClockTest::test_fs_timestamp()
{
    CPPUNIT_ASSERT(regex_match(fs_timestamp(),
            regex("^[0-9]{4}(-[0-9]{2}){2}_([0-9]{2}.){2}[0-9]{2}$")));
    CPPUNIT_ASSERT(fs_timestamp() > "2016-10-14_12.42.51");
}
