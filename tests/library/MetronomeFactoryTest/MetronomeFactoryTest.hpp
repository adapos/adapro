/*
 * File:   MetronomeFactoryTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 16 May 2017, 14:01:18
 */

#ifndef METRONOMEFACTORYTEST_HPP
#define METRONOMEFACTORYTEST_HPP

#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Logger.hpp"

class MetronomeFactoryTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(MetronomeFactoryTest);

    CPPUNIT_TEST(test_get_metronome);

    CPPUNIT_TEST_SUITE_END();

public:
    MetronomeFactoryTest();
    virtual ~MetronomeFactoryTest();
    void setUp();
    void tearDown();

private:
    static ADAPRO::Control::Logger LOGGER;
    
    void test_get_metronome();
};

#endif /* METRONOMEFACTORYTEST_HPP */

