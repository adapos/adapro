
/*
 * File:   TaskTypeTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 11 June 2017, 10:57:58
 */

#ifndef TASKTYPETEST_HPP
#define TASKTYPETEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class TaskTypeTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TaskTypeTest);

    CPPUNIT_TEST(test_show);
    CPPUNIT_TEST(test_state_mask);

    CPPUNIT_TEST_SUITE_END();

public:
    TaskTypeTest();
    virtual ~TaskTypeTest();
    void setUp();
    void tearDown();

private:
    void test_show();
    void test_state_mask();
};

#endif /* TASKTYPETEST_HPP */

