/*
 * File:   DataPointCompositeObjectTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 26 September 2016, 11:10:29
 */

#include <sstream>
#include <string>
#include <cstring>
#include <atomic>
#include <stdexcept>
#include "DataPointCompositeObjectTest.hpp"
#include "../../../headers/library/StringUtils.hpp"
#include "../../../headers/ADAPOS/DataPointCompositeObject.hpp"
#include "../../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../../headers/ADAPOS/DataPointValue.hpp"
#include "../../../headers/ADAPOS/DeliveryType.hpp"
#include "../../../headers/ADAPOS/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::ADAPOS;

CPPUNIT_TEST_SUITE_REGISTRATION(DataPointCompositeObjectTest);

DataPointCompositeObjectTest::DataPointCompositeObjectTest() {}

DataPointCompositeObjectTest::~DataPointCompositeObjectTest() {}

void DataPointCompositeObjectTest::setUp() {}

void DataPointCompositeObjectTest::tearDown() {}

void DataPointCompositeObjectTest::test_size_and_alignment()
{
    CPPUNIT_ASSERT(sizeof(DataPointCompositeObject) == 128);
    CPPUNIT_ASSERT(alignof(DataPointCompositeObject) == 128);

    char __attribute__ ((unused)) padding_1[2];
    vector<DataPointCompositeObject> dpcoms_1(3);
    char __attribute__ ((unused)) padding_2[2];
    DataPointCompositeObject dpcoms_2[3];
    char __attribute__ ((unused)) padding_3[2];
    CPPUNIT_ASSERT((((uint64_t) dpcoms_2) & 0x7F) == 0);
}

void DataPointCompositeObjectTest::test_placement_new()
{
    uint64_t segment[16]
    {
        15'844'112'909'039'564'837ULL,
        16'199'233'144'867'253'501ULL,
        11'597'141'744'943'415'307ULL,
        12'187'043'394'066'843'088ULL,
            52'653'320'894'349'302ULL,
         6'543'231'481'905'185'050ULL,
         6'688'524'503'448'693'011ULL,
           672'708'944'273'191'604ULL,
        14'676'420'615'592'328'508ULL,
         8'259'968'853'215'297'184ULL,
            65'589'377'967'120'772ULL,
        15'013'567'780'321'324'701ULL,
        13'146'075'661'143'016'940ULL,
           419'265'363'670'269'836ULL,
         6'333'598'635'819'805'455ULL,
           906'059'468'279'803'163ULL
    };

    DPCOM*  __attribute__ ((unused)) new_dpcom = new (segment) DPCOM();

    CPPUNIT_ASSERT(segment[0] == 0);
    CPPUNIT_ASSERT(segment[1] == 0);
    CPPUNIT_ASSERT(segment[2] == 0);
    CPPUNIT_ASSERT(segment[3] == 0);
    CPPUNIT_ASSERT(segment[4] == 0);
    CPPUNIT_ASSERT(segment[5] == 0);
    CPPUNIT_ASSERT(segment[6] == 0);
    CPPUNIT_ASSERT(segment[7] == 0);
}

void DataPointCompositeObjectTest::test_constructors_and_put_to_operator()
{
    stringstream ss;
    ss << DataPointCompositeObject();
    CPPUNIT_ASSERT(ss.str() == ";Void;-------- --------;1970-01-01T00:00:00.000Z;");

    const DataPointIdentifier dpid;
    uint64_t* buf = new uint64_t[7];
    buf[0] = 0x0123456789ABCDEFL;

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_INT);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_INT));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Int;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;-1985229329");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_INT);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_INT));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Int;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;-1985229329");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_UINT);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_UINT));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Uint;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;2309737967");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_UINT);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_UINT));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Uint;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;2309737967");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_FLOAT);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_FLOAT));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Float;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;-4.13604e-33");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_FLOAT);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_FLOAT));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Float;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;-4.13604e-33");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_DOUBLE);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_DOUBLE));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Double;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;3.5127e-303");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_DOUBLE);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_DOUBLE));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Double;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;3.5127e-303");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_TIME);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_TIME));
    // The value in buffer gives a nonsensical date, which DPCOM is not supposed to fix:
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Time;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;1970-08-09T22:25:43.35243Z");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_TIME);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_TIME));
    // The value in buffer gives a nonsensical date, which DPCOM is not supposed to fix:
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Time;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;1970-08-09T22:25:43.35243Z");

    buf[0] = 0x582DE31900FF0000;

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_TIME);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_TIME));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Time;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;2016-11-17T17:04:25.255Z");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_TIME);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_TIME));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Time;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;2016-11-17T17:04:25.255Z");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_BOOL);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_BOOL));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Bool;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;True");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_BOOL);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_BOOL));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Bool;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;True");

    buf[0] = 0x7069206D65726F4C;
    buf[1] = 0x6F6C6F64206D7573;
    buf[2] = 0x6D61207469732072;
    buf[3] = 0x736E6F63202C7465;
    buf[4] = 0x2072757465746365;
    buf[5] = 0x6963736970696461;
    buf[6] = 0x0074696C6520676E;

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_CHAR);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_CHAR));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Char;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;L");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_CHAR);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_CHAR));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Char;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;L");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_STRING);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_STRING));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/String;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;Lorem ipsum dolor sit amet, consectetur adipiscing elit");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_STRING);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_STRING));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/String;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;Lorem ipsum dolor sit amet, consectetur adipiscing elit");

    buf[3] = 0;

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_STRING);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_STRING));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/String;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;Lorem ipsum dolor sit am");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_STRING);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_STRING));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/String;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;Lorem ipsum dolor sit am");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::RAW_BINARY);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::RAW_BINARY));
    CPPUNIT_ASSERT(ss.str() == "test;Raw/Binary;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;"
            "4C 6F 72 65 6D 20 69 70 73 75 6D 20 64 6F 6C 6F 72 20 73 69 74 20 61 6D 00 00 00 00 00 00 00 00 "
            "65 63 74 65 74 75 72 20 61 64 69 70 69 73 63 69 6E 67 20 65 6C 69 74 00");

    ss.str(std::string());
    DataPointIdentifier::FILL(dpid, std::string("test"), DeliveryType::DPVAL_BINARY);
    ss << DataPointCompositeObject(dpid, DataPointValue(0xC71A, 462, 1474645621, buf, DeliveryType::DPVAL_BINARY));
    CPPUNIT_ASSERT(ss.str() == "test;DPVAL/Binary;-Z-ND--- OVE---PB;2016-09-23T15:47:01.462Z;"
            "4C 6F 72 65 6D 20 69 70 73 75 6D 20 64 6F 6C 6F 72 20 73 69 74 20 61 6D 00 00 00 00 00 00 00 00 "
            "65 63 74 65 74 75 72 20 61 64 69 70 69 73 63 69 6E 67 20 65 6C 69 74 00");
}

void DataPointCompositeObjectTest::test_equality_comparison()
{
    uint64_t data_a[16]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    uint64_t data_b[16]
    {
        0XF3'EB'E0'52'7D'F5'3B'4FULL,
        0XC0'0B'C4'19'26'53'33'D5ULL,
        0XE5'CB'A9'66'28'82'D9'53ULL,
        0X98'BB'46'4D'47'E1'40'17ULL,
        0X95'44'A1'4A'24'2D'1F'A7ULL,
        0XB4'0B'D8'B4'30'49'30'E5ULL,
        0X76'AA'99'81'47'3B'CC'79ULL,
        0X5A'40'CC'F1'06'B1'B8'C2ULL,
        0XFB'36'AD'EC'17'5D'27'ACULL,
        0XF2'F7'70'4E'94'6D'61'83ULL,
        0XCB'E3'DF'50'73'47'66'3DULL,
        0X91'6A'04'A6'4E'A7'B2'ECULL,
        0X0E'06'43'C5'F3'8D'82'D1ULL,
        0X7D'34'ED'7A'59'63'5C'88ULL,
        0XD3'CD'7C'FC'F9'7D'EE'E9ULL,
        0XD7'BC'7E'2A'0E'3F'62'81ULL
    };
    DPCOM test_1(DPID("abcd", RAW_STRING), DPVAL(0, 890, 1234567, data_a, RAW_STRING));
    DPCOM test_2(DPID("abcd", RAW_STRING), DPVAL(0, 890, 1234567, data_a, RAW_STRING));
    DPCOM test_3(DPID("efgh", RAW_STRING), DPVAL(0, 890, 1234567, data_b, RAW_STRING));
    DPCOM test_4(DPID("ijkl", RAW_STRING), DPVAL(0, 890, 1234567, data_b, RAW_STRING));
    DPCOM test_5(DPID("ijkl", RAW_STRING), DPVAL(0, 890, 1234567, data_b, RAW_STRING));
    CPPUNIT_ASSERT(test_1 == test_1);
    CPPUNIT_ASSERT(test_1 == test_2);
    CPPUNIT_ASSERT(test_2 == test_1);
    CPPUNIT_ASSERT(test_3 != test_4);
    CPPUNIT_ASSERT(test_4 != test_3);
    CPPUNIT_ASSERT(test_4 == test_5);
    test_4.data.payload_pt3 = 0X91'6A'04'A6'0E'A7'B2'EC; // One-bit change
    CPPUNIT_ASSERT(test_4 != test_5);
}

void DataPointCompositeObjectTest::test_binary_data_setter()
{
    const DataPointIdentifier dpid(std::string("test"), DeliveryType::VOID);
    uint64_t* buf = new uint64_t[7];
    buf[0] = 1;
    buf[1] = 1;
    buf[2] = 2;
    buf[3] = 4;
    buf[4] = 7;
    buf[5] = 13;
    buf[6] = 24;
    DataPointCompositeObject dpcom1;
    const DataPointCompositeObject dpcom2(
            dpid, DataPointValue(0x77AA, 462, 1474645621, buf, DeliveryType::RAW_BINARY)
    );
    CPPUNIT_ASSERT(strcmp(dpcom1.id.get_alias(), dpcom2.id.get_alias()) != 0);
    CPPUNIT_ASSERT(dpcom1.id.get_type()    == dpcom2.id.get_type());
    CPPUNIT_ASSERT(dpcom1.data.flags       != dpcom2.data.flags);
    CPPUNIT_ASSERT(dpcom1.data.msec        != dpcom2.data.msec);
    CPPUNIT_ASSERT(dpcom1.data.sec         != dpcom2.data.sec);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt1 != dpcom2.data.payload_pt1);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt2 != dpcom2.data.payload_pt2);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt3 != dpcom2.data.payload_pt3);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt4 != dpcom2.data.payload_pt4);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt5 != dpcom2.data.payload_pt5);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt6 != dpcom2.data.payload_pt6);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt7 != dpcom2.data.payload_pt7);

    dpcom1.set((uint64_t*) &dpcom2);
    CPPUNIT_ASSERT(strcmp(dpcom1.id.get_alias(), dpcom2.id.get_alias()) == 0);
    CPPUNIT_ASSERT(dpcom1.id.get_type()    == dpcom2.id.get_type());
    CPPUNIT_ASSERT(dpcom1.data.flags       == dpcom2.data.flags);
    CPPUNIT_ASSERT(dpcom1.data.msec        == dpcom2.data.msec);
    CPPUNIT_ASSERT(dpcom1.data.sec         == dpcom2.data.sec);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt1 != dpcom2.data.payload_pt1);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt2 != dpcom2.data.payload_pt2);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt3 != dpcom2.data.payload_pt3);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt4 != dpcom2.data.payload_pt4);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt5 != dpcom2.data.payload_pt5);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt6 != dpcom2.data.payload_pt6);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt7 != dpcom2.data.payload_pt7);

    DPID::FILL(dpcom2.id, "test", DeliveryType::RAW_BINARY);

    dpcom1.set((uint64_t*) &dpcom2);
    CPPUNIT_ASSERT(strcmp(dpcom1.id.get_alias(), dpcom2.id.get_alias()) == 0);
    CPPUNIT_ASSERT(dpcom1.id.get_type()     == dpcom2.id.get_type());
    CPPUNIT_ASSERT(dpcom1.data.flags        == dpcom2.data.flags);
    CPPUNIT_ASSERT(dpcom1.data.msec         == dpcom2.data.msec);
    CPPUNIT_ASSERT(dpcom1.data.sec          == dpcom2.data.sec);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt1  == dpcom2.data.payload_pt1);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt2  == dpcom2.data.payload_pt2);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt3  == dpcom2.data.payload_pt3);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt4  == dpcom2.data.payload_pt4);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt5  == dpcom2.data.payload_pt5);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt6  == dpcom2.data.payload_pt6);
    CPPUNIT_ASSERT(dpcom1.data.payload_pt7  == dpcom2.data.payload_pt7);
}

void DataPointCompositeObjectTest::test_dim_buffer()
{
    DPCOM dpcom_1{{"TEST_1", DeliveryType::DPVAL_BINARY}, {}};
    DPCOM dpcom_2{{"TEST_2", DeliveryType::RAW_INT},      {}};
    DPCOM dpcom_3{{"TEST_3", DeliveryType::VOID},         {}};
    CPPUNIT_ASSERT(dpcom_1.dim_buffer() == (void*) &dpcom_1.data);
    CPPUNIT_ASSERT(dpcom_2.dim_buffer() == (void*) &dpcom_2.data.payload_pt1);
    bool exception(false);
    try
    {
        dpcom_3.dim_buffer();
    }
    catch (const std::domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}
