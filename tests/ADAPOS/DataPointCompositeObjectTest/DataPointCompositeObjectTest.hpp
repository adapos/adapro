/*
 * File:   DataPointCompositeObjectTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch) (john.larry.lang@cenr.ch)
 *
 * Created on 26.9.2016, 11:10:29
 */

#ifndef DATAPOINTCOMPOSITEOBJECTTEST_HPP
#define DATAPOINTCOMPOSITEOBJECTTEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class DataPointCompositeObjectTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(DataPointCompositeObjectTest);

    CPPUNIT_TEST(test_size_and_alignment);
    CPPUNIT_TEST(test_placement_new);
    CPPUNIT_TEST(test_constructors_and_put_to_operator);
    CPPUNIT_TEST(test_equality_comparison);
    CPPUNIT_TEST(test_binary_data_setter);
    CPPUNIT_TEST(test_dim_buffer);

    CPPUNIT_TEST_SUITE_END();

public:
    DataPointCompositeObjectTest();
    virtual ~DataPointCompositeObjectTest();
    void setUp();
    void tearDown();

private:
    void test_size_and_alignment();
    void test_placement_new();
    void test_constructors_and_put_to_operator();
    void test_equality_comparison();
    void test_binary_data_setter();
    void test_dim_buffer();
};

#endif /* DATAPOINTCOMPOSITEOBJECTTEST_HPP */

