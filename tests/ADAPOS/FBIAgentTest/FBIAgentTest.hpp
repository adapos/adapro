/*
 * File:   FBIAgentTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 February 2017, 17:43:34
 */

#ifndef FBIAGENTTEST_HPP
#define FBIAGENTTEST_HPP

#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/control/Supervisor.hpp"

class FBIAgentTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(FBIAgentTest);

    CPPUNIT_TEST(test_accessors);
    CPPUNIT_TEST(test_active_mode);
    CPPUNIT_TEST(test_passive_mode);

    CPPUNIT_TEST_SUITE_END();

public:
    FBIAgentTest();
    virtual ~FBIAgentTest();
    void setUp();
    void tearDown();

private:
    static ADAPRO::Control::Logger LOGGER;
    static ADAPRO::Control::Supervisor SUPERVISOR;
    
    void test_accessors();
    void test_active_mode();
    void test_passive_mode();
};

#endif /* FBIAGENTTEST_HPP */

