/*
 * File:   FBIAgentTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 February 2017, 17:43:34
 */

#include <cstdint>
#include <cstring>
#include <string>
#include <atomic>
#include <map>
#include <deque>
#include <vector>
#include <functional>
#include <memory>
#include <thread>
#include <chrono>
#include <iostream>
#include "../../../headers/control/Session.hpp"
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/ADAPOS/FBIAgent.hpp"
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/control/Metronome.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/library/MetronomeFactory.hpp"
#include "../../../headers/library/Clock.hpp"
#include "../../../headers/library/StringUtils.hpp"
#include "../../../headers/ADAPOS/DataPointCompositeObject.hpp"
#include "../../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../../headers/ADAPOS/DataPointValue.hpp"
#include "../../../headers/ADAPOS/DeliveryType.hpp"
#include "../../../headers/ADAPOS/Typedefs.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/LoggingLevel.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "FBIAgentTest.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

CPPUNIT_TEST_SUITE_REGISTRATION(FBIAgentTest);

ADAPRO::Control::Logger FBIAgentTest::LOGGER;
ADAPRO::Control::Supervisor FBIAgentTest::SUPERVISOR(LOGGER);

FBIAgentTest::FBIAgentTest() {}

FBIAgentTest::~FBIAgentTest() {}

void FBIAgentTest::setUp() {}

void FBIAgentTest::tearDown() {}

void FBIAgentTest::test_accessors()
{
    class DummyAgent final : public FBIAgent
    {
    protected:
        virtual void prepare() override {}
        virtual void finish() override {}
        virtual void send_fbi() override {}
        virtual void consume_fbi(const image_copy_t& fbi_copy) override {}
    public:
        DummyAgent(string&& name) noexcept:
                FBIAgent
                (
                        LOGGER,
                        0,
                        0,
                        SUPERVISOR,
                        std::move(name)
                )
        {}
        virtual ~DummyAgent() noexcept {};
    };

    DummyAgent x(std::move("test1"));
    uint64_t tmp[7]{65, 68, 65, 80, 82, 79, 33};
    image_copy_t expected_dpcoms
    {
        DPCOM(DPID("DPCOM-1", RAW_TIME), DPVAL(0, 0, 0, tmp, RAW_TIME)),
        DPCOM(DPID("DPCOM-2", RAW_DOUBLE), DPVAL(0, 0, 0, tmp, RAW_DOUBLE)),
        DPCOM(DPID("DPCOM-3", RAW_INT), DPVAL(0, 0, 0, tmp, RAW_INT)),
        DPCOM(DPID("DPCOM-4", RAW_FLOAT), DPVAL(0, 0, 0, tmp, RAW_FLOAT))
    };

    const bool condition1(x.get_fbi_size() == 0);
    x.append_to_fbi(expected_dpcoms[0]);
    const bool condition2(x.get_fbi_size() == 1);
    x.append_to_fbi(expected_dpcoms[0]);
    // FBIAgent doesn't check for duplicates:
    const bool condition3(x.get_fbi_size() == 2);
    x.append_to_fbi(expected_dpcoms[1]);
    const bool condition4(x.get_fbi_size() == 3);
    x.append_to_fbi(expected_dpcoms[2]);
    const bool condition5(x.get_fbi_size() == 4);
    x.update_fbi(2, DPVAL(DPVAL::DIRTY_FLAG | DPVAL::DIM_ERROR_FLAG, 0, 0, tmp, RAW_DOUBLE));
    const bool condition6(x.get_fbi_size() == 4);
    image_copy_t actual_dpcoms;
    x.copy_fbi(actual_dpcoms);
    const bool condition7(actual_dpcoms.size() == 4);
    const bool condition8(expected_dpcoms[0] == actual_dpcoms[0]);
    const bool condition9(expected_dpcoms[0] == actual_dpcoms[1]);
    const bool condition10(expected_dpcoms[1] != actual_dpcoms[2]);
    const bool condition11(expected_dpcoms[2] == actual_dpcoms[3]);
    const bool condition12(actual_dpcoms[2].data.flags == DPVAL::DIM_ERROR_FLAG);
    const bool condition13(expected_dpcoms[0] == x.peek_fbi(0));
    const bool condition14(expected_dpcoms[0] == x.peek_fbi(1));
    const bool condition15(expected_dpcoms[1] != x.peek_fbi(2));
    const bool condition16(expected_dpcoms[2] == x.peek_fbi(3));

    CPPUNIT_ASSERT(condition1);
    CPPUNIT_ASSERT(condition2);
    CPPUNIT_ASSERT(condition3);
    CPPUNIT_ASSERT(condition4);
    CPPUNIT_ASSERT(condition5);
    CPPUNIT_ASSERT(condition6);
    CPPUNIT_ASSERT(condition7);
    CPPUNIT_ASSERT(condition8);
    CPPUNIT_ASSERT(condition9);
    CPPUNIT_ASSERT(condition10);
    CPPUNIT_ASSERT(condition11);
    CPPUNIT_ASSERT(condition12);
    CPPUNIT_ASSERT(condition13);
    CPPUNIT_ASSERT(condition14);
    CPPUNIT_ASSERT(condition15);
    CPPUNIT_ASSERT(condition16);
}

void FBIAgentTest::test_active_mode()
{
    const size_t services(10);
    const size_t cycles(5);

    class ActiveAgent final : public FBIAgent
    {
        bool* conditions;
        uint64_t* cache;
        size_t consume_count;

    protected:

        virtual void prepare() override {}
        virtual void send_fbi() override {}
        virtual void finish() override
        {
            for (size_t i = 0; i < consume_count * services; ++i)
            {
                if (conditions[i] == false)
                {
                    pass.store(false, memory_order_release);
                    cout << "Condition " << i << " failed." << endl;
                    break;
                }
            }
        }

        virtual void consume_fbi(const image_copy_t& fbi_copy) override
        {
            this_thread::sleep_for(chrono::milliseconds(10));
            for (size_t i = 0; i < services; ++i)
            {
                uint64_t new_value(fbi_copy[i].data.payload_pt1);
//                cout << "cached: " << cache[i] << ", new: " << new_value << endl;
                conditions[consume_count + i] = cache[i] < new_value;
                cache[i] = new_value;
            }
//            cout << endl;
            ++consume_count;
        }

    public:
        atomic_bool pass;

        ActiveAgent(string&& name, const uint32_t tick_length):
                FBIAgent
                (
                        LOGGER,
                        tick_length,
                        0,
                        SUPERVISOR,
                        std::move(name),
                        DEFAULT_CONFIGURATION,
                        1
                ),
                conditions(new bool[cycles * services]),
                cache(new uint64_t[services]),
                consume_count(0),
                pass(true)
        {
            memset((void*) cache, 0, 80);
            memset((void*) conditions, 0, cycles * services * sizeof(bool));
        }

        virtual ~ActiveAgent() noexcept
        {
            delete[] conditions;
            delete[] cache;
        }
    };

    DPCOM dpcoms[services]
    {
        {DPID("TST_0", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_1", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_2", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_3", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_4", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_5", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_6", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_7", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_8", DeliveryType::RAW_UINT), DPVAL()},
        {DPID("TST_9", DeliveryType::RAW_UINT), DPVAL()}
    };
    uint64_t tmp[7] {0, 0, 0, 0, 0, 0, 0};
    ActiveAgent y(std::move("test2"), 100);
    Metronome& metronome(get_metronome(LOGGER, 100));
    for (size_t i = 0; i < services; ++i)
    {
        y.append_to_fbi(dpcoms[i]);
    }
    metronome.synchronize();
    y.start(true);

    for (size_t i = 0; i < cycles; ++i)
    {
        ++tmp[0];
        for (size_t j = 0; j < services; ++j)
        {
            dpcoms[j].update(DPVAL(0, 0, 0, tmp, DeliveryType::RAW_UINT));
            y.update_fbi(j, dpcoms[j].data);
        }
        metronome.synchronize();
    }

    y.stop(true);
//    CPPUNIT_ASSERT(y.pass.load(memory_order_consume));
}

void FBIAgentTest::test_passive_mode()
{
    class PassiveAgent final : public FBIAgent
    {
        image_copy_t& second_copy;
    protected:

        virtual void prepare() override {}
        virtual void finish() override {}
        virtual void send_fbi() override {}
        virtual void consume_fbi(const image_copy_t& fbi_copy) override
        {
//            Session::get_logger().print("Consuming FBI...", _DEBUG, this);
            second_copy.clear();
            for (const DPCOM& dpcom : fbi_copy)
            {
                second_copy.push_back(dpcom);
            }
        }

    public:
        PassiveAgent(string&& name, image_copy_t& second_copy) noexcept:
                FBIAgent
                (
                        LOGGER,
                        0,
                        0,
                        SUPERVISOR,
                        std::move(name)
                ),
                second_copy(second_copy) {}
    };

    uint64_t tmp[7]{0, 0, 0, 0, 0, 0, 0};
    image_copy_t expected_dpcoms
    {
        DPCOM(DPID("DPCOM-1", RAW_TIME), DPVAL(0, 0, 0, tmp, RAW_TIME)),
        DPCOM(DPID("DPCOM-2", RAW_DOUBLE), DPVAL(0, 0, 0, tmp, RAW_DOUBLE)),
        DPCOM(DPID("DPCOM-3", RAW_INT), DPVAL(0, 0, 0, tmp, RAW_INT)),
        DPCOM(DPID("DPCOM-4", RAW_FLOAT), DPVAL(0, 0, 0, tmp, RAW_FLOAT))
    };
    image_copy_t actual_dpcoms;
    PassiveAgent z(std::move("test3"), actual_dpcoms);

    z.start(true);

    z.append_to_fbi(expected_dpcoms[0]);
    z.append_to_fbi(expected_dpcoms[1]);
    z.append_to_fbi(expected_dpcoms[2]);
    z.append_to_fbi(expected_dpcoms[3]);
    z.notify();
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition_1 = expected_dpcoms == actual_dpcoms;

    const uint64_t now(epoch_time());
    tmp[0] = 0x58BD659C007B0000;
    expected_dpcoms[0].data = DPVAL(0, now % 1000, now / 1000, tmp, RAW_TIME);
    expected_dpcoms[1].data = DPVAL(0, now % 1000, now / 1000, tmp, RAW_DOUBLE);
    expected_dpcoms[2].data = DPVAL(0, now % 1000, now / 1000, tmp, RAW_INT);
    expected_dpcoms[3].data = DPVAL(0, now % 1000, now / 1000, tmp, RAW_FLOAT);
    z.update_fbi(0, expected_dpcoms[0].data);
    z.update_fbi(1, expected_dpcoms[1].data);
    z.update_fbi(2, expected_dpcoms[2].data);
    z.update_fbi(3, expected_dpcoms[3].data);
    z.notify();
    this_thread::sleep_for(chrono::milliseconds(100));
    const bool condition_2 = expected_dpcoms == actual_dpcoms;

    z.stop(false);
    z.notify();
    z.wait_for_state(STOPPED);

    CPPUNIT_ASSERT(condition_1);
    CPPUNIT_ASSERT(condition_2);
}
