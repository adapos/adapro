/*
 * File:   DataPointIdentifierTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 September 2016, 13:31:25
 */
#include <cstring>
#include <cstddef>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>
#include "../../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../../headers/ADAPOS/DeliveryType.hpp"
#include "DataPointIdentifierTest.hpp"

using namespace std;
using namespace ADAPRO::ADAPOS;

CPPUNIT_TEST_SUITE_REGISTRATION(DataPointIdentifierTest);

DataPointIdentifierTest::DataPointIdentifierTest() {}

DataPointIdentifierTest::~DataPointIdentifierTest() {}

void DataPointIdentifierTest::setUp() {}

void DataPointIdentifierTest::tearDown() {}

void DataPointIdentifierTest::test_size_and_alignment()
{
    CPPUNIT_ASSERT(sizeof(DataPointIdentifier) == 64);
    CPPUNIT_ASSERT(alignof(DataPointIdentifier) == 64);

    char __attribute__ ((unused)) padding_1[2];
    vector<DataPointIdentifier> dpids_1(3);
    char __attribute__ ((unused)) padding_2[2];
    DataPointIdentifier dpids_2[3];
    char __attribute__ ((unused)) padding_3[2];
    CPPUNIT_ASSERT((((uint64_t) dpids_2) & 0x3F) == 0);
}

void DataPointIdentifierTest::test_ctors_and_put_to_operator()
{
    DataPointIdentifier dpid1;
    std::stringstream ss;

    ss << dpid1;
    CPPUNIT_ASSERT(ss.str() == ";Void");
    ss.str(std::string());

    DataPointIdentifier::FILL(dpid1, std::string("test1"), DeliveryType::RAW_INT);
    ss << dpid1;
    CPPUNIT_ASSERT(ss.str() == "test1;Raw/Int");
    ss.str(std::string());

    DataPointIdentifier dpid2("test2", DeliveryType::RAW_BOOL);
    ss << dpid2;
    CPPUNIT_ASSERT(ss.str() == "test2;Raw/Bool");
}

void DataPointIdentifierTest::test_get_alias()
{
    const DataPointIdentifier dpid;
    DataPointIdentifier::FILL(dpid, std::string("test3"), DeliveryType::RAW_INT);
    CPPUNIT_ASSERT(strcmp("test3", dpid.get_alias()) == 0);
    DataPointIdentifier::FILL(dpid, std::string(
            "This string is way too long to fully fit in the alias field properly"),
            DeliveryType::RAW_INT
    );
    CPPUNIT_ASSERT(strcmp("This string is way too long to fully fit in the alias field pr", dpid.get_alias()) == 0);
}

void DataPointIdentifierTest::test_get_type()
{
    const DataPointIdentifier dpid;
    DataPointIdentifier::FILL(dpid, std::string("test4"), DeliveryType::RAW_INT);
    CPPUNIT_ASSERT(dpid.get_type() == DeliveryType::RAW_INT);
}

void DataPointIdentifierTest::test_hash_code()
{
    const DataPointIdentifier dpid1;
    DataPointIdentifier::FILL(dpid1, std::string("test5"), DeliveryType::RAW_INT);
    const DataPointIdentifier dpid2;
    DataPointIdentifier::FILL(dpid2, std::string("test6"), DeliveryType::RAW_INT);
    const DataPointIdentifier dpid3;
    DataPointIdentifier::FILL(dpid3, (uint64_t*) &dpid2);
//    cout << dpid1.hash_code() << endl;
//    cout << dpid2.hash_code() << endl;
//    cout << dpid3.hash_code() << endl;
    CPPUNIT_ASSERT(dpid1.hash_code() == dpid1.hash_code());
    CPPUNIT_ASSERT(dpid1.hash_code() != dpid2.hash_code());
    CPPUNIT_ASSERT(dpid2.hash_code() == dpid3.hash_code());
}


void DataPointIdentifierTest::test_equality_comparison()
{
    const DataPointIdentifier dpid1, dpid2, dpid3, dpid4;
    DataPointIdentifier::FILL(dpid1, std::string("test"), DeliveryType::RAW_INT);
    DataPointIdentifier::FILL(dpid2, std::string("test"), DeliveryType::RAW_INT);
    DataPointIdentifier::FILL(dpid3, std::string("test"), DeliveryType::DPVAL_INT);
    DataPointIdentifier::FILL(dpid4, std::string("test"), DeliveryType::DPVAL_UINT);
    CPPUNIT_ASSERT(dpid1 == dpid2);
    CPPUNIT_ASSERT(dpid1 == dpid3);
    CPPUNIT_ASSERT(dpid1 != dpid4);
}


void DataPointIdentifierTest::test_binary_data_fill()
{
    const DataPointIdentifier dpid1;
    const DataPointIdentifier dpid2 = DataPointIdentifier(
            std::string("This is a test alias."), DeliveryType::RAW_CHAR
    );
    DataPointIdentifier::FILL(dpid1, (uint64_t*) &dpid2);
    CPPUNIT_ASSERT(strcmp(dpid1.get_alias(), dpid2.get_alias()) == 0);
    CPPUNIT_ASSERT(dpid1.get_type() == dpid2.get_type());
}
