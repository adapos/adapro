/*
 * File:   DataPointIdentifierTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 September 2016, 13:31:25
 */
#ifndef DATAPOINTIDENTIFIERTEST_HPP
#define DATAPOINTIDENTIFIERTEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class DataPointIdentifierTest : public CPPUNIT_NS::TestFixture 
{
    CPPUNIT_TEST_SUITE(DataPointIdentifierTest);

    CPPUNIT_TEST(test_size_and_alignment);
    CPPUNIT_TEST(test_ctors_and_put_to_operator);
    CPPUNIT_TEST(test_get_alias);
    CPPUNIT_TEST(test_get_type);
    CPPUNIT_TEST(test_hash_code);
    CPPUNIT_TEST(test_equality_comparison);
    CPPUNIT_TEST(test_binary_data_fill);

    CPPUNIT_TEST_SUITE_END();

public:
    DataPointIdentifierTest();
    virtual ~DataPointIdentifierTest();
    void setUp();
    void tearDown();

private:
    void test_size_and_alignment();
    void test_ctors_and_put_to_operator();
    void test_get_alias();
    void test_get_type();
    void test_hash_code();
    void test_equality_comparison();
    void test_binary_data_fill();
};

#endif /* DATAPOINTIDENTIFIERTEST_HPP */

