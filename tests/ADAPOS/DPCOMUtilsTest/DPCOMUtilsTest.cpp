/*
 * File:   DPCOMUtilsTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 24 March 2017, 19:16:21
 */

#include <cstring>
#include <string>
#include <iostream>
#include <stdexcept>
#include "../../../headers/ADAPOS/DPCOMUtils.hpp"
#include "../../../headers/library/StringUtils.hpp"
#include "../../../headers/ADAPOS/DataPointCompositeObject.hpp"
#include "../../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../../headers/ADAPOS/DataPointValue.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "DPCOMUtilsTest.hpp"

using namespace std;
using namespace ADAPRO::ADAPOS;

CPPUNIT_TEST_SUITE_REGISTRATION(DPCOMUtilsTest);

DPCOMUtilsTest::DPCOMUtilsTest() {}

DPCOMUtilsTest::~DPCOMUtilsTest() {}

void DPCOMUtilsTest::setUp() {}

void DPCOMUtilsTest::tearDown() {}

void DPCOMUtilsTest::test_increment()
{
    uint64_t data1[7]{42, 0, 0, 0, 0, 0, 0};
    uint64_t data2[7]{43, 0, 0, 0, 0, 0, 0};
    DPCOM dpcom1
    (
            DPID("TEST_1", DeliveryType::RAW_INT),
            DPVAL(DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG, 0, 0, data1, DeliveryType::RAW_INT)
    );
    increment(dpcom1);
    DPCOM dpcom2
    (
            DPID("TEST_2", DeliveryType::RAW_INT),
            DPVAL(DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG, 0, 0, data2, DeliveryType::RAW_INT)
    );
    dpcom2.data.update_timestamp();

    CPPUNIT_ASSERT(dpcom1.id == DPID("TEST_1", DeliveryType::RAW_INT));
    CPPUNIT_ASSERT(dpcom1.data.flags == (DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG));
    CPPUNIT_ASSERT(memcmp((void*) &dpcom1.data.payload_pt1, (void*) data2, 56)
            == 0);
    CPPUNIT_ASSERT(memcmp(dpcom1.id.get_alias(), "TEST_1", 7) == 0);
    CPPUNIT_ASSERT(dpcom1.id != dpcom2.id);
    CPPUNIT_ASSERT(dpcom1.data.flags == dpcom2.data.flags);
    CPPUNIT_ASSERT(dpcom1.data.msec + 1000 * dpcom1.data.sec <=
            dpcom2.data.msec + 1000 * dpcom2.data.sec);
    CPPUNIT_ASSERT(memcmp((void*) &dpcom1.data.payload_pt1,
            (void*) &dpcom2.data.payload_pt1, 56) == 0);

    DPCOM dpcom3(DPID("TEST_3", DeliveryType::RAW_UINT), dpcom1.data);
    increment(dpcom3);
    CPPUNIT_ASSERT(dpcom3.data.payload_pt1 == 44);

    DPCOM dpcom4(DPID("TEST_4", DeliveryType::RAW_CHAR), dpcom3.data);
    increment(dpcom4);
    CPPUNIT_ASSERT(dpcom4.data.payload_pt1 == (uint64_t) '5');

    DPCOM dpcom5(DPID("TEST_5", DeliveryType::RAW_DOUBLE), dpcom4.data);
    increment(dpcom5);
    CPPUNIT_ASSERT(dpcom5.data.payload_pt1 == 4'607'182'418'800'017'408);
    CPPUNIT_ASSERT(*((double*) &dpcom5.data.payload_pt1) == 1.0);

    DPCOM dpcom6(DPID("TEST_6", DeliveryType::RAW_STRING), dpcom5.data);
    increment(dpcom6);
    CPPUNIT_ASSERT(dpcom6.data.payload_pt1 == 49);

    DPCOM dpcom7(DPID("TEST_7", DeliveryType::RAW_STRING), dpcom6.data);
    increment(dpcom7);
    CPPUNIT_ASSERT(dpcom7.data.payload_pt1 == 50);

    DPCOM dpcom19(DPID("TEST_19", DeliveryType::RAW_FLOAT), dpcom7.data);
    increment(dpcom19);
    CPPUNIT_ASSERT(dpcom19.data.payload_pt1 == 1'065'353'216);
    CPPUNIT_ASSERT(*((float*) &dpcom19.data.payload_pt1) == 1.0);

    DPCOM dpcom8(DPID("TEST_8", DeliveryType::RAW_BOOL), dpcom19.data);
    increment(dpcom8);
    CPPUNIT_ASSERT(dpcom8.data.payload_pt1 == 0);

    DPCOM dpcom9(DPID("TEST_9", DeliveryType::RAW_TIME), dpcom8.data);
    increment(dpcom9);
    CPPUNIT_ASSERT(dpcom9.data.payload_pt1 == 4294967296);

    DPCOM dpcom10(DPID("TEST_10", DeliveryType::RAW_BINARY), dpcom9.data);
    CPPUNIT_ASSERT(dpcom10.data.payload_pt1 == 0x00'00'00'01'00'00'00'00ULL);

    DPCOM dpcom11(DPID("TEST_11", DeliveryType::DPVAL_BINARY), dpcom10.data);
    increment(dpcom11);
    CPPUNIT_ASSERT(dpcom11.data.payload_pt1 == 0x00'00'00'01'00'00'00'01ULL);

    DPCOM dpcom12(DPID("TEST_12", DeliveryType::DPVAL_INT), dpcom11.data);
    increment(dpcom12);
    CPPUNIT_ASSERT(dpcom12.data.payload_pt1 == 2);

    DPCOM dpcom13(DPID("TEST_13", DeliveryType::DPVAL_UINT), dpcom12.data);
    increment(dpcom13);
    CPPUNIT_ASSERT(dpcom13.data.payload_pt1 == 3);

    DPCOM dpcom14(DPID("TEST_14", DeliveryType::DPVAL_DOUBLE), dpcom13.data);
    increment(dpcom14);
    CPPUNIT_ASSERT(dpcom14.data.payload_pt1 == 0x3F'F0'00'00'00'00'00'00ULL);

    DPCOM dpcom20(DPID("TEST_20", DeliveryType::DPVAL_FLOAT), dpcom14.data);
    increment(dpcom20);
    CPPUNIT_ASSERT(dpcom20.data.payload_pt1 == 0x3F'80'00'00U);

    DPCOM dpcom15(DPID("TEST_15", DeliveryType::DPVAL_BOOL), dpcom20.data);
    increment(dpcom15);
    CPPUNIT_ASSERT(!dpcom15.data.payload_pt1);

    DPCOM dpcom16(DPID("TEST_16", DeliveryType::DPVAL_CHAR), dpcom15.data);
    increment(dpcom16);
    CPPUNIT_ASSERT(*((char*) &dpcom16.data.payload_pt1) == '1');

    DPCOM dpcom17(DPID("TEST_17", DeliveryType::DPVAL_STRING), dpcom16.data);
    increment(dpcom17);
    CPPUNIT_ASSERT(*((char*) &dpcom17.data.payload_pt1) == '2');

    DPCOM dpcom18(DPID("TEST_18", DeliveryType::DPVAL_TIME), dpcom17.data);
    increment(dpcom18);
    CPPUNIT_ASSERT(dpcom18.data.payload_pt1 == 4'294'967'346ULL);
}

void DPCOMUtilsTest::test_increment_without_ts()
{
    uint64_t data1[7]{42, 0, 0, 0, 0, 0, 0};
    uint64_t data2[7]{43, 0, 0, 0, 0, 0, 0};
    DPCOM dpcom1
    (
            DPID("TEST_1", DeliveryType::RAW_INT),
            DPVAL(DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG, 0, 0, data1, DeliveryType::RAW_INT)
    );
    increment(dpcom1, false);
    DPCOM dpcom2
    (
            DPID("TEST_1", DeliveryType::RAW_INT),
            DPVAL(DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG, 0, 0, data2, DeliveryType::RAW_INT)
    );

    CPPUNIT_ASSERT(dpcom1.id == DPID("TEST_1", DeliveryType::RAW_INT));
    CPPUNIT_ASSERT(dpcom1.data.flags == (DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG));
    CPPUNIT_ASSERT(memcmp((void*) &dpcom1.data.payload_pt1, (void*) data2, 56)
            == 0);
    CPPUNIT_ASSERT(dpcom1.id == dpcom2.id);
    CPPUNIT_ASSERT(dpcom1.data.flags == dpcom2.data.flags);
    CPPUNIT_ASSERT(dpcom1.data.msec + 1000 * dpcom1.data.sec ==
            dpcom2.data.msec + 1000 * dpcom2.data.sec);
    CPPUNIT_ASSERT(dpcom1.data.msec + 1000 * dpcom1.data.sec == 0);
    CPPUNIT_ASSERT(memcmp((void*) &dpcom1.data.payload_pt1,
            (void*) &dpcom2.data.payload_pt1, 56) == 0);
}

void DPCOMUtilsTest::test_verify()
{
    uint64_t data1[7]{42, 0, 0, 0, 0, 0, 0};
    uint64_t data2[7]{42, 7, 0, 0, 0, 0, 0};
    DPCOM dpcom1
    (
            DPID("TEST_1", DeliveryType::RAW_INT),
            DPVAL(DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG, 0, 0, data1, DeliveryType::RAW_INT)
    );
    dpcom1.data.update_timestamp();
    DPCOM dpcom2
    (
            DPID("TEST_1", DeliveryType::RAW_INT),
            DPVAL(DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG, 0, 0, data2, DeliveryType::RAW_INT)
    );
    dpcom2.data.update_timestamp();

    CPPUNIT_ASSERT(verify(dpcom1, dpcom2, 4) == 0);
    CPPUNIT_ASSERT(dpcom1.id == DPID("TEST_1", DeliveryType::RAW_INT));
    CPPUNIT_ASSERT(dpcom1.data.flags == (DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG));
    CPPUNIT_ASSERT(dpcom1.data.msec < 1000);
    CPPUNIT_ASSERT(dpcom1.data.sec > 0);
    CPPUNIT_ASSERT(memcmp((void*) &dpcom1.data.payload_pt1, (void*) data1, 56)
            == 0);
    CPPUNIT_ASSERT(memcmp((void*) &dpcom1.data.payload_pt1, (void*) data2, 56)
            < 0);
    CPPUNIT_ASSERT(dpcom2.id == DPID("TEST_1", DeliveryType::RAW_INT));
    CPPUNIT_ASSERT(dpcom2.data.flags == (DPVAL::NEW_FLAG | DPVAL::DIRTY_FLAG));
    CPPUNIT_ASSERT(dpcom2.data.msec < 1000);
    CPPUNIT_ASSERT(dpcom2.data.sec > 0);
    CPPUNIT_ASSERT(memcmp((void*) &dpcom2.data.payload_pt1, (void*) data1, 56)
            == 0);
    CPPUNIT_ASSERT(memcmp((void*) &dpcom2.data.payload_pt1, (void*) data2, 56)
            < 0);
}

