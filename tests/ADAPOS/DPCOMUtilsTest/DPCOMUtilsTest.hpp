/*
 * File:   DPCOMUtilsTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 24.3.2017, 19:16:21
 */

#ifndef DPCOMUTILSTEST_HPP
#define DPCOMUTILSTEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class DPCOMUtilsTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(DPCOMUtilsTest);

    CPPUNIT_TEST(test_increment);
    CPPUNIT_TEST(test_increment_without_ts);
    CPPUNIT_TEST(test_verify);

    CPPUNIT_TEST_SUITE_END();

public:
    DPCOMUtilsTest();
    virtual ~DPCOMUtilsTest();
    void setUp();
    void tearDown();

private:
    void test_increment();
    void test_increment_without_ts();
    void test_verify();
};

#endif /* DPCOMUTILSTEST_HPP */

