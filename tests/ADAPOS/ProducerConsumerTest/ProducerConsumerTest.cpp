/*
 * File:   ProducerConsumerTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 15 February 2017, 17:47:48
 */

#include <cstdint>
#include <iostream>
#include <thread>
#include <chrono>
#include <memory>
#include <vector>
#include <deque>
#include <atomic>
#include <map>
#include <string>
#include <functional>
#include "../../../headers/control/Thread.hpp"
#include "../../../headers/control/Worker.hpp"
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/control/AlignedAllocator.hpp"
#include "../../../headers/ADAPOS/PipelineProcessor.hpp"
#include "../../../headers/ADAPOS/Producer.hpp"
#include "../../../headers/ADAPOS/Consumer.hpp"
#include "../../../headers/ADAPOS/DPIDUtils.hpp"
#include "../../../headers/ADAPOS/DataPointCompositeObject.hpp"
#include "../../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../../headers/ADAPOS/DataPointValue.hpp"
#include "../../../headers/ADAPOS/DataPointEventRecord.hpp"
#include "../../../headers/ADAPOS/DeliveryType.hpp"
#include "../../../headers/ADAPOS/Typedefs.hpp"
#include "../../../headers/library/Clock.hpp"
#include "../../../headers/data/Parameters.hpp"
#include "../../../headers/data/Typedefs.hpp"
#include "ProducerConsumerTest.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(ProducerConsumerTest);

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;
using namespace ADAPRO::Library;

ADAPRO::Control::Logger ProducerConsumerTest::LOGGER;
Supervisor ProducerConsumerTest::SUPERVISOR(ProducerConsumerTest::LOGGER);
registry_t ProducerConsumerTest::DPIDS;

class TestProducer final : public Producer
{
    uint64_t* data;
    registry_t input_dpids;
public:
    uint64_t update_count;
    TestProducer
    (
            registry_t& input_dpids,
            registry_t& output_dpids,
//            buffer_t& ring_buffer
            buffer_t ring_buffer,
            const size_t buffer_size
    )
    noexcept:
            Producer
            (
                    ProducerConsumerTest::LOGGER,
                    ring_buffer,
                    buffer_size,
                    output_dpids,
                    ProducerConsumerTest::SUPERVISOR,
                    std::move("TestProducer"),
                    DEFAULT_CONFIGURATION,
                    2
            ),
            data(new uint64_t[7]{0, 0, 0, 0, 0, 0, 0}),
            input_dpids(input_dpids),
            update_count(0)
    {}

    virtual ~TestProducer() noexcept
    {
        delete[] data;
    }

protected:
    virtual void prepare() override
    {
        for (const DPID& dpid : input_dpids)
        {
            produce(dpid, DPVAL());
        }
        pause(false);
    }

    virtual void execute() override
    {
        for (size_t i = 0; i < get_dpid_count(); ++i)
        {
            const uint64_t t(epoch_time());
            update(i, DPVAL(0, t % 1000, t / 1000, &(++(data[0])), get_dpid(i).get_type()));
            ++update_count;
        }
    }
};

class TestConsumer final : public Consumer
{
    image_t fbi;
    const chrono::nanoseconds delay;

protected:

    virtual void prepare() override {}

    virtual void consume(const size_t index, const DPVAL& dpval) override
    {
        const uint16_t current_flags(dpval.flags);
        if (current_flags & DPVAL::NEW_FLAG)
        {
            fbi.push_back(DPCOM(get_dpid(index), dpval));
        }
        else if (current_flags & DPVAL::END_FLAG)
        {
            stop(false);
        }
        else
        {
            fbi[index].data = dpval;
            ++consume_count;
        }
    }

public:
    uint64_t consume_count;
    TestConsumer
    (
            registry_t& dpids,
//            buffer_t& ring_buffer,
            buffer_t ring_buffer,
            const size_t buffer_size,
            const chrono::nanoseconds delay
    )
    noexcept:
            Consumer
            (
                    ProducerConsumerTest::LOGGER,
                    ring_buffer,
                    buffer_size,
                    dpids,
                    ProducerConsumerTest::SUPERVISOR,
                    std::move("TestConsumer"),
                    DEFAULT_CONFIGURATION,
                    3
            ),
            fbi(),
            delay(delay),
            consume_count(0)
    {}

    virtual ~TestConsumer() noexcept {}

    const image_t& get_fbi() const noexcept
    {
        return fbi;
    }

    virtual void finish() override {}

};

ProducerConsumerTest::ProducerConsumerTest()
{
    LOAD_DPIDS();
}

ProducerConsumerTest::~ProducerConsumerTest() {}

void ProducerConsumerTest::setUp() {}

void ProducerConsumerTest::tearDown() {}

void ProducerConsumerTest::test_producer() // TODO: Fix this
{
    registry_t dpids;
    const size_t buffer_size(4);
    buffer_t ring_buffer = new DPER[buffer_size];
    TestProducer tp(DPIDS, dpids, ring_buffer, buffer_size);
    tp.start(true);
    const bool condition_1(tp.get_dpid_count() == 7);
    tp.resume(true);
    this_thread::sleep_for(chrono::milliseconds(10));
    tp.stop(true);
    bool condition_2(true);
    bool condition_3(true);
    bool condition_4(true);
//    cout << tp.update_count << endl;
//    DPER* buffer_copy(tp.copy_buffer());
    for (size_t i = 0; i < buffer_size; ++i)
    {
        const DPCOM dpcom(dpids[i], ring_buffer[i].data);
//        const DPCOM dpcom(dpids[i], tp.get_event(i).data);
//        const DPCOM dpcom(dpids[i], buffer_copy[i].data);
        condition_2 = dpcom.data.get_epoch_time() > 1487233602000;
        condition_3 = dpcom.data.payload_pt1 > 0;
        condition_4 = dpcom.data.flags & DPVAL::OVERWRITE_FLAG;
        cout << "    " << dpcom << endl;
    }
    CPPUNIT_ASSERT(condition_1);
    CPPUNIT_ASSERT(condition_2);
    CPPUNIT_ASSERT(condition_3);
    CPPUNIT_ASSERT(condition_4);

    delete[] ring_buffer;
}

void ProducerConsumerTest::test_consumer()
{
    uint64_t tmp_0[7]{ 1,  2,  3,  4,  5,  6,  7};
    uint64_t tmp_1[7]{ 8,  9, 10, 11, 12, 13, 14};
    uint64_t tmp_2[7]{15, 16, 17, 18, 19, 20, 21};
    uint64_t tmp_3[7]{22, 23, 24, 25, 26, 27, 28};
    uint64_t tmp_4[7]{29, 30, 31, 32, 33, 34, 35};
//    buffer_t ring_buffer
//    {
//        {0, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::NEW_FLAG, 0, 0, tmp_0, RAW_BINARY}},
//        {1, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::NEW_FLAG, 0, 0, tmp_1, RAW_BINARY}},
//        {2, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::NEW_FLAG, 0, 0, tmp_2, RAW_BINARY}},
//        {3, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::NEW_FLAG, 0, 0, tmp_3, RAW_BINARY}},
//        {4, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::END_FLAG, 0, 0, tmp_4, RAW_BINARY}}
//    };
    buffer_t ring_buffer = new DPER[5];
    ring_buffer[0] = {0, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::NEW_FLAG, 0, 0, tmp_0, RAW_BINARY}};
    ring_buffer[1] = {1, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::NEW_FLAG, 0, 0, tmp_1, RAW_BINARY}};
    ring_buffer[2] = {2, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::NEW_FLAG, 0, 0, tmp_2, RAW_BINARY}};
    ring_buffer[3] = {3, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::NEW_FLAG, 0, 0, tmp_3, RAW_BINARY}};
    ring_buffer[4] = {4, DPVAL{DPVAL::DIRTY_FLAG | DPVAL::END_FLAG, 0, 0, tmp_4, RAW_BINARY}};

    image_t expected_fbi
    {
        DPCOM(DPIDS[0], ring_buffer[0].data),
        DPCOM(DPIDS[1], ring_buffer[1].data),
        DPCOM(DPIDS[2], ring_buffer[2].data),
        DPCOM(DPIDS[3], ring_buffer[3].data)
    };
    expected_fbi[0].data.flags &= ~DPVAL::DIRTY_FLAG;
    expected_fbi[1].data.flags &= ~DPVAL::DIRTY_FLAG;
    expected_fbi[2].data.flags &= ~DPVAL::DIRTY_FLAG;
    expected_fbi[3].data.flags &= ~DPVAL::DIRTY_FLAG;
    TestConsumer tc(DPIDS, ring_buffer, 5, chrono::nanoseconds(0));
    tc.start(false);
    this_thread::sleep_for(chrono::milliseconds(10));
//    for (const DPCOM dpcom : expected_fbi)
//    {
//        cout << dpcom << endl;
//    }
//    cout << endl;
//    for (const DPCOM dpcom : tc.get_fbi())
//    {
//        cout << dpcom << endl;
//    }
    CPPUNIT_ASSERT(expected_fbi == tc.get_fbi());

    delete[] ring_buffer;
}

void ProducerConsumerTest::test_both()
{
    const size_t buffer_size(32);
    DPER ring_buffer[buffer_size] alignas(4096);

    registry_t input_dpids;
    ADAPRO::Library::import_dpids
    (
            ProducerConsumerTest::LOGGER,
            input_dpids,
            "tests/resources/services2.lst"
    );
    registry_t output_dpids;
    TestProducer tp(input_dpids, output_dpids, ring_buffer, buffer_size);
    TestConsumer tc(output_dpids, ring_buffer, buffer_size, chrono::nanoseconds(0));
    tc.start(true);
    this_thread::sleep_for(chrono::milliseconds(10));
    tp.start(true);
    this_thread::sleep_for(chrono::milliseconds(10));
    tp.resume(true);
    this_thread::sleep_for(chrono::milliseconds(20));
    tp.stop(true);
    this_thread::sleep_for(chrono::milliseconds(10));

    tc.wait_for_state(STOPPED);

    cout << "    Update count:  " << tp.update_count << endl;
    cout << "    Consume count: " << tc.consume_count << endl;

    const image_t fbi(tc.get_fbi());
    const size_t expected_count(input_dpids.size());
    const size_t actual_count(fbi.size());
    cout << "    FBI size:" << actual_count << endl;
    const bool condition_1(actual_count == expected_count);
    bool condition_2(true);
    bool condition_3(false);
//    size_t terminator_count(0);
    for (size_t i = 0; i < actual_count; ++i)
    {
        const DPCOM& dpcom(fbi[i]);
        condition_2 = condition_2 && input_dpids[i] == dpcom.id;
        condition_3 = condition_3 || dpcom.data.flags & DPVAL::OVERWRITE_FLAG;
//        cout << "    " << dpcom << endl;
//        if (dpcom.data.flags & DPVAL::END_FLAG)
//        {
//            ++terminator_count;
//        }
    }
//    const bool condition_4(terminator_count == 1);
    const bool condition_5(
            ((int64_t) tp.update_count) - ((int64_t)tc.consume_count) >= 0
    );
    CPPUNIT_ASSERT(condition_1);
    CPPUNIT_ASSERT(condition_2);
    CPPUNIT_ASSERT(!condition_3);
//    CPPUNIT_ASSERT(condition_4);
    CPPUNIT_ASSERT(condition_5);
}
