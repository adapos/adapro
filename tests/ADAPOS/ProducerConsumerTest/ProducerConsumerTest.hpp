/*
 * File:   ProducerConsumerTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 15 February 2017, 17:47:48
 */

#ifndef PRODUCERCONSUMERTEST_HPP
#define PRODUCERCONSUMERTEST_HPP

#include <vector>
#include <deque>
#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Supervisor.hpp"
#include "../../../headers/control/AlignedAllocator.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/ADAPOS/DPIDUtils.hpp"
#include "../../../headers/ADAPOS/DataPointIdentifier.hpp"

class ProducerConsumerTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(ProducerConsumerTest);

//    CPPUNIT_TEST(test_producer);
    CPPUNIT_TEST(test_consumer);
    CPPUNIT_TEST(test_both);

    CPPUNIT_TEST_SUITE_END();

public:
    static ADAPRO::Control::Logger LOGGER;
    static ADAPRO::Control::Supervisor SUPERVISOR;
    static ADAPRO::ADAPOS::registry_t DPIDS;

    ProducerConsumerTest();
    virtual ~ProducerConsumerTest();
    void setUp();
    void tearDown();

private:

    static void LOAD_DPIDS()
    {
        if (DPIDS.size() == 0)
        {
            ADAPRO::ADAPOS::import_dpids(LOGGER, DPIDS, "tests/resources/services.lst");
        }
    }

    void test_producer();
    void test_consumer();
    void test_both();
};

#endif /* PRODUCERCONSUMERTEST_HPP */

