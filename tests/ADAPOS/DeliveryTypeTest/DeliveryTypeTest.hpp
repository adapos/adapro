/*
 * File:   DeliveryTypeTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 7 October 2016, 11:20:27
 */
#ifndef PAYLOADTYPETEST_HPP
#define PAYLOADTYPETEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class DeliveryTypeTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(DeliveryTypeTest);

    CPPUNIT_TEST(test_sizeof);
    CPPUNIT_TEST(test_read);
    CPPUNIT_TEST(test_show);
    CPPUNIT_TEST(test_composition);
    CPPUNIT_TEST(test_DPVAL_variant);
    CPPUNIT_TEST(test_dim_description);
    CPPUNIT_TEST(test_dim_buffer_size);

    CPPUNIT_TEST_SUITE_END();

public:
    DeliveryTypeTest();
    virtual ~DeliveryTypeTest();
    void setUp();
    void tearDown();

private:
    void test_sizeof();
    void test_read();
    void test_show();
    void test_composition();
    void test_DPVAL_variant();
    void test_dim_description();
    void test_dim_buffer_size();
};

#endif /* PAYLOADTYPETEST_HPP */

