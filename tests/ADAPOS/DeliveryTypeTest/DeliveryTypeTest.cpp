/*
 * File:   DeliveryTypeTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 7 October 2016, 11:20:27
 */

#include <string>
#include <stdexcept>
#include "DeliveryTypeTest.hpp"
#include "../../../headers/ADAPOS/DeliveryType.hpp"
#include "../../../headers/library/GenericFunctions.hpp"

using namespace std;
using namespace ADAPRO::Library;
using namespace ADAPRO::ADAPOS;

CPPUNIT_TEST_SUITE_REGISTRATION(DeliveryTypeTest);

DeliveryTypeTest::DeliveryTypeTest() {}

DeliveryTypeTest::~DeliveryTypeTest() {}

void DeliveryTypeTest::setUp() {}

void DeliveryTypeTest::tearDown() {}

void DeliveryTypeTest::test_sizeof()
{
    CPPUNIT_ASSERT(sizeof(DeliveryType) == 4);
}

void DeliveryTypeTest::test_read()
{
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/Int")         == RAW_INT);
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/Uint")        == RAW_UINT);
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/Float")       == RAW_FLOAT);
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/Double")      == RAW_DOUBLE);
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/Bool")        == RAW_BOOL);
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/Char")        == RAW_CHAR);
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/String")      == RAW_STRING);
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/Time")        == RAW_TIME);
    CPPUNIT_ASSERT(read<DeliveryType>("Raw/Binary")      == RAW_BINARY);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/Int")       == DPVAL_INT);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/Uint")      == DPVAL_UINT);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/Float")     == DPVAL_FLOAT);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/Double")    == DPVAL_DOUBLE);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/Bool")      == DPVAL_BOOL);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/Char")      == DPVAL_CHAR);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/String")    == DPVAL_STRING);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/Time")      == DPVAL_TIME);
    CPPUNIT_ASSERT(read<DeliveryType>("DPVAL/Binary")    == DPVAL_BINARY);
    bool exception_1(false);
    bool exception_2(false);
    try
    {
        read<DeliveryType>("TIme");
    }
    catch (const std::domain_error& e)
    {
        exception_1 = true;
    }
    try
    {
        read<DeliveryType>("grfffgsds");
    }
    catch (const std::domain_error& e)
    {
        exception_2 = true;
    }
    CPPUNIT_ASSERT(exception_1);
    CPPUNIT_ASSERT(exception_2);
}

void DeliveryTypeTest::test_show()
{
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_INT)       == "Raw/Int");
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_UINT)      == "Raw/Uint");
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_FLOAT)     == "Raw/Float");
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_DOUBLE)    == "Raw/Double");
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_BOOL)      == "Raw/Bool");
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_CHAR)      == "Raw/Char");
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_STRING)    == "Raw/String");
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_TIME)      == "Raw/Time");
    CPPUNIT_ASSERT(show<DeliveryType>(RAW_BINARY)    == "Raw/Binary");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_INT)     == "DPVAL/Int");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_UINT)    == "DPVAL/Uint");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_FLOAT)   == "DPVAL/Float");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_DOUBLE)  == "DPVAL/Double");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_BOOL)    == "DPVAL/Bool");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_CHAR)    == "DPVAL/Char");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_STRING)  == "DPVAL/String");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_TIME)    == "DPVAL/Time");
    CPPUNIT_ASSERT(show<DeliveryType>(DPVAL_BINARY)  == "DPVAL/Binary");
    CPPUNIT_ASSERT(show<DeliveryType>(VOID)          == "Void");
    bool exception(false);
    try
    {
        show<DeliveryType>((DeliveryType) 0x07);
    }
    catch (const std::domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

void DeliveryTypeTest::test_composition()
{
    using PT = DeliveryType;
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_INT))          == RAW_INT);
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_UINT))         == RAW_UINT);
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_FLOAT))        == RAW_FLOAT);
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_DOUBLE))       == RAW_DOUBLE);
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_BOOL))         == RAW_BOOL);
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_CHAR))         == RAW_CHAR);
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_STRING))       == RAW_STRING);
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_TIME))         == RAW_TIME);
    CPPUNIT_ASSERT(read<PT>(show<PT>(RAW_BINARY))       == RAW_BINARY);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_INT))        == DPVAL_INT);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_UINT))       == DPVAL_UINT);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_FLOAT))      == DPVAL_FLOAT);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_DOUBLE))     == DPVAL_DOUBLE);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_BOOL))       == DPVAL_BOOL);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_CHAR))       == DPVAL_CHAR);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_STRING))     == DPVAL_STRING);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_TIME))       == DPVAL_TIME);
    CPPUNIT_ASSERT(read<PT>(show<PT>(DPVAL_BINARY))     == DPVAL_BINARY);
    CPPUNIT_ASSERT(read<PT>(show<PT>(VOID))             == VOID);
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/Int"))        == "Raw/Int");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/Uint"))       == "Raw/Uint");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/Float"))      == "Raw/Float");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/Double"))     == "Raw/Double");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/Bool"))       == "Raw/Bool");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/Char"))       == "Raw/Char");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/String"))     == "Raw/String");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/Time"))       == "Raw/Time");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Raw/Binary"))     == "Raw/Binary");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/Int"))      == "DPVAL/Int");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/Uint"))     == "DPVAL/Uint");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/Float"))    == "DPVAL/Float");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/Double"))   == "DPVAL/Double");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/Bool"))     == "DPVAL/Bool");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/Char"))     == "DPVAL/Char");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/String"))   == "DPVAL/String");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/Time"))     == "DPVAL/Time");
    CPPUNIT_ASSERT(show<PT>(read<PT>("DPVAL/Binary"))   == "DPVAL/Binary");
    CPPUNIT_ASSERT(show<PT>(read<PT>("Void"))           == "Void");
}

void DeliveryTypeTest::test_DPVAL_variant()
{
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_INT));
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_UINT));
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_FLOAT));
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_DOUBLE));
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_BOOL));
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_CHAR));
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_STRING));
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_TIME));
    CPPUNIT_ASSERT(!DPVAL_variant(RAW_BINARY));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_INT));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_UINT));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_FLOAT));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_DOUBLE));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_BOOL));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_CHAR));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_STRING));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_TIME));
    CPPUNIT_ASSERT( DPVAL_variant(DPVAL_BINARY));
}

void DeliveryTypeTest::test_dim_description()
{
    CPPUNIT_ASSERT(dim_description(RAW_INT)         == "I:1");
    CPPUNIT_ASSERT(dim_description(RAW_UINT)        == "I:1");
    CPPUNIT_ASSERT(dim_description(RAW_FLOAT)       == "F:1");
    CPPUNIT_ASSERT(dim_description(RAW_DOUBLE)      == "D:1");
    CPPUNIT_ASSERT(dim_description(RAW_BOOL)        == "I:1");
    CPPUNIT_ASSERT(dim_description(RAW_CHAR)        == "C:4");
    CPPUNIT_ASSERT(dim_description(RAW_STRING)      == "C:55");
    CPPUNIT_ASSERT(dim_description(RAW_TIME)        == "I:2");
    CPPUNIT_ASSERT(dim_description(RAW_BINARY)      == "C:56");
    CPPUNIT_ASSERT(dim_description(DPVAL_INT)       == "S:2;I:1;I:1");
    CPPUNIT_ASSERT(dim_description(DPVAL_UINT)      == "S:2;I:1;I:1");
    CPPUNIT_ASSERT(dim_description(DPVAL_FLOAT)     == "S:2;I:1;F:1");
    CPPUNIT_ASSERT(dim_description(DPVAL_DOUBLE)    == "S:2;I:1;D:1");
    CPPUNIT_ASSERT(dim_description(DPVAL_BOOL)      == "S:2;I:1;I:1");
    CPPUNIT_ASSERT(dim_description(DPVAL_CHAR)      == "S:2;I:1;C:4");
    CPPUNIT_ASSERT(dim_description(DPVAL_STRING)    == "S:2;I:1;C:55");
    CPPUNIT_ASSERT(dim_description(DPVAL_TIME)      == "S:2;I:1;I:2");
    CPPUNIT_ASSERT(dim_description(DPVAL_BINARY)    == "S:2;I:1;C:56");
    bool exception(false);
    try
    {
        dim_description(VOID);
    }
    catch (const std::domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

void DeliveryTypeTest::test_dim_buffer_size()
{
    CPPUNIT_ASSERT(dim_buffer_size(RAW_INT)        == 4);
    CPPUNIT_ASSERT(dim_buffer_size(RAW_UINT)       == 4);
    CPPUNIT_ASSERT(dim_buffer_size(RAW_FLOAT)      == 4);
    CPPUNIT_ASSERT(dim_buffer_size(RAW_DOUBLE)     == 8);
    CPPUNIT_ASSERT(dim_buffer_size(RAW_BOOL)       == 4);
    CPPUNIT_ASSERT(dim_buffer_size(RAW_CHAR)       == 4);
    CPPUNIT_ASSERT(dim_buffer_size(RAW_STRING)     == 55);
    CPPUNIT_ASSERT(dim_buffer_size(RAW_TIME)       == 8);
    CPPUNIT_ASSERT(dim_buffer_size(RAW_BINARY)     == 56);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_INT)      == 64);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_UINT)     == 64);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_FLOAT)    == 64);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_DOUBLE)   == 64);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_BOOL)     == 64);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_CHAR)     == 64);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_STRING)   == 64);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_TIME)     == 64);
    CPPUNIT_ASSERT(dim_buffer_size(DPVAL_BINARY)   == 64);
    CPPUNIT_ASSERT(dim_buffer_size(VOID)           == 0);
}
