/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DPIDUtilsTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 21.10.2016, 12:11:53
 */

#ifndef DPIDUTILSTEST_HPP
#define DPIDUTILSTEST_HPP

#include <cppunit/extensions/HelperMacros.h>
#include "../../../headers/control/Logger.hpp"

class DPIDUtilsTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(DPIDUtilsTest);

    CPPUNIT_TEST(test_import);
    CPPUNIT_TEST(test_import_old_format);
    CPPUNIT_TEST(test_export);
    CPPUNIT_TEST(test_both);

    CPPUNIT_TEST_SUITE_END();

public:
    DPIDUtilsTest();
    virtual ~DPIDUtilsTest();
    void setUp();
    void tearDown();

private:
    static ADAPRO::Control::Logger LOGGER;
    
    void test_import();
    void test_import_old_format();
    void test_export();
    void test_both();
};

#endif /* DPIDUTILSTEST_HPP */

