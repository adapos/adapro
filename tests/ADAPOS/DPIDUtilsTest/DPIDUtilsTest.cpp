/*
 * File:   DPIDUtilsTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 21 October 2016, 12:11:53
 */

#include <string>
#include <memory>
#include <deque>
#include <iostream>
#include <cstdio>
#include <stdexcept>
#include "DPIDUtilsTest.hpp"
#include "../../../headers/control/Session.hpp"
#include "../../../headers/control/Logger.hpp"
#include "../../../headers/ADAPOS/DPIDUtils.hpp"
#include "../../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../../headers/ADAPOS/DeliveryType.hpp"
#include "../../../headers/ADAPOS/Typedefs.hpp"
#include "../../../headers/data/LoggingLevel.hpp"

using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

CPPUNIT_TEST_SUITE_REGISTRATION(DPIDUtilsTest);

ADAPRO::Control::Logger DPIDUtilsTest::LOGGER;

DPIDUtilsTest::DPIDUtilsTest() {}

DPIDUtilsTest::~DPIDUtilsTest() {}

void DPIDUtilsTest::setUp() {}

void DPIDUtilsTest::tearDown() {}

void DPIDUtilsTest::test_import()
{
    registry_t dpids;
    import_dpids(LOGGER, dpids, "tests/resources/services.lst");
    CPPUNIT_ASSERT(dpids.size() == 18);
    CPPUNIT_ASSERT(dpids[0].get_alias() == std::string("TST_136") &&
            dpids[0].get_type() == DeliveryType::RAW_INT);
    CPPUNIT_ASSERT(dpids[1].get_alias() == std::string("TST_137") &&
            dpids[1].get_type() == DeliveryType::RAW_UINT);
    CPPUNIT_ASSERT(dpids[2].get_alias() == std::string("TST_138") &&
            dpids[2].get_type() == DeliveryType::RAW_DOUBLE);
    CPPUNIT_ASSERT(dpids[3].get_alias() == std::string("TST_139") &&
            dpids[3].get_type() == DeliveryType::RAW_BOOL);
    CPPUNIT_ASSERT(dpids[4].get_alias() == std::string("TST_140") &&
            dpids[4].get_type() == DeliveryType::RAW_CHAR);
    CPPUNIT_ASSERT(dpids[5].get_alias() == std::string("TST_141") &&
            dpids[5].get_type() == DeliveryType::RAW_STRING);
    CPPUNIT_ASSERT(dpids[6].get_alias() == std::string("TST_142") &&
            dpids[6].get_type() == DeliveryType::RAW_TIME);
    CPPUNIT_ASSERT(dpids[7].get_alias() == std::string("TST_143") &&
            dpids[7].get_type() == DeliveryType::RAW_BINARY);
    CPPUNIT_ASSERT(dpids[8].get_alias() == std::string("TST_144") &&
            dpids[8].get_type() == DeliveryType::DPVAL_INT);
    CPPUNIT_ASSERT(dpids[9].get_alias() == std::string("TST_145") &&
            dpids[9].get_type() == DeliveryType::DPVAL_UINT);
    CPPUNIT_ASSERT(dpids[10].get_alias() == std::string("TST_146") &&
            dpids[10].get_type() == DeliveryType::DPVAL_DOUBLE);
    CPPUNIT_ASSERT(dpids[11].get_alias() == std::string("TST_147") &&
            dpids[11].get_type() == DeliveryType::DPVAL_BOOL);
    CPPUNIT_ASSERT(dpids[12].get_alias() == std::string("TST_148") &&
            dpids[12].get_type() == DeliveryType::DPVAL_CHAR);
    CPPUNIT_ASSERT(dpids[13].get_alias() == std::string("TST_149") &&
            dpids[13].get_type() == DeliveryType::DPVAL_STRING);
    CPPUNIT_ASSERT(dpids[14].get_alias() == std::string("TST_150") &&
            dpids[14].get_type() == DeliveryType::DPVAL_TIME);
    CPPUNIT_ASSERT(dpids[15].get_alias() == std::string("TST_151") &&
            dpids[15].get_type() == DeliveryType::DPVAL_BINARY);
    CPPUNIT_ASSERT(dpids[16].get_alias() == std::string("TST_153") &&
            dpids[16].get_type() == DeliveryType::RAW_FLOAT);
    CPPUNIT_ASSERT(dpids[17].get_alias() == std::string("TST_154") &&
            dpids[17].get_type() == DeliveryType::DPVAL_FLOAT);
//    CPPUNIT_ASSERT(dpids[15].get_alias() == std::string("TST_152") &&
//            dpids[15].get_type() == DeliveryType::VOID);
}

void DPIDUtilsTest::test_import_old_format()
{
    registry_t dpids;
    bool exception(false);
    try
    {
        import_dpids(LOGGER, dpids, "tests/resources/services_old_fmt.lst");
    }
    catch (const std::domain_error& e)
    {
        exception = true;
    }
    CPPUNIT_ASSERT(exception);
}

void DPIDUtilsTest::test_export()
{
    registry_t dpids;
    DPID dpid;
    DPID::FILL(dpid, "TST_01", RAW_INT);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_02", RAW_UINT);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_03", RAW_DOUBLE);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_04", RAW_BOOL);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_05", RAW_CHAR);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_06", RAW_STRING);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_07", RAW_TIME);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_08", RAW_BINARY);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_09", DPVAL_INT);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_10", DPVAL_UINT);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_11", DPVAL_DOUBLE);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_12", DPVAL_BOOL);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_13", DPVAL_CHAR);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_14", DPVAL_STRING);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_15", DPVAL_TIME);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_16", DPVAL_BINARY);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_17", RAW_FLOAT);
    dpids.push_back(dpid);
    DPID::FILL(dpid, "TST_18", DPVAL_FLOAT);
    dpids.push_back(dpid);
    export_dpids(dpids, "tests/resources/services3.lst");
    CPPUNIT_ASSERT(remove("tests/resources/services3.lst") == 0);
}

void DPIDUtilsTest::test_both()
{

    registry_t dpids1;
    DPID dpid;
    DPID::FILL(dpid, "TST_08", RAW_INT);
    dpids1.push_back(dpid);
    DPID::FILL(dpid, "TST_09", RAW_UINT);
    dpids1.push_back(dpid);
    DPID::FILL(dpid, "TST_10", RAW_DOUBLE);
    dpids1.push_back(dpid);
    DPID::FILL(dpid, "TST_11", RAW_BOOL);
    dpids1.push_back(dpid);
    DPID::FILL(dpid, "TST_12", RAW_CHAR);
    dpids1.push_back(dpid);
    DPID::FILL(dpid, "TST_13", RAW_STRING);
    dpids1.push_back(dpid);
    DPID::FILL(dpid, "TST_14", RAW_TIME);
    dpids1.push_back(dpid);
    DPID::FILL(dpid, "TST_15", RAW_FLOAT);
    dpids1.push_back(dpid);
    export_dpids(dpids1, "tests/resources/services4.lst");
    registry_t dpids2;
    import_dpids(LOGGER, dpids2, "tests/resources/services4.lst");
    CPPUNIT_ASSERT(dpids1 == dpids2);
    CPPUNIT_ASSERT(remove("tests/resources/services4.lst") == 0);
}
