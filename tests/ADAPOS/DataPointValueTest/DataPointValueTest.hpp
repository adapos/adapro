/*
 * File:   DataPointValueTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23 September 2016, 17:18:27
 */

#ifndef DATAPOINTVALUETEST_HPP
#define DATAPOINTVALUETEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class DataPointValueTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(DataPointValueTest);

    CPPUNIT_TEST(test_size_and_alignment);
    CPPUNIT_TEST(test_constructors);
    CPPUNIT_TEST(test_new_setter);
    CPPUNIT_TEST(test_flag_free_setter);
    CPPUNIT_TEST(test_new_binary_data_setter);
    CPPUNIT_TEST(test_get_flags);
    CPPUNIT_TEST(test_get_timestamp);
    CPPUNIT_TEST(test_update_timestamp);
    CPPUNIT_TEST(test_get_epoch_time);
    CPPUNIT_TEST(test_copy_assignment_operator);
    CPPUNIT_TEST(test_put_to_operator);

    CPPUNIT_TEST_SUITE_END();

public:
    DataPointValueTest();
    virtual ~DataPointValueTest();
    void setUp();
    void tearDown();

private:
    void test_size_and_alignment();
    void test_constructors();
    void test_new_setter();
    void test_flag_free_setter();
    void test_new_binary_data_setter();
    void test_get_flags();
    void test_get_timestamp();
    void test_update_timestamp();
    void test_get_epoch_time();
    void test_copy_assignment_operator();
    void test_equality_comparison();
    void test_put_to_operator();
};

#endif /* DATAPOINTVALUETEST_HPP */

