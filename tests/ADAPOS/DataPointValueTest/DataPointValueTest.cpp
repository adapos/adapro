/*
 * File:   DataPointValueTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 23.9.2016, 17:18:27
 */

#include <algorithm>
#include <string>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <iostream>
#include <sstream>
#include <cstdint>
#include <thread>
#include <atomic>
#include <vector>
#include "DataPointValueTest.hpp"
#include "../../../headers/library/Clock.hpp"
#include "../../../headers/ADAPOS/DataPointValue.hpp"

using namespace std;
using namespace ADAPRO::ADAPOS;

CPPUNIT_TEST_SUITE_REGISTRATION(DataPointValueTest);

DataPointValueTest::DataPointValueTest(){}

DataPointValueTest::~DataPointValueTest() {}

void DataPointValueTest::setUp() {}

void DataPointValueTest::tearDown() {}

alignas(64) static DataPointValue STATIC_DPVALS[3];

void DataPointValueTest::test_size_and_alignment()
{
    CPPUNIT_ASSERT(sizeof(DataPointValue) == 64);
    CPPUNIT_ASSERT(alignof(DataPointValue) == 64);
    CPPUNIT_ASSERT(sizeof(atomic<DataPointValue>) == 64);
    CPPUNIT_ASSERT(alignof(atomic<DataPointValue>) == 64);
    cout << "atomic<DPVAL> is " <<
            (atomic<DataPointValue>().is_lock_free() ? "" : "not ") <<
            "lock-free." << endl;

    char __attribute__ ((unused)) padding_1[2];
    DataPointValue dpvals_1[3];
    char __attribute__ ((unused)) padding_2[2];
//    cout << dpvals_2 << endl;
//    cout << (((uint64_t) dpvals_1) & 0x3F) << endl;
    CPPUNIT_ASSERT((((uint64_t) dpvals_1) & 0x3F) == 0);

    char __attribute__ ((unused)) padding_3[2];
//    cout << STATIC_DPVALS << endl;
//    cout << (((uint64_t) STATIC_DPVALS) & 0x3F) << endl;
    CPPUNIT_ASSERT((((uint64_t) STATIC_DPVALS) & 0x3F) == 0);

//    // Alignment doesn't work here. For dynamic allocation, we need to use
//    // ADAPRO::Control::AlignedAllocator.
//    DataPointValue* dpvals_2(new DataPointValue[3]);
//    char padding_4[2];
//    cout << (((uint64_t) dpvals_2) & 0x3F) << endl;
////    CPPUNIT_ASSERT((((uint64_t) dpvals_3) & 0x3F) == 0);

}

void DataPointValueTest::test_constructors()
{
    DataPointValue dpval1;
    CPPUNIT_ASSERT(dpval1.msec          == 0);
    CPPUNIT_ASSERT(dpval1.sec           == 0);
    CPPUNIT_ASSERT(dpval1.flags         == 0);
    CPPUNIT_ASSERT(dpval1.payload_pt1   == 0);
    CPPUNIT_ASSERT(dpval1.payload_pt2   == 0);
    CPPUNIT_ASSERT(dpval1.payload_pt3   == 0);
    CPPUNIT_ASSERT(dpval1.payload_pt4   == 0);
    CPPUNIT_ASSERT(dpval1.payload_pt5   == 0);
    CPPUNIT_ASSERT(dpval1.payload_pt6   == 0);
    CPPUNIT_ASSERT(dpval1.payload_pt7   == 0);

    uint64_t* buf = new uint64_t[7];
    buf[0] = 2;
    buf[1] = 3;
    buf[2] = 5;
    buf[3] = 7;
    buf[4] = 11;
    buf[5] = 13;
    buf[6] = 17;
    DataPointValue dpval2(0x7777, 999, 123456789, buf, DeliveryType::RAW_BINARY);
    CPPUNIT_ASSERT(dpval2.flags       == 0x7777);
    CPPUNIT_ASSERT(dpval2.msec        == 999);
    CPPUNIT_ASSERT(dpval2.sec         == 123456789);
    CPPUNIT_ASSERT(dpval2.payload_pt1 == 2);
    CPPUNIT_ASSERT(dpval2.payload_pt2 == 3);
    CPPUNIT_ASSERT(dpval2.payload_pt3 == 5);
    CPPUNIT_ASSERT(dpval2.payload_pt4 == 7);
    CPPUNIT_ASSERT(dpval2.payload_pt5 == 11);
    CPPUNIT_ASSERT(dpval2.payload_pt6 == 13);
    CPPUNIT_ASSERT(dpval2.payload_pt7 == 17);

    DataPointValue dpval3(0x4321, 999, 123456789, buf, DeliveryType::RAW_BINARY);
    CPPUNIT_ASSERT(dpval3.flags       == 0x4321);
    CPPUNIT_ASSERT(dpval3.msec        == 999);
    CPPUNIT_ASSERT(dpval3.sec         == 123456789);
    CPPUNIT_ASSERT(dpval3.payload_pt1 == 2);
    CPPUNIT_ASSERT(dpval3.payload_pt2 == 3);
    CPPUNIT_ASSERT(dpval3.payload_pt3 == 5);
    CPPUNIT_ASSERT(dpval3.payload_pt4 == 7);
    CPPUNIT_ASSERT(dpval3.payload_pt5 == 11);
    CPPUNIT_ASSERT(dpval3.payload_pt6 == 13);
    CPPUNIT_ASSERT(dpval3.payload_pt7 == 17);

    delete[] buf;
}

void DataPointValueTest::test_new_setter()
{
    DataPointValue dpval1;
    uint64_t* buf = new uint64_t[7];
    buf[0] = 2;
    buf[1] = 3;
    buf[2] = 5;
    buf[3] = 7;
    buf[4] = 11;
    buf[5] = 13;
    buf[6] = 17;
    dpval1.set(0x7777, 999, 123456789, buf, DeliveryType::RAW_BINARY);
    CPPUNIT_ASSERT(dpval1.flags       == 0x7777);
    CPPUNIT_ASSERT(dpval1.msec        == 999);
    CPPUNIT_ASSERT(dpval1.sec         == 123456789);
    CPPUNIT_ASSERT(dpval1.payload_pt1 == 2);
    CPPUNIT_ASSERT(dpval1.payload_pt2 == 3);
    CPPUNIT_ASSERT(dpval1.payload_pt3 == 5);
    CPPUNIT_ASSERT(dpval1.payload_pt4 == 7);
    CPPUNIT_ASSERT(dpval1.payload_pt5 == 11);
    CPPUNIT_ASSERT(dpval1.payload_pt6 == 13);
    CPPUNIT_ASSERT(dpval1.payload_pt7 == 17);
}

void DataPointValueTest::test_flag_free_setter()
{
    uint64_t* buf = new uint64_t[7];
    buf[0] = 0;
    buf[1] = 1;
    buf[2] = 1;
    buf[3] = 2;
    buf[4] = 4;
    buf[5] = 7;
    buf[6] = 13;
    DataPointValue dpval(10007, 789, 123456, buf, DeliveryType::DPVAL_BINARY);
    buf[0] = 24;
    buf[1] = 44;
    buf[2] = 81;
    buf[3] = 149;
    buf[4] = 274;
    buf[5] = 504;
    buf[6] = 927;
    dpval.set(123, 987654, buf, DeliveryType::RAW_UINT);
    CPPUNIT_ASSERT(dpval.flags       == 10007 );
    CPPUNIT_ASSERT(dpval.msec        == 123   );
    CPPUNIT_ASSERT(dpval.sec         == 987654);
    CPPUNIT_ASSERT(dpval.payload_pt1 == 24    );
    CPPUNIT_ASSERT(dpval.payload_pt2 == 1     );
    CPPUNIT_ASSERT(dpval.payload_pt3 == 1     );
    CPPUNIT_ASSERT(dpval.payload_pt4 == 2     );
    CPPUNIT_ASSERT(dpval.payload_pt5 == 4     );
    CPPUNIT_ASSERT(dpval.payload_pt6 == 7     );
    CPPUNIT_ASSERT(dpval.payload_pt7 == 13    );
}

void DataPointValueTest::test_new_binary_data_setter()
{
    uint64_t* buf = new uint64_t[7];
    buf[0] = 0;
    buf[1] = 1;
    buf[2] = 1;
    buf[3] = 2;
    buf[4] = 3;
    buf[5] = 5;
    buf[6] = 8;
    DataPointValue dpval1, dpval2(15367, 789, 123456, buf, DeliveryType::DPVAL_BINARY);
    dpval1.set((uint64_t*) &dpval2, DeliveryType::DPVAL_BINARY);
    CPPUNIT_ASSERT(dpval1.flags       == dpval2.flags      );
    CPPUNIT_ASSERT(dpval1.msec        == dpval2.msec       );
    CPPUNIT_ASSERT(dpval1.sec         == dpval2.sec        );
    CPPUNIT_ASSERT(dpval1.payload_pt1 == dpval2.payload_pt1);
    CPPUNIT_ASSERT(dpval1.payload_pt2 == dpval2.payload_pt2);
    CPPUNIT_ASSERT(dpval1.payload_pt3 == dpval2.payload_pt3);
    CPPUNIT_ASSERT(dpval1.payload_pt4 == dpval2.payload_pt4);
    CPPUNIT_ASSERT(dpval1.payload_pt5 == dpval2.payload_pt5);
    CPPUNIT_ASSERT(dpval1.payload_pt6 == dpval2.payload_pt6);
    CPPUNIT_ASSERT(dpval1.payload_pt7 == dpval2.payload_pt7);
}

void DataPointValueTest::test_get_flags()
{
    CPPUNIT_ASSERT(DataPointValue().get_flags() == 0);
    uint64_t* buf = new uint64_t[7];
    CPPUNIT_ASSERT(DataPointValue(15367, 789, 123456, buf, DeliveryType::RAW_BINARY).get_flags() == 15367);
}

void DataPointValueTest::test_get_timestamp()
{
    CPPUNIT_ASSERT(*(DataPointValue()
            .get_timestamp()) == "1970-01-01T00:00:00.000Z");
    uint64_t* buf = new uint64_t[7];
    CPPUNIT_ASSERT(*(DataPointValue(0, 0, 0, buf, DeliveryType::RAW_BINARY)
            .get_timestamp()) == "1970-01-01T00:00:00.000Z");
    buf[3] = 2345;
    CPPUNIT_ASSERT(*(DataPointValue(5367, 0, 0, buf, DeliveryType::RAW_BINARY)
            .get_timestamp()) == "1970-01-01T00:00:00.000Z");
    CPPUNIT_ASSERT(*(DataPointValue(0, 462, 1474645621, buf, DeliveryType::RAW_BINARY)
            .get_timestamp()) == "2016-09-23T15:47:01.462Z");
}

void DataPointValueTest::test_update_timestamp()
{
    DataPointValue dpval;
    dpval.update_timestamp();
    string today("2017-03-07T10:14:25.026Z");
    string then(*(dpval.get_timestamp()));
    CPPUNIT_ASSERT(lexicographical_compare(today.begin(), today.end(),
            then.begin(), then.end()));
}

void DataPointValueTest::test_get_epoch_time()
{
    uint64_t et = ADAPRO::Library::epoch_time();
    uint64_t* buf = new uint64_t[7];
    const DataPointValue dpval(0, et % 1000, et / 1000, buf, DeliveryType::RAW_BINARY);
    CPPUNIT_ASSERT(dpval.get_epoch_time() == et);
}

void DataPointValueTest::test_copy_assignment_operator()
{
    uint64_t* buf = new uint64_t[7];
    buf[0] = 0;
    buf[1] = 1;
    buf[2] = 2;
    buf[3] = 3;
    buf[4] = 4;
    buf[5] = 5;
    buf[6] = 6;
    DataPointValue dpval1, dpval2(15367, 789, 123456, buf, DeliveryType::RAW_BINARY);
    dpval1 = dpval2;
    CPPUNIT_ASSERT(dpval1.flags       == dpval2.flags      );
    CPPUNIT_ASSERT(dpval1.msec        == dpval2.msec       );
    CPPUNIT_ASSERT(dpval1.sec         == dpval2.sec        );
    CPPUNIT_ASSERT(dpval1.payload_pt1 == dpval2.payload_pt1);
    CPPUNIT_ASSERT(dpval1.payload_pt2 == dpval2.payload_pt2);
    CPPUNIT_ASSERT(dpval1.payload_pt3 == dpval2.payload_pt3);
    CPPUNIT_ASSERT(dpval1.payload_pt4 == dpval2.payload_pt4);
    CPPUNIT_ASSERT(dpval1.payload_pt5 == dpval2.payload_pt5);
    CPPUNIT_ASSERT(dpval1.payload_pt6 == dpval2.payload_pt6);
    CPPUNIT_ASSERT(dpval1.payload_pt7 == dpval2.payload_pt7);
}

void DataPointValueTest::test_equality_comparison()
{
    uint64_t data[7]
    {
        0xA1'6C'EF'97'BC'8C'DC'27,
        0x96'eb'70'68'bc'1c'af'e5,
        0x17'22'd2'84'97'fa'0f'bc,
        0x06'b7'93'7d'95'ec'28'd5,
        0x86'39'29'8e'd8'fb'16'26,
        0xa4'a1'43'ed'83'65'2d'e1,
        0x71'1a'21'7d'dc'59'e3'26
    };
    DataPointValue dpval1(15367, 789, 123456, data, DeliveryType::RAW_BINARY);
    DataPointValue dpval2(15367, 789, 123456, data, DeliveryType::RAW_BINARY);
    DataPointValue dpval3(15367, 789, 123456, data, DeliveryType::RAW_BINARY);
    CPPUNIT_ASSERT(dpval1 == dpval1);
    CPPUNIT_ASSERT(dpval1 == dpval2);
    CPPUNIT_ASSERT(dpval2 == dpval1);
    CPPUNIT_ASSERT(dpval2 == dpval3);
    CPPUNIT_ASSERT(dpval1 == dpval3);
    dpval2.payload_pt7 = 0x71'1a'21'6d'dc'59'e3'26; // One-bit change.
    CPPUNIT_ASSERT(dpval1 != dpval2);
    CPPUNIT_ASSERT(dpval2 != dpval1);
}

void DataPointValueTest::test_put_to_operator()
{
    std::stringstream ss;
    ss << DataPointValue();
    CPPUNIT_ASSERT(ss.str() == "-------- --------;1970-01-01T00:00:00.000Z");

    uint64_t* buf = new uint64_t[7];
    ss.str(std::string());
    ss << DataPointValue(0x1214, 462, 1474645621, buf, DeliveryType::RAW_BINARY);
    CPPUNIT_ASSERT(ss.str() == "--M-D--- -V--F---;2016-09-23T15:47:01.462Z");
}
