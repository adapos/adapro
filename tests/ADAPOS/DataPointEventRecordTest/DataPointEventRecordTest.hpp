/*
 * File:   DataPointEventRecordTest.hpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 21 February 2017, 15:55:44
 */

#ifndef DATAPOINTEVENTRECORDTEST_HPP
#define DATAPOINTEVENTRECORDTEST_HPP

#include <cppunit/extensions/HelperMacros.h>

class DataPointEventRecordTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(DataPointEventRecordTest);

    CPPUNIT_TEST(test_size_and_alignment);
    CPPUNIT_TEST(test_ctors_and_assignment);
    CPPUNIT_TEST(test_put_to_operator);    

    CPPUNIT_TEST_SUITE_END();

public:
    DataPointEventRecordTest();
    virtual ~DataPointEventRecordTest();
    void setUp();
    void tearDown();

private:
    void test_size_and_alignment();
    void test_ctors_and_assignment();
    void test_put_to_operator();
};

#endif /* DATAPOINTEVENTRECORDTEST_HPP */

