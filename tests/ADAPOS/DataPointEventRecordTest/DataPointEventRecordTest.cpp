/*
 * File:   DataPointEventRecordTest.cpp
 * Author: John Lång (john.larry.lang@cern.ch)
 *
 * Created on 21 February 2017, 15:55:44
 */

#include <cstdint>
#include <mutex>
#include <vector>
#include <iostream>
#include <regex>
#include "../../../headers/ADAPOS/DataPointValue.hpp"
#include "../../../headers/ADAPOS/DeliveryType.hpp"
#include "../../../headers/ADAPOS/DataPointEventRecord.hpp"
#include "DataPointEventRecordTest.hpp"

using namespace std;
using namespace ADAPRO::ADAPOS;

CPPUNIT_TEST_SUITE_REGISTRATION(DataPointEventRecordTest);

DataPointEventRecordTest::DataPointEventRecordTest() {}

DataPointEventRecordTest::~DataPointEventRecordTest() {}

void DataPointEventRecordTest::setUp() {}

void DataPointEventRecordTest::tearDown() {
}

void DataPointEventRecordTest::test_size_and_alignment()
{
    CPPUNIT_ASSERT(sizeof(DataPointEventRecord) == 128);
    CPPUNIT_ASSERT(alignof(DataPointEventRecord) == 128);

    char __attribute__ ((unused)) padding_1[2];
    vector<DataPointEventRecord> dpers_1(3);
    char __attribute__ ((unused)) padding_2[2];
    DataPointEventRecord dpers_2[3];
    char __attribute__ ((unused)) padding_3[2];
//    CPPUNIT_ASSERT((((uint64_t) dpers_1.data()) & 0x7F) == 0);
    CPPUNIT_ASSERT((((uint64_t) dpers_2) & 0x7F) == 0);
}

void DataPointEventRecordTest::test_ctors_and_assignment()
{
    const uint64_t data_1[7]{0, 0, 0, 0, 0, 0, 0};
    const uint64_t data_2[7]
    {
        0xFB'86'BB'5A'78'3B'E8'8A,
        0xD7'8F'AE'19'42'86'3C'AD,
        0x47'53'E2'80'DC'C8'F6'03,
        0xCB'01'B3'66'7A'9D'5B'C9,
        0x7A'D2'D0'8B'8E'57'5A'36,
        0x11'07'D6'74'57'5A'E4'21,
        0xD3'B5'9C'26'39'D5'6C'84
    };
    const DataPointEventRecord dper_1, dper_2;
    const DataPointEventRecord dper_3{43, DataPointValue(0xDEAD, 999, 999999999, data_1, RAW_BINARY)};
    const DataPointEventRecord dper_4{43, DataPointValue(0xDEAD, 999, 999999999, data_2, RAW_BINARY)};
    const DataPointEventRecord dper_5(dper_4);
    DataPointEventRecord dper_6;
    dper_6 = dper_4;

    CPPUNIT_ASSERT(dper_1.current_dpid_index    == dper_2.current_dpid_index);
    CPPUNIT_ASSERT(dper_1.previous_dpid_index   == dper_2.previous_dpid_index);
    CPPUNIT_ASSERT(dper_1.data                  == dper_2.data);
    CPPUNIT_ASSERT(dper_3.current_dpid_index    == dper_4.current_dpid_index);
    CPPUNIT_ASSERT(dper_3.previous_dpid_index   == dper_4.previous_dpid_index);
    CPPUNIT_ASSERT(dper_3.data                  != dper_4.data);
    CPPUNIT_ASSERT(dper_5.current_dpid_index    == dper_4.current_dpid_index);
    CPPUNIT_ASSERT(dper_5.previous_dpid_index   == dper_4.previous_dpid_index);
    CPPUNIT_ASSERT(dper_5.data                  == dper_4.data);
    CPPUNIT_ASSERT(dper_6.current_dpid_index    == dper_4.current_dpid_index);
    CPPUNIT_ASSERT(dper_6.previous_dpid_index   == dper_4.previous_dpid_index);
    CPPUNIT_ASSERT(dper_6.data                  == dper_4.data);
}

void DataPointEventRecordTest::test_put_to_operator()
{
    stringstream ss;
    DataPointEventRecord dper;
    regex empty_dper("^[0-9A-F]+;0;0;-------- --------;1970-01-01T00:00:00.000Z$");
    regex updated_dper("^[0-9A-F]+;42;53;----D--- --------;"
            "[0-9]{4}(-[0-9]{2}){2}T([0-9]{2}:){2}[0-9]{2}.[0-9]{3}Z$");

    ss << dper;
    CPPUNIT_ASSERT(regex_match(ss.str(), empty_dper));
    ss.str(std::string());

    dper.current_dpid_index = 42;
    dper.previous_dpid_index = 53;
    dper.data.flags = DataPointValue::DIRTY_FLAG;
    dper.data.update_timestamp();

    ss << dper;
    CPPUNIT_ASSERT(regex_match(ss.str(), updated_dper));
}
