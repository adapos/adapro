
#include <string>
#include <memory>
#include <map>
#include <unordered_set>
#include <utility>
#include <functional>
#include <queue>
#include <mutex>
#include <thread>
#include <chrono>
#include <stdexcept>
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/control/Supervisor.hpp"
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/DIM/SubscriptionType.hpp"
#include "../../headers/DIM/Controller.hpp"
#include "../../headers/DIM/TaskType.hpp"
#include "../../headers/DIM/TaskArguments.hpp"
#include "../../headers/DIM/Typedefs.hpp"
#include "../../headers/data/Typedefs.hpp"
#ifndef EXCLUDE_DIM
#include "/usr/include/dim/dim_common.h"
#include "../../headers/DIM/Wrapper.hpp"
#endif

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::DIM;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

Controller::Controller
(
        Logger& logger,
        Supervisor& supervisor,
        const ADAPRO::Data::config_t& configuration
)
noexcept:
        Worker
        (
                logger,
                supervisor,
                std::move("DIMController"),
                configuration,
                stoi(configuration.at(get<0>(DIM_CONTROLLER_CORE)))
        ),
        task_mutex(),
        task_queue(),
        services(),
        commands(),
        subscriptions()
{
#ifdef EXCLUDE_DIM
    print
    (
            "DIM support is disabled. DIM API calls will be skipped silently.",
            WARNING
    );
#endif
}

void Controller::prepare()
{
#ifndef EXCLUDE_DIM
#ifdef _GNU_SOURCE
        dim_long tag(0);
        dtq_start_timer
        (
                1,
                [](void* tag_ptr)
                {
                    pthread_setname_np(pthread_self(), "DIM-CB-thread");
                    Session::PRINT("pthread_setname_np called "
                            "for DIM callback thread.", ADAPRO_DEBUG);
                },
                (void*) &tag
        );
        this_thread::sleep_for(chrono::milliseconds(1100));
#endif
    Wrapper::common_set_dim_dns_node(configuration.at(get<0>(DIM_DNS_NODE)));
    Wrapper::common_set_dim_dns_port(
            stoi(configuration.at(get<0>(DIM_DNS_PORT))));
    Wrapper::server_start(configuration.at(get<0>(DIM_SERVER_NAME)));
#endif
}

void Controller::execute()
{
    unique_lock<mutex> lock(task_mutex);
    if (!task_queue.empty())
    {
        task_t task(task_queue.front());
        task_queue.pop();
        lock.unlock();
        carry_out(task);
    }
    else
    {
        tasks_waiting.wait_for
        (
                lock,
                chrono::milliseconds(100),
                [this](){return get_state() == RUNNING && task_queue.empty();}
        );
    }
}

void Controller::finish()
{
#ifndef EXCLUDE_DIM
    for (const service_id_t service_id : services)
    {
        Wrapper::server_remove(service_id);
    }
    for (const service_id_t service_id : commands)
    {
        Wrapper::server_remove(service_id);
    }
    for (const service_id_t service_id : subscriptions)
    {
        Wrapper::client_unsubscribe(service_id);
    }
    Wrapper::server_stop();
#endif
}

void Controller::carry_out(task_t task)
{
    switch (task.first)
    {
        case PUBLISH_SERVICE:
            publish_service(derefer<ServicePublication>(task.second));
            break;
        case PUBLISH_COMMAND:
            publish_command(derefer<CommandPublication>(task.second));
            break;
        case UPDATE_SERVICE:
            update_service(derefer<ServiceID>(task.second));
            break;
        case SUBSCRIBE_TO_SERVICE:
            subscribe_to_service(derefer<ServiceSubscription>(task.second));
            break;
        case CALL_COMMAND:
            call_command(derefer<CommandInvocation>(task.second));
            break;
        case OVERWRITE_TIMESTAMP:
            overwrite_timestamp(derefer<TimestampRequest>(task.second));
            break;
        default: // Should be unreachable:
            trap("Unknown command " + std::to_string((int) task.first) + ".");
    }
}

void Controller::publish_service(ServicePublication& argument)
{
    int buffer_size((int) argument.buffer_size);
    if (buffer_size >= 0)
    {
#ifndef EXCLUDE_DIM
        dim_long tag((dim_long) argument.tag); // Ignoring the unlikely overflow.
        services.emplace(Wrapper::server_add_service
        (
                argument.service_name,
                argument.service_description,
                argument.buffer,
                buffer_size,
                argument.user_routine,
                tag
        ));
#endif
    }
    else
    {
        trap("Unable to publish DIM service \"" + argument.service_name + "\": Buffer"
                " size overflow.");
    }
}

void Controller::publish_command(CommandPublication& argument)
{
#ifndef EXCLUDE_DIM
    command_callback_t user_routine(argument.user_routine);
    commands.emplace(Wrapper::server_add_command
    (
            argument.service_name,
            argument.service_description,
            user_routine,
            argument.tag
    ));
#endif
}

void Controller::update_service(ServiceID& argument)
{
    if (services.find(argument.service_id) != services.end())
    {
#ifndef EXCLUDE_DIM
    Wrapper::server_update_service(argument.service_id);
#endif
    }
    else
    {
        trap
        (
                "Unable to update DIM service with ID " +
                std::to_string(argument.service_id) + ": The service doesn't "
                "exist."
        );
    }
}

void Controller::subscribe_to_service(ServiceSubscription& argument)
{
    int buffer_size((int) argument.buffer_size);
    int error_object_size(argument.error_object_size);
    if (buffer_size >= 0 && error_object_size >= 0)
    {
#ifndef EXCLUDE_DIM
        int subscription_type((int) argument.subscription_type);
        int interval((int) argument.interval);
        command_callback_t user_routine(argument.user_routine);
        void* error_object(const_cast<void*>(argument.error_object));
        dim_long tag((dim_long) argument.tag);
        subscriptions.emplace(Wrapper::client_subscribe
        (
                argument.service_name,
                subscription_type,
                interval,
                argument.buffer,
                buffer_size,
                user_routine,
                tag,
                error_object,
                error_object_size
        ));
#endif
    }
    else
    {
        trap("Unable to subscribe to DIM service \"" + argument.service_name +
                "\": Buffer/error object size overflow.");
    }
}

void Controller::call_command(CommandInvocation& argument)
{
    int buffer_size((int) argument.buffer_size);
    if (buffer_size >= 0)
    {
#ifndef EXCLUDE_DIM
        Wrapper::client_call_command
        (
                argument.service_name,
                argument.buffer,
                buffer_size
        );
#endif
    }
    else
    {
        trap("Unable to call DIM command \"" + argument.service_name + "\": "
                "Buffer size overflow.");
    }
}

void Controller::overwrite_timestamp(TimestampRequest& argument)
{
    if (subscriptions.find(argument.service_id) != subscriptions.end())
    {
#ifndef EXCLUDE_DIM
        Wrapper::client_write_timestamp
        (
                argument.service_id,
                argument.milliseconds_ptr,
                argument.seconds_ptr
        );
#endif
    }
    else
    {
        trap
        (
                "Unable to retrieve timestamp for DIM service with ID " +
                std::to_string(argument.service_id) + ": The service doesn't "
                "exist."
        );
    }
}

void Controller::handle_exception(const char* message) noexcept
{
    Thread::handle_exception(message);
    finish();
}
