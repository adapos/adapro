#include <cstdio>
#include <deque>
#include <list>
#include <map>
#include <set>
#include <utility>
#include <regex>
#include <memory>
#include <string>
#include <fstream>
#include <regex>
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/ADAPOS/DPIDUtils.hpp"
#include "../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../headers/ADAPOS/DeliveryType.hpp"
#include "../../headers/ADAPOS/Typedefs.hpp"
#include "../../headers/library/StringUtils.hpp"
#include "../../headers/library/ConfigurationUtils.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::ADAPOS;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

void ADAPRO::ADAPOS::import_dpids
(
        Logger& logger,
        registry_t& dpids,
        const list<string>& filenames
)
{
    config_t service_map;
    import_configuration
    (
            logger,
            service_map,
            filenames,
            true,
            REGEX_DIM_SERVICE_NAME,
            [&logger](const string& k, const string& v){
                if (!regex_match(v, REGEX_PT))
                {
                    logger.print("Service \"" + k + "\" has an illegal type "
                            "\"" + v + "\".", FATAL);
                    return false;
                }
                return true;
            }
    );
    for (pair<string, string> service : service_map)
    {
        const DPID dpid;
        DataPointIdentifier::FILL(dpid, service.first,
                ADAPRO::Library::read<DeliveryType>(service.second));
        dpids.push_back(dpid);
    }
}

void ADAPRO::ADAPOS::export_dpids(const registry_t& dpids,
        const string& service_file_name)
{
    config_t service_map;
    for (const DPID& dpid: dpids)
    {
        service_map[string(dpid.get_alias())] = show(dpid.get_type());
    }
    export_configuration(service_map, service_file_name, REGEX_DIM_SERVICE_NAME, true);
}