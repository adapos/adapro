
#include <cstdint>
#include <vector>
#include <deque>
#include <map>
#include <string>
#include <mutex>
#include <iostream>
#include <atomic>
#include <chrono>
#include <thread>
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Supervisor.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/control/AlignedAllocator.hpp"
#include "../../headers/ADAPOS/PipelineProcessor.hpp"
#include "../../headers/ADAPOS/Producer.hpp"
#include "../../headers/ADAPOS/DataPointCompositeObject.hpp"
#include "../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../headers/ADAPOS/DataPointValue.hpp"
#include "../../headers/ADAPOS/DeliveryType.hpp"
#include "../../headers/ADAPOS/DataPointEventRecord.hpp"
#include "../../headers/ADAPOS/Typedefs.hpp"
#include "../../headers/library/Clock.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::ADAPOS;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

const uint64_t TERMINATOR_DATA[7]{0, 0, 0, 0, 0, 0, 0};

const DPVAL TERMINATOR
(
        DPVAL::DIRTY_FLAG | DPVAL::END_FLAG,
        0,
        0,
        TERMINATOR_DATA,
        VOID
);

Producer::Producer
(
        Logger& logger,
        buffer_t buffer,
        const size_t buffer_size,
        registry_t& registry,
        Supervisor& supervisor,
        const string&& name,
        const config_t& configuration,
        const int preferred_core,
        const trans_cb_t transition_callback
)
noexcept:
        PipelineProcessor
        (
                logger,
                buffer,
                buffer_size,
                registry,
                supervisor,
                std::move(name),
                configuration,
                preferred_core,
                transition_callback
        )
{}

uint16_t Producer::check_for_overflow(const DPVAL& dpval) noexcept
{
    if (dpval.flags & DPVAL::DIRTY_FLAG)
    {
        const uint64_t t(ADAPRO::Library::epoch_time());
        if (t - latest_overflow_ts >= 60000)
        {
            print("Ring buffer is overflowing!", ERROR);
            latest_overflow_ts = t;
        }
        return DPVAL::OVERWRITE_FLAG;
    }
    return 0;
}

void Producer::produce(const DPID& dpid, const DPVAL& dpval) noexcept
{
    const size_t dpid_index(registry.size());
    registry.push_back(dpid);
    DPER& dper(buffer[current_index]);

    unique_lock<mutex> l(dper.mutex_object);
    while (dper.data.flags & DPVAL::DIRTY_FLAG)
    {
        l.unlock();
        Thread::SPINLOCK(chrono::nanoseconds(1000));
        l.lock();
    }
    dper.previous_dpid_index = dper.current_dpid_index;
    dper.current_dpid_index = dpid_index;
    dper.data.set
    (
            (dpval.get_flags() & ~DPVAL::CONTROL_MASK) | DPVAL::DIRTY_FLAG |
                    DPVAL::NEW_FLAG | check_for_overflow(dper.data),
            dpval.msec,
            dpval.sec,
            &dpval.payload_pt1,
            dpid.get_type()
    );
    l.unlock();

    current_index = (current_index + 1) % buffer_size;
}

void Producer::update(const size_t dpid_index, const DPVAL& dpval) noexcept
{
    DPER& dper(buffer[current_index]);

    unique_lock<mutex> l(dper.mutex_object);
    dper.previous_dpid_index = dper.current_dpid_index;
    dper.current_dpid_index = dpid_index;
    dper.data.set
    (
            (dpval.get_flags() & ~DPVAL::CONTROL_MASK) | DPVAL::DIRTY_FLAG |
                    check_for_overflow(dper.data),
            dpval.msec,
            dpval.sec,
            &dpval.payload_pt1,
            registry[dpid_index].get_type()
    );
    l.unlock();

    current_index = (current_index + 1) % buffer_size;
}

void Producer::finish()
{
    for (current_index = 0; current_index < buffer_size; ++current_index)
    {
        DPER& dper(buffer[current_index]);
        unique_lock<mutex> l(dper.mutex_object);
        dper.data = TERMINATOR;
    }
}

void Producer::handle_exception(const char* message) noexcept
{
    Thread::handle_exception(message);
    finish();
}
