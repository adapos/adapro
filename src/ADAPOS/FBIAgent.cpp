
#include <cstdint>
#include <cmath>
#include <string>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <map>
#include <deque>
#include <vector>
#include <functional>
#include <algorithm>
#include <memory>
#include <thread>
#include <chrono>
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/control/Supervisor.hpp"
#include "../../headers/control/Metronome.hpp"
#include "../../headers/ADAPOS/FBIAgent.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/control/AlignedAllocator.hpp"
#include "../../headers/library/MetronomeFactory.hpp"
#include "../../headers/ADAPOS/DataPointCompositeObject.hpp"
#include "../../headers/ADAPOS/DataPointValue.hpp"
#include "../../headers/data/Command.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::ADAPOS;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

FBIAgent::FBIAgent
(
        Logger& logger,
        const uint32_t tick_length,
        const image_copy_t::size_type suggested_copy_size,
        Supervisor& supervisor,
        const string&& name,
        const config_t& configuration,
        const int preferred_core,
        const trans_cb_t transition_callback
)
noexcept:
        Worker
        (
                logger,
                supervisor,
                std::move(name),
                configuration,
                preferred_core,
                transition_callback
        ),
        should_consume(false),
        metronome(get_metronome(logger, tick_length)),
        fbi(),
        fbi_copy(),
        active(tick_length > 0)
{
    const size_t fbi_size(fbi.size());
    if (suggested_copy_size > 0)
    {
        if (suggested_copy_size < fbi_size)
        {
            print("Ignoring FBI copy size suggestion, which was between 0 and "
                    "size of FBI (i.e. too small).", WARNING);
        }
        else
        {
            fbi_copy.reserve(suggested_copy_size);
            return;
        }
    }
    fbi_copy.reserve((size_t) (fbi_size * 1.10));
}

// I should probably remove this procedure:
void FBIAgent::copy_fbi(image_copy_t& storage) noexcept
{
    const image_t::size_type fbi_size(fbi.size());
    storage.reserve(storage.size() + fbi_size);
    for (image_t::size_type i(0); i < fbi_size; ++i)
    {
        DPCOM& dpcom(fbi[i]);
        {
            std::lock_guard<std::mutex> lock(fbiAccessMutex);
            storage.push_back(dpcom);
        }
        storage[i].data.flags &= ~DPVAL::CONTROL_MASK;
    }
}

void FBIAgent::read_fbi(image_copy_t& storage) noexcept
{
    const size_t fbi_size(fbi.size());
    const size_t storage_size(storage.size());
    const size_t n(min(fbi_size, storage_size));
    storage.reserve(fbi_size);
    size_t i(0);
    while (i < n)
    {
        DPCOM& dpcom(fbi[i]);
        {
            std::lock_guard<std::mutex> lock(fbiAccessMutex);
            storage[i++].update(dpcom.data);
        }
    }
    while (i < fbi_size)
    {
        DPCOM& dpcom(fbi[i]);
        {
            std::lock_guard<std::mutex> lock(fbiAccessMutex);
            storage.push_back(dpcom);
        }
        storage[i++].data.flags &= ~DPVAL::CONTROL_MASK;
    }
}

void FBIAgent::execute()
{
    if (active)
    {
        metronome.synchronize();
    }
    else
    {
        if (!should_consume.load(memory_order_consume))
        {
            this_thread::sleep_for(chrono::milliseconds(10));
            return;
        }
        should_consume.store(false, memory_order_release);
    }
    if (get_command() == CONTINUE)
    {
        {
            std::lock_guard<std::mutex> lock(fbiOperationMutex);
            read_fbi(fbi_copy);
            consume_fbi(fbi_copy);
        }
        send_fbi();
    }
}
