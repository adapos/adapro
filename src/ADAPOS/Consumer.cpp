
#include <vector>
#include <deque>
#include <map>
#include <string>
#include <mutex>
#include <atomic>
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Supervisor.hpp"
#include "../../headers/ADAPOS/PipelineProcessor.hpp"
#include "../../headers/ADAPOS/Consumer.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/control/AlignedAllocator.hpp"
#include "../../headers/ADAPOS/DataPointCompositeObject.hpp"
#include "../../headers/ADAPOS/DataPointIdentifier.hpp"
#include "../../headers/ADAPOS/DataPointValue.hpp"
#include "../../headers/ADAPOS/DataPointEventRecord.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::ADAPOS;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

Consumer::Consumer
(
        Logger& logger,
        buffer_t buffer,
        const size_t buffer_size,
        registry_t& registry,
        Supervisor& supervisor,
        const string&& name,
        const config_t& configuration,
        const int preferred_core,
        const trans_cb_t transition_callback
)
noexcept:
        PipelineProcessor
        (
                logger,
                buffer,
                buffer_size,
                registry,
                supervisor,
                std::move(name),
                configuration,
                preferred_core,
                transition_callback
        )
{}

void Consumer::execute()
{
    current_index = 0;
    while (get_command() == CONTINUE && current_index < buffer_size)
    {
        DPER& dper(buffer[current_index]);
        unique_lock<mutex> l(dper.mutex_object);
        if ((dper.data.flags & DPVAL::DIRTY_FLAG) > 0)
        {
            dper.data.flags &= (~DPVAL::CONTROL_MASK) ^ DPVAL::NEW_FLAG;
            consume(dper.current_dpid_index, dper.data);
            dper.data.flags &= ~DPVAL::NEW_FLAG;
            ++current_index;
        }
    }
}

void Consumer::handle_exception(const char* message) noexcept
{
    Thread::handle_exception(message);
    finish();
}
