
#include <string>
#include <regex>
#include <memory>
#include <iostream>
#include <fstream>
#include <map>
#include <functional>
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/library/ConfigurationUtils.hpp"
#include "../../headers/library/StringUtils.hpp"
#include "../../headers/data/LoggingLevel.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

void ADAPRO::Library::import_configuration
(
        Logger& logger,
        config_t& key_value_map,
        const list<string>& filenames,
        const bool allow_new_keys,
        const regex& key_predicate,
        const function<bool(const string&, const string&)>& relation
)
{
    for (const string& filename : filenames)
    {
        ifstream file(filename);
        if (file.is_open())
        {
            logger.print
            (
                    "File \"" + filename +  "\" opened succesfully.",
                    ADAPRO_DEBUG
            );
            map<string, size_t> first_definitions;
            string error_message;
            string line;
            size_t line_number(0), error_number(0);
            while (getline(file, line))
            {
                ++line_number;
                unique_ptr<vector<string>> tokens(split_by_whitespace(line));
                if (tokens->size() >= 1)
                {
                    const string& key = (*tokens)[0];
                    if (key[0] != '#')
                    {
                        switch (tokens->size())
                        {
                            case 1:
                                error_message += "Error " +
                                        to_string(++error_number) + ":\n    Line " +
                                        to_string(line_number) + " in file \"" +
                                        filename + "\" is missing the value after"
                                        " key \"" + key + "\".\n";
                                continue;
                            case 2: break;
                            default:
                                if ((*tokens)[2][0] != '#')
                                {
                                    error_message += "Error " +
                                            to_string(++error_number) +
                                            ":\n    Line " +
                                            to_string(line_number) + " in file \"" +
                                            filename + "\" contains too many tokens"
                                            " (e.g. \"" + (*tokens)[2] + "\").\n";
                                    continue;
                                }
                        }
                        if (first_definitions[key] != 0)
                        {
                            error_message += "Error " + to_string(++error_number) +
                                    ":\n    Line " + to_string(line_number) +
                                    " in file \"" + filename + "\" contains a"
                                    " redefinition for the key \"" + key + "\""
                                    " (first defined in line " +
                                    to_string(first_definitions[key]) + ").\n";
                            continue;
                        }
                        if (!allow_new_keys && key_value_map[key] == "")
                        {
                            error_message += "Error " + to_string(++error_number) +
                                    ":\n    Line " + to_string(line_number) +
                                    " in file \"" + filename + "\" contains an"
                                    " unknown key \"" + key + "\".\n";
                        }
                        const string& value = (*tokens)[1];
                        if (regex_match(key, key_predicate))
                        {
                            if (relation(key, value))
                            {
                                key_value_map[key]      = value;
                                first_definitions[key]  = line_number;
                            }
                            else
                            {
                                error_message += "Error " +
                                        to_string(++error_number) + ":\n    Line " +
                                        to_string(line_number) + " in file \"" +
                                        filename + "\" failed to satisfy the "
                                        "relation provided by the application.\n";
                            }
                        }
                        else
                        {
                            error_message += "Error " + to_string(++error_number) +
                                    ":\n    Line " + to_string(line_number) +
                                    " in file \"" + filename + "\" contains a"
                                    " malformed key \"" + key + "\".\n";
                        }
                    }
                }
            }
            if (error_message != "")
            {
                throw domain_error(error_message.c_str());
            }
            else
            {
                return;
            }
        }
        logger.print("Failed to access file \"" + filename + "\".", WARNING);
    }
    throw ios_base::failure("Cannot access file any of the given files.");
}

void ADAPRO::Library::print_configuration
(
        Logger& logger,
        const string& title,
        const config_t& key_value_map
)
noexcept
{
    map<string,string> tmp(key_value_map);
    list<pair<string, string>> pairs;
    for (map<string, string>::iterator it = tmp.begin(); it != tmp.end(); ++it)
    {
        pairs.push_back(pair<string, string>(it->first, it->second));
    }
    print_k_v_list(logger, title, pairs);
}

void ADAPRO::Library::export_configuration
(
        const config_t& key_value_map,
        const string& filename,
        const regex& key_predicate,
        const bool tabulate
)
{
    ofstream file(filename);
    size_t largest_key_length = 0;
    for (const pair<string, string>& k_v : key_value_map)
    {
        if (regex_match(k_v.first, key_predicate))
        {
            if (regex_match(k_v.second, REGEX_WHITESPACE))
            {
                throw invalid_argument("Value of \"" + k_v.first + "\", \"" +
                        k_v.second + "\", contains whitespace characters.");
            }
            else if (k_v.first.length() > largest_key_length)
            {
                largest_key_length = k_v.first.length();
            }
        }
        else
        {
            throw invalid_argument("Malformed key \"" + k_v.first + "\".");
        }
    }
    for (const pair<string, string>& k_v : key_value_map)
    {
        file << k_v.first << string(
                tabulate ? (largest_key_length + 1) - k_v.first.length() : 1,
                ' ') << k_v.second << endl;
    }
}