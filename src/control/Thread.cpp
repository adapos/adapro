
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <string>
#include <functional>
#include <memory>
#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <list>
#include <vector>
#include <stdexcept>
#ifdef _GNU_SOURCE
#include <pthread.h>
#include <unistd.h>
#include <sched.h>
#include <signal.h>
#endif
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/library/StringUtils.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/Command.hpp"
#include "../../headers/data/Typedefs.hpp"

/**
 * The purpose of this macro is to call <tt>handle_exception</tt> with a proper
 * error message, bypassing problems that are caused by object slicing when
 * exploiting polymorphism of C++ exception classes.
 */
#define META_HANDLER(x) \
    set_state(ABORTING);\
    print("Aborting...", ADAPRO_DEBUG);\
    transition_callback(ABORTING);\
    handle_exception(x.what());\
    set_state(ABORTED);\
    print("Aborted.", ADAPRO_DEBUG);

using namespace ADAPRO::Control;
using namespace ADAPRO::Data;

const chrono::milliseconds DELAY(10);

const size_t Thread::CORE_COUNT(thread::hardware_concurrency());

const function<void(const State)> Thread::NOP_CB = [](const State s){};

ADAPRO::Data::trans_cb_t Thread::STATE_PROPAGATION_CB(list<Thread*>&& subordinates) noexcept
{
    return [subordinates](const ADAPRO::Data::State s)
    {
        for (Thread* subordinate : subordinates)
        {
            switch (s)
            {
                case STARTING:
                    subordinate->start(true);
                    break;
                case RUNNING:
                    if (subordinate->get_state() == PAUSED)
                    {
                        subordinate->resume(true);
                    }
                    break;
                case PAUSED:
                    subordinate->pause(true);
                    break;
                case STOPPING:
                case ABORTING:
                    subordinate->stop(true);
                default:;
            }
        }
    };
}

void Thread::SET_AFFINITY(const int affinity)
{
#ifdef _GNU_SOURCE
    pthread_t worker(pthread_self());
    if (affinity >= 0)
    {
        if ((size_t) affinity < CORE_COUNT)
        {
            cpu_set_t cpu_set;
            CPU_ZERO(&cpu_set);
            CPU_SET(affinity, &cpu_set);
            int code = pthread_setaffinity_np
            (
                    worker,
                    sizeof(cpu_set_t),
                    &cpu_set
            );
            if (code != 0)
            {
                throw std::runtime_error
                (
                        "Couldn't set affinity; error code " + to_string(code) +
                        "."
                );
            }
            else
            {
                Session::PRINT
                (
                        "Affinity set to core " + to_string(affinity) + ".",
                        ADAPRO_DEBUG
                );
            }
#ifdef _POSIX_PRIORITY_SCHEDULING
            const struct sched_param sp
            {
                sched_get_priority_max(SCHED_FIFO)
            };
            code = pthread_setschedparam(worker, SCHED_FIFO, &sp);
            switch (code)
            {
                case 0:
                    break;
                case EPERM:
                    throw std::runtime_error
                    (
                            "Couldn't set scheduling parameters (EPERM).\n    "
                            "Try adding the capability CAP_SYS_NICE to the "
                            "executable, or rerunning with sudo."
                    );
                default:
                    throw std::runtime_error
                    (
                            "Couldn't set scheduling parameters; error code " +
                            to_string(code) + "."
                    );
            }
#endif /* _POSIX_PRIORITY_SCHEDULING */
        }
        else
        {
            throw std::runtime_error
            (
                    "Cannot set affinity to CPU core number (" +
                    to_string(affinity) + "), which is greater than the number "
                    "of cores available (" + to_string(CORE_COUNT) + ")."
            );
        }
    }
#endif /* _GNU_SOURCE */
}

Thread::Thread
(
        Logger& logger,
        const string&& name,
        const int preferred_core
)
noexcept:
        logger(logger),
        state_mutex(),
        semaphore(),
        state(READY),
        command(CONTINUE),
        transition_callback([](const State s){}),
        responsive(false),
        joined(false),
        worker_ptr(),
        error_msg(nullptr),
        name(std::move(name)),
        preferred_core(preferred_core) {}

Thread::Thread
(
        Logger& logger,
        const string&& name,
        const trans_cb_t transition_callback,
        const int preferred_core
)
noexcept:
        logger(logger),
        state_mutex(),
        semaphore(),
        state(READY),
        command(CONTINUE),
        transition_callback(transition_callback),
        responsive(false),
        joined(false),
        worker_ptr(),
        error_msg(nullptr),
        name(std::move(name)),
        preferred_core(preferred_core) {}

void Thread::run() noexcept
{
#ifdef _GNU_SOURCE
    pthread_t worker(pthread_self());
    try
    {
        SET_AFFINITY(preferred_core);
    }
    catch (const std::runtime_error& e)
    {
        print(e.what(), WARNING);
    }
    char worker_name[16];
    if (name.length() >= 16)
    {
        strncpy(worker_name, name.c_str(), 11);
        worker_name[11] = '.';
        worker_name[12] = '.';
        worker_name[13] = '.';
        worker_name[14] = name[name.length() - 1];
        worker_name[15] = '\0';
    }
    else
    {
        strncpy(worker_name, name.c_str(), name.length() + 1);
    }
    pthread_setname_np(worker, worker_name);
#endif /* _GNU_SOURCE */

    try
    {
        // Startup:
        set_command(START);
        set_state(STARTING);
        print("Starting...", ADAPRO_DEBUG);
        transition_callback(STARTING);
        set_command(CONTINUE);
        prepare();
        Command first_command = get_command();
        if (first_command == CONTINUE)
        {
            set_state(RUNNING);
            print("Started.", ADAPRO_DEBUG);
            transition_callback(RUNNING);
        }
        else if (first_command == PAUSE)
        {
            set_state(PAUSED);
            print("Started.", ADAPRO_DEBUG);
            transition_callback(PAUSED);
        }

        // Execution:
        while (true)
        {
            responsive.store(true, memory_order_release);
            switch (get_command())
            {
                case CONTINUE:
                    if (get_state() == PAUSED)
                    {
                        set_state(RUNNING);
                        print("Resuming.", ADAPRO_DEBUG);
                        transition_callback(RUNNING);
                    }
                    execute();
                    break;
                case PAUSE:
                    if (get_state() == RUNNING)
                    {
                        set_state(PAUSED);
                        print("Paused.", ADAPRO_DEBUG);
                        transition_callback(PAUSED);
                    }
                    this_thread::sleep_for(DELAY);
                    break;
                default:
                    goto shutdown;
            }
        }

        // Shutdown:
        shutdown: if (get_command() == ABORT)
        {
            throw runtime_error(error_msg ? error_msg : "");
        }
        set_state(STOPPING);
        print("Stopping...", ADAPRO_DEBUG);
        transition_callback(STOPPING);
        finish();
        set_state(STOPPED);
        print("Stopped.", ADAPRO_DEBUG);
        transition_callback(STOPPED);
    }
    catch (const ios_base::failure& e)     {META_HANDLER(e);}
    catch (const system_error& e)          {META_HANDLER(e);}
    catch (const regex_error& e)           {META_HANDLER(e);}
    catch (const underflow_error& e)       {META_HANDLER(e);}
    catch (const overflow_error& e)        {META_HANDLER(e);}
    catch (const range_error& e)           {META_HANDLER(e);}
    catch (const runtime_error& e)         {META_HANDLER(e);}
    catch (const out_of_range& e)          {META_HANDLER(e);}
    catch (const length_error& e)          {META_HANDLER(e);}
    catch (const domain_error& e)          {META_HANDLER(e);}
    catch (const invalid_argument& e)      {META_HANDLER(e);}
    catch (const logic_error& e)           {META_HANDLER(e);}
    catch (const bad_weak_ptr& e)          {META_HANDLER(e);}
    catch (const bad_typeid& e)            {META_HANDLER(e);}
    catch (const bad_function_call& e)     {META_HANDLER(e);}
    catch (const bad_exception& e)         {META_HANDLER(e);}
    catch (const bad_cast& e)              {META_HANDLER(e);}
    catch (const bad_array_new_length& e)  {META_HANDLER(e);}
    catch (const bad_alloc& e)             {META_HANDLER(e);}
    catch (const exception& e)             {META_HANDLER(e);}
    catch (...)
    {
        // This is a really bad situation.
        set_state(ABORTING);
        transition_callback(ABORTING);
        print("Aborting.", ADAPRO_DEBUG);
        print("Cannot call exception handler!!!", FATAL);
        set_state(ABORTED);
        transition_callback(ABORTED);
        print("Aborted.", ADAPRO_DEBUG);
    }
}

// TODO: Non-fatal errors should be unit tested also.
void Thread::start(const bool wait) noexcept
{
    if (get_state() != READY || get_command() == START)
    {
        // Non-fatal error:
        print("Multiple START calls detected.", WARNING);
        return;
    }
    worker_ptr = make_unique<thread>(&Thread::run, this);
    worker_ptr->detach();
    if (wait)
    {
        wait_for_state(RUNNING);
    }
}

void Thread::pause(const bool wait) noexcept
{
    if (state_in(STARTING | RUNNING))
    {
        set_command(PAUSE);
    }
    else
    {
        print("Thread must be starting or running before pausing.", WARNING);
    }
    if (wait)
    {
        wait_for_state(PAUSED);
    }
}

void Thread::resume(const bool wait) noexcept
{
    if (get_state() == PAUSED)
    {
        set_command(CONTINUE);
    }
    else
    {
        print("Thread must be paused before resuming.", WARNING);
    }
    if (wait)
    {
        wait_for_state(RUNNING);
    }
}

void Thread::stop(const bool join) noexcept
{
    if (get_state() > READY)
    {
        wait_for_state(RUNNING);
        if (command_in(ABORT | STOP) || get_state() >= STOPPING)
        {
            print("Multiple stop/abort calls detected.", WARNING);
        }
        else
        {
            set_command(STOP);
        }
        if (join)
        {
            wait_for_state_mask(STOPPED | ABORTED);
        }
    }
    else
    {
        print("stop was called before start.", WARNING);
        return;
    }
}

void Thread::wait_for_state(const State target_state) noexcept
{
    if (get_state() < target_state)
    {
        unique_lock<mutex> lock(state_mutex);
        do {semaphore.wait_for(lock, DELAY);}
        while (get_state() < target_state);
        lock.unlock();
    }
    if (target_state == STOPPED || target_state == ABORTED)
    {
        handle_join();
    }
}

void Thread::wait_for_state(const State target_state,
        const chrono::milliseconds timeout)
{
    if (get_state() < target_state)
    {
        unique_lock<mutex> lock(state_mutex);
        semaphore.wait_for
        (
                lock,
                timeout,
                [this, target_state](){return get_state() >= target_state;}
        );
        lock.unlock();
        if (get_state() < target_state)
        {
            throw runtime_error("State transition timed out.");
        }
    }
}

void Thread::wait_for_state_mask(const uint8_t state_mask) noexcept
{
    if (!state_in(state_mask))
    {
        unique_lock<mutex> lock(state_mutex);
        do {semaphore.wait_for(lock, DELAY);}
        while (!state_in(state_mask));
        lock.unlock();
    }
    if (state_mask & (STOPPED | ABORTED))
    {
        handle_join();
    }
}

void Thread::wait_for_state_mask(const uint8_t state_mask,
        const chrono::milliseconds timeout)
{
    if (!state_in(state_mask))
    {
        unique_lock<mutex> lock(state_mutex);
        semaphore.wait_for
        (
                lock,
                timeout,
                [this, state_mask](){return state_in(state_mask);}
        );
        lock.unlock();
        if (!state_in(state_mask))
        {
            throw runtime_error("State transition timed out.");
        }
    }
}

void Thread::abort(const char* message) noexcept
{
    error_msg = const_cast<char*>(message);
    set_command(ABORT);
}

void Thread::abort(const string& message) noexcept
{
    const size_t size(message.length());
    error_msg = new char[size];
    memcpy(error_msg, message.c_str(), size + 1);
    set_command(ABORT);
}

void Thread::trap(const char* message)
{
    error_msg = const_cast<char*>(message);
    throw std::runtime_error(message);
}

void Thread::trap(const string& message)
{
    const size_t size(message.length());
    error_msg = new char[size];
    memcpy(error_msg, message.c_str(), size);
    throw std::runtime_error(message);
}

void Thread::handle_exception(const char* message) noexcept
{
    print
    (
            (string) "Unhandled exception:\n    " +
                    (error_msg == nullptr ? message : error_msg),
            FATAL
    );
}

void Thread::handle_join() noexcept
{
    if (!(joined.load(memory_order_consume)))
    {
        if (worker_ptr != nullptr && worker_ptr->joinable())
        {
            worker_ptr->join();
        }
        joined.store(true, memory_order_release);
    }
}

Thread::~Thread()
{
    handle_join();
}
