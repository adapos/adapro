
#include <csignal>
#include <string>
#include <map>
#include <utility>
#include <iostream>
#include <sstream>
#include <memory>
#include <atomic>
#include <list>
#include <thread>
#include <chrono>
#include <mutex>
#include <pthread.h>
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Worker.hpp"
#include "../../headers/control/Supervisor.hpp"
#ifndef EXCLUDE_DIM
#include "../../headers/DIM/Controller.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/data/Typedefs.hpp"
#endif
#include "../../headers/data/State.hpp"
#include "../../headers/data/Command.hpp"
#include "../../headers/data/StatusFlags.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/Typedefs.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

void Supervisor::send_command
(
        unique_ptr<Worker>& worker_ptr,
        const ADAPRO::Data::Command command,
        const bool wait
)
noexcept
{
    const State worker_state = worker_ptr->get_state();
    switch(command)
    {
        case START:
            if (worker_state == READY)
            {
                worker_ptr->start(wait);
            }
            else
            {
                print("Didn't start \"" + worker_ptr->name + "\", that was "
                        "already " + show(worker_state) + ".", WARNING);
            }
            break;
        case PAUSE:
            if (worker_state == RUNNING)
            {
                worker_ptr->pause(wait);
            }
            else
            {
                print("Didn't pause \"" + worker_ptr->name + "\", that was "
                        "already " + show(worker_state) + ".", WARNING);
            }
            break;
        case CONTINUE:
            if (worker_state == PAUSED)
            {
                worker_ptr->resume(wait);
            }
            else if (started)
            {
                print("Didn't resume \"" + worker_ptr->name + "\", that was "
                        "already " + show(worker_state) + ".", WARNING);
            }
            break;
        case STOP:
        case ABORT:
        default:
            if (worker_state < STOPPING)
            {
                worker_ptr->stop(wait);
            }
            else
            {
                print("Didn't stop \"" + worker_ptr->name + "\", that was "
                        "already " + show(worker_state) + ".", WARNING);
            }
    }
    if (command == CONTINUE && !started)
    {
        started = true;
    }
}

void Supervisor::stop_worker(unique_ptr<Worker>& worker_ptr) noexcept
{
    // These different kind of forceful shutdown mechanisms should be moved to
    // Thread probably...
    try
    {
        print("Waiting for " + worker_ptr->name + " to stop gracefully...",
                ADAPRO_DEBUG);
        worker_ptr->wait_for_state(STOPPED, chrono::seconds(10));
        print(worker_ptr->name + " stopped in time.", ADAPRO_DEBUG);
    }
    catch (const std::runtime_error& e)
    {
        print("Worker \"" + worker_ptr->name + "\" failed to stop in 10 "
                "seconds.", WARNING);
    }
}

void Supervisor::propagate_command_in_parallel
(
        const Command command,
        const State expected_state
)
noexcept
{
    unique_lock<mutex> lck(command_mutex);
    for (unique_ptr<Worker>& worker_ptr : workers)
    {
        send_command(worker_ptr, command, false);
    }
    if (command == STOP || command == ABORT)
    {
        for (unique_ptr<Worker>& worker_ptr : workers)
        {
            stop_worker(worker_ptr);
        }
    }
    else
    {
        for (unique_ptr<Worker>& worker_ptr : workers)
        {
            worker_ptr->wait_for_state(expected_state);
        }
    }
}

void Supervisor::propagate_command_in_lifo
(
        const Command command,
        const State expected_state
)
noexcept
{
    unique_lock<mutex> lck(command_mutex);
    if (command == START || command == CONTINUE)
    {
        for (unique_ptr<Worker>& worker_ptr : workers)
        {
            send_command(worker_ptr, command, true);
        }
    }
    else
    {
        for (auto iterator = workers.rbegin(); iterator != workers.rend(); ++iterator)
        {
            const bool stopping(command == STOP);
            send_command(*iterator, command, !stopping);
            if (stopping)
            {
                stop_worker(*iterator);
            }
        }
    }
}

Supervisor::Supervisor
(
        Logger& logger,
        uint32_t* state_ptr,
        const list<worker_factory_t>& worker_factories,
        const config_t& configuration
):
        Worker
        (
                logger,
                *this,
                std::move("Supervisor"),
                configuration,
                stoi(configuration.at(get<0>(SUPERVISOR_CORE))),
                [this, state_ptr](const State s)
                {
                    *state_ptr = (uint32_t) s;
                    switch (s)
                    {
                        case RUNNING:
                            Session::ENABLE_WATCHDOG();
                            propagate_command(CONTINUE, RUNNING);
                            break;
                        case PAUSED:
                            Session::DISABLE_WATCHDOG();
                            propagate_command(PAUSE, PAUSED);
                            break;
                        case ABORTING:
                            Session::DISABLE_WATCHDOG();
                            propagate_command(STOP, STOPPED);
                            break;
                        case STOPPING:
                            Session::DISABLE_WATCHDOG();
                            break;
                        default:;
                    }
                }
        ),
        started(false),
        all_workers_halted(false),
        some_workers_dead(false),
        worker_factories(worker_factories),
        workers(),
        command_mutex()
{}

void Supervisor::prepare()
{
    for (const worker_factory_t& factory: worker_factories)
    {
        workers.push_back(factory(logger, *this, configuration));
    }
    propagate_command(START, RUNNING);
}

void Supervisor::execute()
{
    Session::NOTIFY_WATCHDOG();
    all_workers_halted = true;
    for (unique_ptr<Worker>& worker_ptr : workers)
    {
        const State worker_state = worker_ptr->get_state();
        if (worker_state != STOPPED)
        {
            all_workers_halted = false;
        }
        if (worker_state == ABORTED)
        {
            some_workers_dead = true;
        }
        if (worker_state & (RUNNING | PAUSED))
        {
            Worker& worker(*worker_ptr);
            if (!worker.responsive.load(memory_order_consume))
            {
                if (++worker.missed_checkpoints % worker.missed_checkpoints_treshold == 0)
                {
                    print
                    (       worker.name + " has missed " +
                            to_string(worker.missed_checkpoints) +
                            " checkpoint" +
                            (worker.missed_checkpoints == 1 ? "" : "s") +
                            " so far.",
                            ADAPRO_DEBUG
                    );
                    worker.missed_checkpoints_treshold += 10;
                }

            }
            worker.responsive.store(false, memory_order_release);
        }
    }
    if (some_workers_dead)
    {
        if (Session::SESSION_PTR)
        {
            Session::SESSION_PTR->mask_status_code(FLAG_WORKER);
        }
        print("Stopping due to the death of one or more workers.", ADAPRO_DEBUG);
        print_report();
        // Gonna complain about multiple STOP/ABORT commands...
        this->stop(false);
    }
    else if (all_workers_halted)
    {
        print("Stopping because all workers have stopped.", ADAPRO_DEBUG);
        this->stop(false);
    }
    else
    {
        this_thread::sleep_for(chrono::seconds(1));
    }
}

void Supervisor::finish()
{
    if (!all_workers_halted)
    {
        propagate_command(STOP, STOPPED);
    }
    workers.clear();
}

void Supervisor::handle_exception(const char* message) noexcept
{
    Thread::handle_exception(message);
    print_report();
    finish();
}

void Supervisor::print_report() noexcept
{
    stringstream s;
    s << "Report:" << endl;
    for (unique_ptr<Worker>& worker_ptr : workers)
    {
        s << "    " << worker_ptr->report() << endl;
    }
    print(s.str(), INFO);
}
