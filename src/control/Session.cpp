
#include <cstdlib>
#include <cstdint>
#include <csignal>
#include <exception>
#include <fstream>
#include <iostream>
#include <functional>
#include <utility>
#include <string>
#include <memory>
#include <list>
#include <map>
#include <atomic>
#include <thread>
#include <chrono>
#ifdef _POSIX_C_SOURCE
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#endif
#ifdef __linux__
#include <systemd/sd-daemon.h>
#endif
#ifndef EXCLUDE_DIM
#include "/usr/include/dim/dim_common.h"
// A forward declaration:
namespace ADAPRO
{
namespace DIM
{
namespace Wrapper
{
    void common_set_dim_dns_node(const std::string&);
    void common_set_dim_dns_port(const uint16_t) noexcept;
    void server_add_error_handler(void(*)(int, int, char*)) noexcept;
    void client_add_error_handler(void(*)(int, int, char*)) noexcept;
    unsigned server_add_command
    (
            const std::string&,
            const std::string&,
            void (*)(void*, void*, int*),
            dim_long
    )
    noexcept;
    unsigned server_add_service
    (
            const std::string&,
            const std::string&,
            void*,
            int,
            void (*)(void*, void**, int*, int*),
            dim_long
    );
    void server_remove_service(const unsigned service_id);
    void server_start(const std::string&);
    void server_stop() noexcept;
}
}
}
using namespace ADAPRO::DIM;
#endif
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Supervisor.hpp"
#include "../../headers/control/Session.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/library/StringUtils.hpp"
#include "../../headers/library/ConfigurationUtils.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/data/Context.hpp"
#include "../../headers/data/Command.hpp"
#include "../../headers/data/Report.hpp"
#include "../../headers/data/Parameters.hpp"
#include "../../headers/data/StatusFlags.hpp"
#include "../../headers/data/LoggingLevel.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

Session* Session::SESSION_PTR;

uint32_t Session::SHUTDOWN_COUNTER = 0;

void Session::FORCE_SHUTDOWN(uint8_t code) noexcept
{
    if (++SHUTDOWN_COUNTER > 1)
    {
        Session::PRINT
        (
                "Shutdown already in progress. If this process doesn't halt "
                "in one minute, it needs to be terminated externally.",
                WARNING
        );
        return;
    }
    else if (SESSION_PTR != nullptr)
    {
        Session& session(*SESSION_PTR);
        session.mask_status_code(code);
        if (session.supervisor_ptr != nullptr)
        {
            atomic_bool waiting_for_supervisor(true);
            thread t([&session, &waiting_for_supervisor]() {
                Session::PRINT("Commencing abort sequence...", ADAPRO_DEBUG);
                for (size_t i = 0; i < 60 && waiting_for_supervisor.load(memory_order_consume); ++i)
                {
                    this_thread::sleep_for(chrono::seconds(1));
                }
                if (waiting_for_supervisor.load(memory_order_consume))
                {
                    Session::PRINT("Shutdown timed out!", WARNING);
                    session.mask_status_code(FLAG_TIMEOUT);
//                    session.context.exit_handler(session.get_status_code());
                    raise(SIGKILL);
                }
                else
                {
                    Session::PRINT("Supervisor stopped in time.", ADAPRO_DEBUG);
                }
            });
            t.detach();
            session.supervisor_ptr->stop(true);
            session.finish();
            waiting_for_supervisor.store(false, memory_order_release);
        }
        Session::PRINT("Abort sequence complete.", ADAPRO_DEBUG);
        session.context.exit_handler(session.get_status_code());
    }
    else
    {
        Session::PRINT("session_ptr is null.", ADAPRO_DEBUG);
        exit(code | FLAG_UNHANDLED_EXCEPTION);
    }
}

void Session::ADAPRO_TERMINATE_HANDLER() noexcept
{
    SET_PROCESS_STATE(ABORTING);
    exception_ptr eptr(current_exception());
    try
    {
        if (eptr) {std::rethrow_exception(eptr);}
        Session::PRINT("Unhandled exception. [Couldn't retrieve message.]",
                FATAL);
    }
    catch (const exception& e)
    {
        Session::PRINT("Unhandled exception:\n    " + string(e.what()), FATAL);
    }
    FORCE_SHUTDOWN(FLAG_UNHANDLED_EXCEPTION);
};

#ifdef _POSIX_C_SOURCE
void Session::ADAPRO_SIGNAL_HANDLER(int signal) noexcept
{
    SET_PROCESS_STATE(ABORTING);
    uint8_t code;
    function<void(void)> signal_callback = [](){};
    if (SESSION_PTR != nullptr)
    {
        Session & session(*SESSION_PTR);
        auto iterator(session.context.signal_callbacks.find(signal));
        if (iterator != session.context.signal_callbacks.end())
        {
            signal_callback = iterator->second;
        }
    }
    switch (signal)
    {
        case SIGSEGV:
            Session::PRINT("A segmentation violation occured.", FATAL);
            code = FLAG_SIGSEGV;
            break;
        case SIGABRT:
            Session::PRINT("An abort signal was caught.", FATAL);
            code = FLAG_SIGABRT;
            break;
        case SIGINT:
            Session::PRINT("An interrupt signal was caught.", WARNING);
            code = FLAG_SIGINT;
            break;
        case SIGHUP:
            Session::PRINT("A hang up signal was caught.", WARNING);
            code = FLAG_SIGHUP;
            break;
        default:        // default should be unreachable!
            Session::PRINT("Unsupported signal " + to_string(signal) +
                    " was caught.", FATAL);
            code = FLAG_UNHANDLED_EXCEPTION;
    }
    signal_callback();
    FORCE_SHUTDOWN(code);
}
#else
// ANSI C fallback section.
void Session::ADAPRO_SIGSEGV_HANDLER(int dummy) noexcept
{
    ADAPRO_SIGNAL_HANDLER(SIGSEGV);
}

void Session::ADAPRO_SIGABRT_HANDLER(int dummy) noexcept
{
    ADAPRO_SIGNAL_HANDLER(SIGABRT);
}

void Session::ADAPRO_SIGINT_HANDLER(int dummy) noexcept
{
    ADAPRO_SIGNAL_HANDLER(SIGINT);
}

void Session::ADAPRO_SIGHUP_HANDLER(int dummy) noexcept
{
    ADAPRO_SIGNAL_HANDLER(SIGHUP);
}
#endif

Session::Session(ADAPRO::Data::Context context) noexcept:
        sd_mutex(),
        context(context),
        configuration(context.initial_configuration),
        logger_ready(false),
        daemon_mode_started(false),
#ifdef _POSIX_C_SOURCE
        memory_locked(false),
#endif
        cleaned_up(false),
#ifndef EXCLUDE_DIM
        dim_server_started(false),
        dim_service_ids(),
#endif
        logger_ptr(make_shared<Logger>(ADAPRO::Library::logging_mask(
                context.initial_configuration.at(get<0>(LOGGING_MASK))))),
        supervisor_ptr(),
        supervisor_state(ADAPRO::Data::State::READY),
        status_code(0)
{
    SESSION_PTR = this;
    SET_PROCESS_STATE(READY);
}

void Session::register_handlers() noexcept
{
    set_terminate(ADAPRO_TERMINATE_HANDLER);
#ifdef _POSIX_C_SOURCE
    struct sigaction action;
    action.sa_handler   = ADAPRO_SIGNAL_HANDLER;
    action.sa_flags     = 0;
//    action.sa_mask      = SA_RESTART;
//    sigemptyset(&action.sa_mask);
    sigfillset(&action.sa_mask);
    sigaction(SIGSEGV, &action, nullptr);
    sigaction(SIGABRT, &action, nullptr);
    sigaction(SIGINT, &action, nullptr);
    sigaction(SIGHUP, &action, nullptr);
#else
    // ANSI C fallback:
    signal(SIGSEGV, ADAPRO_SIGSEGV_HANDLER);
    signal(SIGABRT, ADAPRO_SIGABRT_HANDLER);
    signal(SIGINT,  ADAPRO_SIGINT_HANDLER);
    signal(SIGHUP,  ADAPRO_SIGHUP_HANDLER);
#endif

    for (pair<uint8_t, function<void(void)>> handler: context.signal_callbacks)
    {
        switch(handler.first)
        {
            case SIGSEGV: case SIGABRT: case SIGINT: case SIGHUP:
                break;
            default:
                logger_ptr->print
                (
                        "Ignoring callback function for unsupported signal " +
                                std::to_string(handler.first) + ".",
                        WARNING
                );
        }
    }
}

void Session::update_configuration()
{
    list<string> paths(context.configuration_paths);
    if (context.argument_count > 1)
    {
        paths.push_front(string(context.argument_values[1]));
    }
    import_configuration
    (
            *logger_ptr,
            configuration,
            paths,
            context.allow_new_keys,
            REGEX_WORD,
            context.relation
    );
}

void Session::handle_daemon_mode()
{
    const uint8_t logging_mask(ADAPRO::Library::logging_mask(
            configuration.at(get<0>(LOGGING_MASK))));
    if (configuration.at(get<0>(DAEMON_ENABLED)) == "TRUE")
    {
        logger_ptr = make_shared<Logger>(
                logging_mask, configuration.at(get<0>(DAEMON_LOGFILE)));
        daemon_mode_started = true;
    }
    else if (logging_mask != ADAPRO::Library::logging_mask(
            context.initial_configuration.at(get<0>(LOGGING_MASK))))
    {
        logger_ptr = make_shared<Logger>(logging_mask);
    }
    logger_ready = true;
}

#if _POSIX_C_SOURCE >= 200112L
void Session::print_environment_report() noexcept
{
    char* host = new char[128];
    gethostname(host, 128);
    char* user(getenv("USER"));
    map<string, string> environment
    {
        {"Application", context.application_name},
        {"User", string(user == nullptr ? "[unknown]" : user)},
        {"Host", string(host)},
        {"PID", std::to_string(getpid())}
    };
    print_configuration(*logger_ptr, "ENVIRONMENT", environment);
    delete[] host;
}

void Session::handle_POSIX_parameters()
{
//#ifdef _XOPEN_SOURCE
    int nice_val(stoi(configuration.at(get<0>(NICE))));
    // Known issue: If nice is being set to -1, then failure and success are
    // indistinguishable
    if (nice(nice_val) != nice_val)
    {
        logger_ptr->print
        (
                "Couldn't set process nice value.\n    Try adding the "
                "capability CAP_SYS_NICE to the executable, or rerunning with "
                "sudo.",
                WARNING
        );
    }
    if (configuration.at(get<0>(LOCK_MEMORY)) == "TRUE")
    {
        memory_locked = mlockall(MCL_CURRENT | MCL_FUTURE) == 0;
        if (!memory_locked)
        {
            logger_ptr->print
            (
                    "Couldn't lock process virtual memory.\n    Try adding the "
                    "capability CAP_IPC_LOCK to the executable, or rerunning "
                    "with sudo.",
                    WARNING
            );
        }
    }
//#endif /* _XOPEN_SOURCE */
#ifdef _GNU_SOURCE
    try
    {
        Thread::SET_AFFINITY(stoi(configuration.at(get<0>(MAIN_CORE))));
    }
    catch (const std::runtime_error& e)
    {
        logger_ptr->print("<main> " + string(e.what()), WARNING);
    }
#endif /* _GNU_SOURCE */
}
#endif /* _POSIX_C_SOURCE */

#ifndef EXCLUDE_DIM
void Session::handle_DIM_server_mode()
{
    if (configuration.at(get<0>(DIM_SERVER_ENABLED)) == "TRUE")
    {
        dim_init();
        Wrapper::common_set_dim_dns_node
        (
                configuration.at(get<0>(DIM_DNS_NODE))
        );
        Wrapper::common_set_dim_dns_port
        (
                stoi(configuration.at(get<0>(DIM_DNS_PORT)))
        );
        Wrapper::server_add_error_handler(context.dim_client_error_handler);
        Wrapper::client_add_error_handler(context.dim_server_error_handler);
        dim_long tag(0);
#ifdef _GNU_SOURCE
        dtq_start_timer
        (
                1,
                [](void* tag_ptr)
                {
                    pthread_setname_np(pthread_self(), "DIM-CB-thread");
                    try
                    {
                        Thread::SET_AFFINITY
                        (
                                stoi(SESSION_PTR->configuration.at(
                                        get<0>(DIM_CALLBACK_CORE)))
                        );
                    }
                    catch (const std::runtime_error& e)
                    {
                        PRINT("<DIM-CB-thread> " + string(e.what()), WARNING);
                    }
                },
                (void*) &tag
        );
#endif /* _GNU_SOURCE */
        for (auto callback : context.dim_command_factories)
        {
            auto tuple(callback(configuration));
            dim_service_ids.push_back(Wrapper::server_add_command(
                    get<0>(tuple), get<1>(tuple), get<2>(tuple), tag++
            ));
        }
        dim_service_ids.push_back(Wrapper::server_add_service(
                configuration.at(get<0>(DIM_SERVER_NAME)) + "/STATE",
                "I:1",
                (void*) &supervisor_state,
                4,
                nullptr,
                tag++
        ));
        dim_service_ids.push_back(Wrapper::server_add_command(
                configuration.at(get<0>(DIM_SERVER_NAME)) + "/COMMAND",
                "I:1",
                [](void* tag, void* buffer, int* size)
                {
                    Session& session(*SESSION_PTR);
                    const Command command(*((Command*) buffer));
                    if (*size == 4)
                    {
                        switch (command)
                        {
                            case PAUSE:
                                session.logger_ptr->print("Executing PAUSE.",
                                        ADAPRO_DEBUG);
                                session.supervisor_ptr->pause(true);
                                SET_PROCESS_STATE(PAUSED);
                                return;
                            case CONTINUE:
                                session.logger_ptr->print("Executing CONTINUE.",
                                        ADAPRO_DEBUG);
                                session.supervisor_ptr->resume(true);
                                SET_PROCESS_STATE(RUNNING);
                                return;
                            case STOP:
                                session.logger_ptr->print("Executing STOP.",
                                        ADAPRO_DEBUG);
                                session.supervisor_ptr->stop(false);
                                return;
                            default:
                                session.logger_ptr->print("Ignoring command " +
                                        show(command) + ".", WARNING);
                                return;
                        }
                    }
                    session.logger_ptr->print("Ignoring unsupported command " +
                            std::to_string(*((int*) buffer)) + ".", WARNING);
                },
                tag++
        ));
        dim_service_ids.push_back(Wrapper::server_add_command(
                configuration.at(get<0>(DIM_SERVER_NAME)) + "/REPORT",
                "I:1",
                [](void* tag, void* buffer, int* size)
                {
                    Session& session(*SESSION_PTR);
                    const Report report(*((Report*) buffer));
                    if (*size == 4)
                    {
                        switch (report)
                        {
                            case THREADS:
                                session.supervisor_ptr->print_report();
                                return;
                            case CONFIGURATION:
                                print_configuration
                                (
                                        *session.logger_ptr,
                                        "CONFIGURATION",
                                        session.configuration
                                );
                                return;
                            case ENVIRONMENT:
                                session.print_environment_report();
                                return;
                            default:
                                session.logger_ptr->print("Ignoring report " +
                                        show(report) + ".", WARNING);
                                return;
                        }
                    }
                    session.logger_ptr->print("Ignoring unsupported report " +
                            std::to_string(*((int*) buffer)) + ".", WARNING);
                },
                tag++
        ));
    }
}
#endif /* EXCLUDE_DIM */

void Session::prepare()
{
    SET_PROCESS_STATE(STARTING);
    logger_ptr->print("Initializing ADAPRO Session...", SPECIAL);

    register_handlers();
    if (context.update_configuration)
    {
        update_configuration();
    }
    handle_daemon_mode();
    // At this point, ADAPRO is not going to change logger_ptr or configuration
    // anymore.

#if _POSIX_C_SOURCE >= 200112L
    handle_POSIX_parameters();
    print_environment_report();
#endif
    print_configuration(*logger_ptr, "CONFIGURATION", configuration);

#ifndef EXCLUDE_DIM
    handle_DIM_server_mode();
#endif
    supervisor_ptr = make_shared<Supervisor>
    (
            *logger_ptr,
            &supervisor_state,
            context.worker_factories,
            configuration
    );
    logger_ptr->print("ADAPRO Session initialized.", SPECIAL);
}

void Session::execute() noexcept
{
    supervisor_ptr->start(true);
    this_thread::sleep_for(chrono::milliseconds(10));
    if (supervisor_ptr->state_in(RUNNING | PAUSED))
    {
        logger_ptr->print("Supervisor started.", SPECIAL);
    }

#ifndef EXCLUDE_DIM
    // The at-operations can't throw anymore, since the existence of the
    // DIM configuration keys was ensured during prepare:
    if (configuration.at(get<0>(DIM_SERVER_ENABLED)) == "TRUE")
    {
        Wrapper::server_start(configuration.at(get<0>(DIM_SERVER_NAME)));
        dim_server_started = true;
    }
#endif

#ifdef __linux__
    if (daemon_mode_started)
    {
        notify_sd("READY=1");
        SET_PROCESS_STATE(RUNNING);
    }
#endif
    supervisor_ptr->wait_for_state(STOPPING);
#ifdef __linux__
    if (daemon_mode_started)
    {
        notify_sd("STOPPING=1");
        SET_PROCESS_STATE(get_status_code() == 0 ? STOPPING : ABORTING);
    }
#endif
    supervisor_ptr->wait_for_state_mask(STOPPED | ABORTED);
    logger_ptr->print("Supervisor halted.", SPECIAL);
}

void Session::finish() noexcept
{
    logger_ptr->print("Cleaning up ADAPRO Session...", SPECIAL);
    cleanup();
    SET_PROCESS_STATE(get_status_code() == 0 ? STOPPED : ABORTED);
    this_thread::sleep_for(chrono::milliseconds(10));
    logger_ptr->print("ADAPRO Session cleanup complete.", SPECIAL);
}

uint8_t Session::run() noexcept
{
    try
    {
        prepare();

        // If execution of the code makes it this far, then session and logger
        // pointers must be valid, as well as the framework configuration.
        execute();
    }
    catch (const std::logic_error& e)
    {
        SET_PROCESS_STATE(ABORTING);
        logger_ptr->print
        (
                "Exception during startup sequence: \n    " + string(e.what()),
                FATAL
        );
        mask_status_code(FLAG_BAD_CONFIG);
    }
    finish();
    return get_status_code();
}

void Session::cleanup() noexcept
{
    if (!cleaned_up)
    {
        // get_state segfaults here:
        if (supervisor_ptr != nullptr && supervisor_ptr->get_state() < STOPPING)
        {
            supervisor_ptr->stop(true);
        }

#ifndef EXCLUDE_DIM
        if (dim_server_started)
        {
            for (unsigned id : dim_service_ids)
            {
                Wrapper::server_remove_service(id);
            }
            Wrapper::server_stop();
        }
#endif
        SESSION_PTR = nullptr;

#if _POSIX_C_SOURCE >= 200112L
        if (memory_locked)
        {
            munlockall();
        }
#endif
        cleaned_up = true;
    }
}

Session::~Session() noexcept
{
    cleanup();
}
