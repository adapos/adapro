#include <string>
#include <mutex>
#include <iostream>
#include <ostream>
#include <fstream>
#ifdef _GNU_SOURCE
#include <sched.h>
#endif
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Logger.hpp"
#include "../../headers/library/Clock.hpp"
#include "../../headers/library/GenericFunctions.hpp"
#include "../../headers/data/State.hpp"
#include "../../headers/data/Command.hpp"
#include "../../headers/data/LoggingLevel.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Library;
using namespace ADAPRO::Data;

const string Logger::EMPTY_STRING;

#ifdef __linux__
#define INITIALIZE_COLORS \
        switch (severity) {\
            case ADAPRO::Data::LoggingLevel::WARNING:\
                color_begin = "\e[0;33m";\
                break;\
            case ADAPRO::Data::LoggingLevel::ERROR:\
            case ADAPRO::Data::LoggingLevel::FATAL:\
                color_begin = "\e[0;31m";\
                break;\
            case ADAPRO::Data::LoggingLevel::SPECIAL:\
                color_begin = "\e[0;34m";\
                break;\
            case ADAPRO::Data::LoggingLevel::ADAPRO_DEBUG:\
            case ADAPRO::Data::LoggingLevel::USER_DEBUG:\
                color_begin = "\e[1;30m";\
                break;\
            case ADAPRO::Data::LoggingLevel::INFO:\
            default:;\
        }\
        color_end = severity != ADAPRO::Data::LoggingLevel::INFO ? "\e[0m" : "";
#else
#define INITIALIZE_COLORS
#endif

void Logger::print
(
        const Thread& origin,
        const string& message,
        const LoggingLevel severity
)
noexcept
{
    if (logging_mask & severity)
    {
        std::string color_begin, color_end;
        INITIALIZE_COLORS;
        lock_guard<mutex> lock(output_mutex);
        output_stream.flush();
        output_stream << color_begin <<
                "[" << timestamp() << "] (" << symbol(severity) << ")" <<
                " <" << origin.name <<
                "," << show(origin.get_state()) <<
                "," << show(origin.get_command()) <<
#ifdef _GNU_SOURCE
                "," << to_string(sched_getcpu()) <<
#endif
                "> " << message << color_end << endl;
    }
}

void Logger::print
(
        const std::string& message,
        const ADAPRO::Data::LoggingLevel severity
)
noexcept
{
    if (logging_mask & severity)
    {
        std::string color_begin, color_end;
        INITIALIZE_COLORS;
        lock_guard<mutex> lock(output_mutex);
        output_stream.flush();
        output_stream << color_begin <<
                "[" << timestamp() << "] (" << symbol(severity) << ") " <<
                message << color_end << endl;
    }
}
