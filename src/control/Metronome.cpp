#include <cstdint>
#include <thread>
#include <chrono>
#include <mutex>
#include "../../headers/control/Logger.hpp"
#include "../../headers/control/Thread.hpp"
#include "../../headers/control/Metronome.hpp"
#include "../../headers/library/Clock.hpp"
#include "../../headers/data/State.hpp"

using namespace std;
using namespace ADAPRO::Control;
using namespace ADAPRO::Data;

uint32_t Metronome::S_NO = 0;

Metronome::Metronome
(
        Logger& logger,
        const uint32_t tick_length,
        const int preferred_core
)
noexcept:
        Thread
        (
                logger,
                "Metronome<" + to_string(tick_length) + ">-" + to_string(S_NO++),
                preferred_core
        ),
        semaphore(),
        barrier(),
        tick(chrono::milliseconds(tick_length))
{
    if (tick_length > 0)
    {
        start(true);
    }
}

void Metronome::prepare() {}

void Metronome::execute()
{
    this_thread::sleep_for(tick);
    barrier.notify_all();
}

void Metronome::finish()
{
    barrier.notify_all();
}

void Metronome::handle_exception(const char* message) noexcept
{
    Thread::handle_exception(message);
    barrier.notify_all();
}

void Metronome::synchronize(const uint32_t ticks)
{
    if (get_state() == RUNNING && ticks > 0)
    {
        const uint64_t beginning(ADAPRO::Library::now());
        const uint64_t duration(ticks * tick.count());
        unique_lock<mutex> lock(semaphore);
        barrier.wait_for
        (
                lock,
                chrono::milliseconds(duration),
                [this, beginning, duration]()
                {
                    return get_state() < STOPPING &&
                            ADAPRO::Library::now() - beginning >= duration;
                }
        );
    }

}

Metronome::~Metronome()
{
    const State current_state(get_state());
    if ((current_state & (STARTING | RUNNING | PAUSED)) > 0)
    {
        stop(true);
    }
    else if ((current_state & (STOPPING | STOPPED | ABORTING | ABORTED)) > 0)
    {
        wait_for_state(STOPPED);
    }
}
