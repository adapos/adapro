
#include <cstdlib>
#include <cstdint>
#include <csignal>
#include <list>
#include <map>
#include <utility>
#include <string>
#include <memory>
#include <functional>
#include <adapro/control/Thread.hpp>
#include <adapro/control/Worker.hpp>
#include <adapro/control/Supervisor.hpp>
#include <adapro/control/Session.hpp>
#include <adapro/control/Metronome.hpp>
#include <adapro/control/AOFStream.hpp>
#include <adapro/control/Logger.hpp>
#include <adapro/library/Clock.hpp>
#include <adapro/library/GenericFunctions.hpp>
#include <adapro/ADAPOS/DeliveryType.hpp>
#include <adapro/ADAPOS/DataPointCompositeObject.hpp>
#include <adapro/ADAPOS/DataPointIdentifier.hpp>
#include <adapro/ADAPOS/DataPointValue.hpp>
#include <adapro/data/Context.hpp>
#include <adapro/data/State.hpp>
#include <adapro/data/LoggingLevel.hpp>
#include <adapro/data/Typedefs.hpp>

/**
 * This is a simple worker Thread demonstrating some of the basic functionality
 * of the ADAPRO framework.
 */
class HelloWorldThread final: public ADAPRO::Control::Worker
{
    size_t counter;
    const size_t target;
    ADAPRO::Control::Metronome metronome;
    ADAPRO::Control::AOFStream<ADAPRO::ADAPOS::DataPointCompositeObject> file_output;

    protected:

        /**
         * This virtual procedure will be invoked when this worker Thread
         * starts.
         */
        virtual void prepare() override
        {
            print(configuration.at("USER_GREETING"),
                    ADAPRO::Data::LoggingLevel::INFO);
        }

        /**
         * This virtual procedure will be invoked repeatedly as long as the
         * current command of the worker Thread is <tt>CONTINUE</tt>.
         */
        virtual void execute() override
        {
            if (++counter <= target)
            {
                print("Working hard...", ADAPRO::Data::LoggingLevel::INFO);
                uint64_t timestamp(ADAPRO::Library::epoch_time());
                file_output << ADAPRO::ADAPOS::DataPointCompositeObject
                (
                        ADAPRO::ADAPOS::DataPointIdentifier
                        (
                                "dpid" + to_string(counter),
                                ADAPRO::ADAPOS::DeliveryType::RAW_STRING
                        ),
                        ADAPRO::ADAPOS::DataPointValue
                        (
                                ADAPRO::ADAPOS::DataPointValue::DIRTY_FLAG |
                                ADAPRO::ADAPOS::DataPointValue::BAD_DPID_FLAG,
                                timestamp % 1000,
                                timestamp / 1000,
                                (uint64_t*) (("Test payload " +
                                        std::to_string(counter) + ".").c_str()),
                                ADAPRO::ADAPOS::DeliveryType::RAW_STRING
                        )
                );
                metronome.synchronize(); // Sleeping until the next second.
            }
            else
            {
                print("Enough for today.", ADAPRO::Data::LoggingLevel::INFO);
                stop(false); // Always send commands to self asynchronously!
            }
        }

        /**
         * This virtual procedure will be invoked when the worker stops. Always
         * remember to stop all delegated Threads (if any).
         */
        virtual void finish() override
        {
            print("Cleaning up...", ADAPRO::Data::LoggingLevel::INFO);
            file_output.close();
        }

        /**
         * This exception handler should be unreachable, since this worker class
         * doesn't do any failure-prone computation.
         *
         * @param message The error message as a C-style string (null-terminated
         * char array).
         */
        virtual void handle_exception(const char* message) noexcept
        {
            print("Something went wrong.", ADAPRO::Data::LoggingLevel::FATAL);
//            ADAPRO::Thread::handle_exception(message);
            finish(); // Not called by default, when a Thread aborts. The reason
                      // is that the framework user might need a different
                      // cleanup procedure for abort scenarios.
        }

    public:

        /**
         * <p>The constructor of HelloWorldThread that will be used by the
         * Supervisor Thread of ADAPRO framework indirectly, by invoking the
         * factory function.</p>
         * <p>An instance of this thread has the name
         * <tt>"HelloWorldThread"</tt>, no affinity to any specific CPU core,
         * and a simple transition callback that prints the name of the new
         * state.</p>
         *
         * @param logger        Logger of the ADAPRO Session.
         * @param supervisor    Supervisor of the ADAPRO Session.
         * @param configuration The configuration fetched from the configuration
         * file.
         */
        HelloWorldThread
        (
                ADAPRO::Control::Logger& logger,
                ADAPRO::Control::Supervisor& supervisor,
                const std::map<std::string,std::string>& configuration
        )
        noexcept:
                ADAPRO::Control::Worker
                (
                        logger,
                        supervisor,
                        std::move("HelloWorldThread"),
                        configuration,
                        -1,
                        [this](const ADAPRO::Data::State s)
                        {
                            ADAPRO::Control::Session::GET_LOGGER().print(
                                    name + " goes to " +
                                    ADAPRO::Library::show(s) + "..."
                            );
                        }
                ),
                counter(0),
                target(stoi(configuration.at("USER_TARGET"))),
                metronome(logger, 1000, 2),
                file_output(logger, std::move("DPCOM"), std::move("example.csv"))
        {}

        virtual ~HelloWorldThread() noexcept
        {
            // A known issue (#5): Destroying a running Thread will cause
            // segmentation faults and/or other nasty stuff.
            print("Hopefully I have finished my computations by now...",
                    ADAPRO::Data::LoggingLevel::INFO);
        }
};

/**
 * The main function of this example program.
 *
 * @param argc  Number of command-line arguments.
 * @param argv  Values of the command-line arguments. ADAPRO interprets the
 * first argument as the location of the configuration file. It will be added in
 * the head of the list of configuration paths.
 * @return      The exit status code.
 */
int main(int argc, char** argv)
{
    /**
     * List of the possible configuration file locations (the correct one being
     * <tt>"example.conf</tt>).
     */
    const std::list<std::string> configuration_paths
    {
        "nonexistent.conf", "ToyApplication.conf", "unreachable.conf"
    };

    /**
     * Default configuration. The values will be overwritten with the ones found
     * in the configuration file. When disallowing new keys, the default
     * configuration must contain non-empty string values for each allowed key.
     */
    std::map<std::string, std::string> configuration
    (
            ADAPRO::Data::DEFAULT_CONFIGURATION
    );
    configuration.insert({  "DUMMY",          "PARAMETER"     });
    configuration.insert({  "USER_TARGET",    "0"             });
    configuration.insert({  "USER_GREETING",  "Hello,_Mars!"  });

    /**
     * The Supervisor Thread of ADAPRO framework will use this factory
     * function for creating a HelloWorldThread instance.
     */
    const std::list<ADAPRO::Data::worker_factory_t> factories
    {
            []
            (
                    ADAPRO::Control::Logger& logger,
                    ADAPRO::Control::Supervisor& supervisor,
                    const std::map<std::string, std::string>& configuration
            )
            {
                return std::unique_ptr<HelloWorldThread>
                (
                        new HelloWorldThread(logger, supervisor, configuration)
                );
            }
    };

    const function<bool(const std::string&, const std::string&)> relation(
    [](const std::string& key, const std::string& value) // The relation
    {
        // DEFAULT_RELATION verifies every ADAPRO parameter.
        if (ADAPRO::Data::DEFAULT_RELATION(key, value))
        {
            return true;
        }
        else if (key == "USER_TARGET")
        {
            if (!regex_match(value, ADAPRO::Data::REGEX_UNSIGNED))
            {
                return false;
            }
            else if (std::stoi(value) >= 10)
            {
                ADAPRO::Control::Session::GET_LOGGER().print
                (
                        "Too much work!",
                        ADAPRO::Data::LoggingLevel::FATAL
                );
                return false;
            }
        }
        return true;
    });

    /**
     * This list contains pairs of integers identifying POSIX signals and
     * callback functions.
     */
    const std::map<uint8_t, std::function<void(void)>> signal_callbacks
    {
        {SIGINT, [](){
            ADAPRO::Control::Session::GET_LOGGER().print
            (
                    "SIGINT callback activated.",
                    ADAPRO::Data::LoggingLevel::USER_DEBUG
            );
        }},
        {SIGHUP, [](){
            ADAPRO::Control::Session::GET_LOGGER().print
            (
                    "SIGHUP callback activated.",
                    ADAPRO::Data::LoggingLevel::USER_DEBUG
            );
        }},
        {SIGABRT, [](){
            ADAPRO::Control::Session::GET_LOGGER().print
            (
                    "SIGABRT callback activated.",
                    ADAPRO::Data::LoggingLevel::USER_DEBUG
            );
        }},
        {SIGSEGV, [](){
            ADAPRO::Control::Session::GET_LOGGER().print
            (
                    "SIGSEGV callback activated.",
                    ADAPRO::Data::LoggingLevel::USER_DEBUG
            );
        }}
    };

    const function<void(const uint8_t)> exit_handler = [](const uint8_t code){
        ADAPRO::Control::Session::GET_LOGGER().print
        (
                "Exit handler activated.",
                ADAPRO::Data::LoggingLevel::USER_DEBUG
        );
        exit(code);
    };

    return ADAPRO::Control::Session(ADAPRO::Data::Context(
            "Toy Application",      // Name of the application
            argc,                   // Number of command-line arguments
            (const char**) argv,    // Actual command-line arguments
            factories,              // Worker factories
            configuration_paths,    // Configuration path candidates
            configuration,          // Default configuration
            true,                   // Enabling configuration file reading
            false,                  // Disallowing new keys
            relation,               // The relation for sanity-checking config
            signal_callbacks,       // Signal callbacks
            exit_handler            // Exit handler
    )).run();
}
