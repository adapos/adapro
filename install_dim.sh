#!/bin/bash

wget http://dim.web.cern.ch/dim_v20r33.zip && \
unzip dim_v20r33.zip && \
cd dim_v20r33 && \
mkdir -p /usr/lib/dim/ && \
cp -rf linux/* /usr/lib/dim/ && \
chmod a+x /usr/lib/dim/libdim.so && \
ln -s /usr/lib/dim/libdim.so /usr/lib/libdim.so && \
mkdir -p /usr/include/dim/ && \
cp -rf dim/* /usr/include/dim/ && \
chmod a+rx /usr/include/dim && \
chmod a+r /usr/include/dim/*
